# MySQL Query Profiler

[![Get it from the Snap Store](https://snapcraft.io/static/images/badges/en/snap-store-black.svg)](https://snapcraft.io/mysql-query-profiler)

[![Download](https://api.bintray.com/packages/kpro4/mysql-query-profiler/mysql-query-profiler/images/download.svg)](https://bintray.com/kpro4/mysql-query-profiler/mysql-query-profiler/_latestVersion)

## Installation and use

### Install

#### **Prerequisites for all platforms**

For all platforms, a user on a MySQL server using the _XProtocol_ for authentication (default on newer versions) with enough privileges\* is required.

#### **Linux**

First, install the dependency `libsecret`:

Depending on your distribution, you will need to run the following command:

- Debian/Ubuntu: `sudo apt-get install libsecret-1-dev`
- Red Hat-based: `sudo yum install libsecret-devel`
- Arch Linux: `sudo pacman -S libsecret`

Debian based: If an error mentioning org.freedesktop.secrets pops up during app runtime, install `gnome-keyring`

Either use the _AppImage_, _deb_, or _rpm_ file from [Bintray](https://bintray.com/kpro4/mysql-query-profiler/mysql-query-profiler) or install the snap:

```bash
snap install mysql-query-profiler
```

#### **Windows**

Install the exe-file from [Bintray](https://bintray.com/kpro4/mysql-query-profiler/mysql-query-profiler). Click on the newest version, then _files_.

#### **MacOS**

Use the zip-file from [Bintray](https://bintray.com/kpro4/mysql-query-profiler/mysql-query-profiler). Click on the newest version, then _files_. We recommend unzipping the file with [The Unarchiver](https://theunarchiver.com/), and not with Archive utility as it may not work.

### Usage

Start the application, connect to a database (remote or local) and insert a query you want to debug. Performance will be shown along with an optimizer trace in addition to more options you can configure.

## Development

### Requirements

- Node 10 or later
- Npm
- Yarn

### Install for development

First, clone the repo via git and install dependencies:

```bash
git clone [project link]
cd mysql-query-profiler
yarn
```

Test database: [test_db](https://github.com/datacharmer/test_db)

### Starting Development

Start the app in the `dev` environment. This starts the renderer process in [**hot-module-replacement**](https://webpack.js.org/guides/hmr-react/) mode and starts a webpack dev server that sends hot updates to the renderer process:

```bash
yarn dev
```

### Packaging for Production

To package apps for the local platform:

```bash
yarn package
```

See the scripts in package.json for more packaging options.
