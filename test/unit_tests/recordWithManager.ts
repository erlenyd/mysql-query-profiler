import SqlManager from '../../app/backend/recorder/SqlManager';

export default async function recordWithManager(
  manager: SqlManager,
  query: string,
  tabID: string,
  explain?: boolean
) {
  return manager.record(
    query,
    tabID,
    () => {},
    () => {},
    0.1,
    Boolean(explain)
  );
}
