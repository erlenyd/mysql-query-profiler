import { v4 as uuid } from 'uuid';
import FileIO from '../../../app/backend/utils/FileIO';
import SqlManagerSingleton from '../../../app/backend/recorder/SqlManagerSingleton';
import recordWithManager from '../recordWithManager';

test('Explain analyze', async () => {
  // Setup
  const tabID = uuid();
  const manager = SqlManagerSingleton.getInstance();
  expect(manager).toBeDefined();
  const login = await FileIO.loadLoginDetails();
  expect(login).toBeDefined();
  await manager.connect(tabID, 1, login);
  expect(manager.connections[tabID]).toBeDefined();

  // Test query
  const result = await recordWithManager(
    manager,
    `EXPLAIN ANALYZE
      SELECT *
      FROM
        employees AS e
        JOIN current_dept_emp AS de
          ON e.emp_no=de.emp_no
        JOIN departments d
          ON de.dept_no=d.dept_no;
  `,
    tabID,
    true
  );
  expect(result.result).toBeDefined();
  expect(result.result.error).toBeUndefined();
  expect(result.explainAnalyzeRoot).toBeDefined();
  expect(result.explainAnalyzeParents).toBeDefined();
}, 20000);
