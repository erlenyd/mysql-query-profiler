import { v4 as uuid } from 'uuid';
import FileIO from '../../../app/backend/utils/FileIO';
import SqlManagerSingleton from '../../../app/backend/recorder/SqlManagerSingleton';
import recordWithManager from '../recordWithManager';
import { u1Drop, u1Create, u1Insert } from './queries/u1Geom';

test('Drop table', async () => {
  // Setup
  const tabID = uuid();
  const manager = SqlManagerSingleton.getInstance();
  expect(manager).toBeDefined();
  const login = await FileIO.loadLoginDetails();
  expect(login).toBeDefined();
  await manager.connect(tabID, 1, login);
  expect(manager.connections[tabID]).toBeDefined();

  // Test query
  const result = await recordWithManager(manager, u1Drop, tabID);
  expect(result.result).toBeDefined();
  expect(result.result.error).toBeUndefined();

  // Cleanup
  await manager.removeConnection(tabID);
}, 20000);

test('Create table', async () => {
  // Setup
  const tabID = uuid();
  const manager = SqlManagerSingleton.getInstance();
  expect(manager).toBeDefined();
  const login = await FileIO.loadLoginDetails();
  expect(login).toBeDefined();
  await manager.connect(tabID, 1, login);
  expect(manager.connections[tabID]).toBeDefined();

  // Test query
  await manager.executeQuery(u1Drop, tabID);
  const result = await recordWithManager(manager, u1Create, tabID);
  expect(result.result).toBeDefined();
  expect(result.result.error).toBeUndefined();

  // Cleanup
  await manager.removeConnection(tabID);
}, 20000);

test('Insert into', async () => {
  // Setup
  const tabID = uuid();
  const manager = SqlManagerSingleton.getInstance();
  expect(manager).toBeDefined();
  const login = await FileIO.loadLoginDetails();
  expect(login).toBeDefined();
  await manager.connect(tabID, 1, login);
  const connection = manager.connections[tabID];
  expect(connection).toBeDefined();

  // Test query
  await manager.executeQuery(u1Drop, tabID);
  await manager.executeQuery(u1Create, tabID);
  const result = await recordWithManager(manager, u1Insert, tabID);
  expect(result.result).toBeDefined();
  expect(result.result.error).toBeUndefined();

  // Cleanup
  await manager.removeConnection(tabID);
}, 20000);
