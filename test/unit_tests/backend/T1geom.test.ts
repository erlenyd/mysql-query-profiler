import { v4 as uuid } from 'uuid';
import FileIO from '../../../app/backend/utils/FileIO';
import SqlManagerSingleton from '../../../app/backend/recorder/SqlManagerSingleton';
import recordWithManager from '../recordWithManager';
import { t1Create, t1Drop, t1Insert, t1Select } from './queries/t1Geom';

test('T1', async () => {
  // Setup
  const tabID = uuid();
  const manager = SqlManagerSingleton.getInstance();
  expect(manager).toBeDefined();
  const login = await FileIO.loadLoginDetails();
  expect(login).toBeDefined();
  await manager.connect(tabID, 1, login);
  const connection = manager.connections[tabID];
  expect(connection).toBeDefined();

  // Test query
  await manager.executeQuery(t1Drop, tabID);
  await manager.executeQuery(t1Create, tabID);
  await manager.executeQuery(t1Insert, tabID);
  const result = await recordWithManager(manager, t1Select, tabID);
  expect(result.result).toBeDefined();
  expect(result.result.error).toBeUndefined();
  expect(result.memoryPerformance).toBeDefined();
  expect(result.memoryPerformance.length).toBeTruthy();
  expect(result.top3Memory).toBeDefined();
  expect(result.top3Memory.length).toBeTruthy();
  expect(result.chartColors).toBeDefined();
  expect(result.stageTimes).toBeDefined();
  // expect(result.stageTimes.length).toBeTruthy();
  expect(result.optimizerTrace).toBeDefined();
  expect(result.optimizerTrace.length).toBeTruthy();

  // Cleanup
  await manager.removeConnection(tabID);
}, 20000);
