/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable linebreak-style */
import DataProcessor from '../../../app/backend/data-processor/DataProcessor';
import {
  chartdata,
  input,
  processedstagetimes,
  processresult,
  stagetimes,
  top3charts,
} from './memorytestdata';

test('calculate columnwidths 1', () => {
  const data = {
    labels: [
      'emp_no',
      'birth_date',
      'first_name',
      'last_name',
      'gender',
      'hire_date',
    ],
    types: [1, 12, 7, 7, 16, 12],
    values: [
      [10001, -515376000000, 'Georgi', 'Facello', 'M', 520128000000],
      [10002, -176169600000, 'Bezalel', 'Simmel', 'F', 501379200000],
      [10003, -318124800000, 'Parto', 'Bamford', 'M', 525571200000],
      [10004, -494553600000, 'Chirstian', 'Koblick', 'M', 533779200000],
      [10005, -471657600000, 'Kyoichi', 'Maliniak', 'M', 621561600000],
      [10006, -527040000000, 'Anneke', 'Preusig', 'F', 612748800000],
      [10007, -397958400000, 'Tzvetan', 'Zielinski', 'F', 603072000000],
      [10008, -374457600000, 'Saniya', 'Kalloufi', 'M', 779587200000],
      [10009, -558662400000, 'Sumant', 'Peac', 'F', 477532800000],
      [10010, -207878400000, 'Duangkaew', 'Piveteau', 'F', 619920000000],
    ],
  };
  expect(DataProcessor.calculateRowWidths(data)).toStrictEqual([
    90,
    150,
    150,
    135,
    90,
    150,
  ]);
});

test('calculate columnwidths 2', () => {
  const data = { labels: ['1'], types: [1], values: [[1]] };
  expect(DataProcessor.calculateRowWidths(data)).toStrictEqual([15]);
});

function simpleJson(object: any) {
  const simpleObject: any = {};
  // eslint-disable-next-line no-restricted-syntax
  for (const prop in object) {
    // eslint-disable-next-line no-prototype-builtins
    if (!object.hasOwnProperty(prop)) {
      // eslint-disable-next-line no-continue
      continue;
    }
    if (typeof object[prop] === 'object') {
      // eslint-disable-next-line no-continue
      continue;
    }
    if (typeof object[prop] === 'function') {
      // eslint-disable-next-line no-continue
      continue;
    }
    simpleObject[prop] = object[prop];
  }
  return simpleObject; // returns cleaned up JSON
}

test('explainAnalyze', () => {
  const data = {
    labels: ['EXPLAIN'],
    types: [7],
    values: [
      [
        '-> Limit: 10 row(s)  (actual time=0.043..0.046 rows=10 loops=1)\n    -> Table scan on employees  (cost=30253.11 rows=299202) (actual time=0.042..0.044 rows=10 loops=1)\n',
      ],
    ],
  };
  expect(simpleJson(DataProcessor.processExplainAnalyze(data))).toStrictEqual({
    id: '0',
    offset: -1,
    name: 'root',
    time: '',
    rows: -1,
    value: 0.046,
    timeFirstRow: 0,
    timeAllRows: 0,
    loops: -1,
  });
});

test('process optimizer trace', () => {
  const data = {
    labels: [
      'QUERY',
      'TRACE',
      'MISSING_BYTES_BEYOND_MAX_MEM_SIZE',
      'INSUFFICIENT_PRIVILEGES',
    ],
    types: [7, 7, 1, 1],
    values: [
      [
        'select * from employees limit 10',
        '{\n  "steps": [\n    {\n      "join_preparation": {\n        "select#": 1,\n        "steps": [\n          {\n            "expanded_query": "/* select#1 */ select `employees`.`emp_no` AS `emp_no`,`employees`.`birth_date` AS `birth_date`,`employees`.`first_name` AS `first_name`,`employees`.`last_name` AS `last_name`,`employees`.`gender` AS `gender`,`employees`.`hire_date` AS `hire_date` from `employees` limit 10"\n          }\n        ]\n      }\n    },\n    {\n      "join_optimization": {\n        "select#": 1,\n        "steps": [\n          {\n            "table_dependencies": [\n              {\n                "table": "`employees`",\n                "row_may_be_null": false,\n                "map_bit": 0,\n                "depends_on_map_bits": [\n                ]\n              }\n            ]\n          },\n          {\n            "rows_estimation": [\n              {\n                "table": "`employees`",\n                "table_scan": {\n                  "rows": 299202,\n                  "cost": 332.91\n                }\n              }\n            ]\n          },\n          {\n            "considered_execution_plans": [\n              {\n                "plan_prefix": [\n                ],\n                "table": "`employees`",\n                "best_access_path": {\n                  "considered_access_paths": [\n                    {\n                      "rows_to_scan": 299202,\n                      "access_type": "scan",\n                      "resulting_rows": 299202,\n                      "cost": 30253,\n                      "chosen": true\n                    }\n                  ]\n                },\n                "condition_filtering_pct": 100,\n                "rows_for_plan": 299202,\n                "cost_for_plan": 30253,\n                "chosen": true\n              }\n            ]\n          },\n          {\n            "attaching_conditions_to_tables": {\n              "original_condition": null,\n              "attached_conditions_computation": [\n              ],\n              "attached_conditions_summary": [\n                {\n                  "table": "`employees`",\n                  "attached": null\n                }\n              ]\n            }\n          },\n          {\n            "finalizing_table_conditions": [\n            ]\n          },\n          {\n            "refine_plan": [\n              {\n                "table": "`employees`"\n              }\n            ]\n          }\n        ]\n      }\n    },\n    {\n      "join_execution": {\n        "select#": 1,\n        "steps": [\n        ]\n      }\n    }\n  ]\n}',
        0,
        0,
      ],
    ],
  };
  expect(DataProcessor.processOptimizerTrace(data)).toStrictEqual([
    [
      'select * from employees limit 10',
      {
        steps: [
          {
            join_preparation: {
              'select#': 1,
              steps: [
                {
                  expanded_query:
                    '/* select#1 */ select `employees`.`emp_no` AS `emp_no`,`employees`.`birth_date` AS `birth_date`,`employees`.`first_name` AS `first_name`,`employees`.`last_name` AS `last_name`,`employees`.`gender` AS `gender`,`employees`.`hire_date` AS `hire_date` from `employees` limit 10',
                },
              ],
            },
          },
          {
            join_optimization: {
              'select#': 1,
              steps: [
                {
                  table_dependencies: [
                    {
                      table: '`employees`',
                      row_may_be_null: false,
                      map_bit: 0,
                      depends_on_map_bits: [],
                    },
                  ],
                },
                {
                  rows_estimation: [
                    {
                      table: '`employees`',
                      table_scan: { rows: 299202, cost: 332.91 },
                    },
                  ],
                },
                {
                  considered_execution_plans: [
                    {
                      plan_prefix: [],
                      table: '`employees`',
                      best_access_path: {
                        considered_access_paths: [
                          {
                            rows_to_scan: 299202,
                            access_type: 'scan',
                            resulting_rows: 299202,
                            cost: 30253,
                            chosen: true,
                          },
                        ],
                      },
                      condition_filtering_pct: 100,
                      rows_for_plan: 299202,
                      cost_for_plan: 30253,
                      chosen: true,
                    },
                  ],
                },
                {
                  attaching_conditions_to_tables: {
                    original_condition: null,
                    attached_conditions_computation: [],
                    attached_conditions_summary: [
                      { table: '`employees`', attached: null },
                    ],
                  },
                },
                { finalizing_table_conditions: [] },
                { refine_plan: [{ table: '`employees`' }] },
              ],
            },
          },
          { join_execution: { 'select#': 1, steps: [] } },
        ],
      },
      0,
      0,
    ],
  ]);
});

test('processMemoryPerformance', () => {
  expect(DataProcessor.processMemoryPerformance(input.results)).toStrictEqual(
    processresult
  );
});

test('chartdata', () => {
  expect(DataProcessor.getChartData(processresult)).toStrictEqual(chartdata);
});

test('top 3 memory events', () => {
  expect(DataProcessor.processTop3ChartData(processresult)).toStrictEqual(
    top3charts
  );
});

test('process stage times', () => {
  expect(DataProcessor.processStageTimes(stagetimes)).toStrictEqual(
    processedstagetimes
  );
});

test('format date', () => {
  expect(DataProcessor.formatDate(new Date(0))).toStrictEqual('1970-01-01');
});
