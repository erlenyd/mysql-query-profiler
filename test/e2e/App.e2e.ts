/* eslint-disable jest/no-done-callback */
/* eslint-disable jest/no-commented-out-tests */
/* eslint jest/expect-expect: off */
import { ClientFunction, Selector } from 'testcafe';

fixture`App Tests`.page('../../app/app.html');

test('should open', async (t) => {
  await t.expect(Selector('.content').exists).ok();
});

test('should have expected page title', async (t) => {
  await t
    .expect(ClientFunction(() => document.title)())
    .eql('MySQL Query Profiler');
});

// Need either a proper docker image or a test db for this:
//
// test('should log in', async (t) => {
//   await t
//     .wait(100)
//     .pressKey(
//       'ctrl+a backspace l o c a l h o s t tab r o o t tab p e p e tab tab e m p l o y e e s enter'
//     )
//     .expect(Selector('h5').withText('Record performance of a query').exists)
//     .ok();
//   // .typeText('#user-input', 'localhost')
//   // .expect(Selector('#user-input').innerText)
//   // .eql('localhost');
// });

// EXAMPLES:

// const getPageUrl = ClientFunction(() => window.location.href);
// const counterSelector = Selector('[data-tid="counter"]');
// const buttonsSelector = Selector('[data-tclass="btn"]');
// const clickToCounterLink = (t) =>
//   t.click(Selector('a').withExactText('to Counter'));
// const incrementButton = buttonsSelector.nth(0);
// const decrementButton = buttonsSelector.nth(1);
// const oddButton = buttonsSelector.nth(2);
// const asyncButton = buttonsSelector.nth(3);
// const getCounterText = () => counterSelector().innerText;
// const assertNoConsoleErrors = async (t) => {
//   const { error } = await t.getBrowserConsoleMessages();
//   await t.expect(error).eql([]);
// };

// fixture`Home Page`.page('../../app/app.html').afterEach(assertNoConsoleErrors);

// test(
//   'should not have any logs in console of main window',
//   assertNoConsoleErrors
// );

// test('should navigate to Counter with click on the "to Counter" link', async (t) => {
//   await t.click('[data-tid=container] > a').expect(getCounterText()).eql('0');
// });

// fixture`Counter Tests`
//   .page('../../app/app.html')
//   .beforeEach(clickToCounterLink)
//   .afterEach(assertNoConsoleErrors);

// test('should display updated count after the increment button click', async (t) => {
//   await t.click(incrementButton).expect(getCounterText()).eql('1');
// });

// test('should display updated count after the descrement button click', async (t) => {
//   await t.click(decrementButton).expect(getCounterText()).eql('-1');
// });

// test('should not change even counter if odd button clicked', async (t) => {
//   await t.click(oddButton).expect(getCounterText()).eql('0');
// });

// test('should change odd counter if odd button clicked', async (t) => {
//   await t
//     .click(incrementButton)
//     .click(oddButton)
//     .expect(getCounterText())
//     .eql('2');
// });

// test('should change if async button clicked and a second later', async (t) => {
//   await t
//     .click(asyncButton)
//     .expect(getCounterText())
//     .eql('0')
//     .expect(getCounterText())
//     .eql('1');
// });

// test('should back to home if back button clicked', async (t) => {
//   await t
//     .click('[data-tid="backButton"] > a')
//     .expect(Selector('[data-tid="container"]').visible)
//     .ok();
// });
