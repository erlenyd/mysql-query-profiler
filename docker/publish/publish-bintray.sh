#! /bin/bash

# Creating new version on Bintray
ticked_version=$(jq .version app/package.json)
version=${ticked_version//[\"]/}
desc=$(jq .description package.json)
# Uploading to Bintray
curl -v -T release/'MySQL Query Profiler'-${version}.AppImage -ukpro4:${BT_TOKEN} -H "X-Bintray-Package:mysql-query-profiler" -H "X-Bintray-Version:${version}" https://api.bintray.com/content/kpro4/mysql-query-profiler/mysql-query-profiler-${version}.AppImage
curl -v -T release/mysql-query-profiler_${version}_amd64.deb -ukpro4:${BT_TOKEN} -H "X-Bintray-Package:mysql-query-profiler" -H "X-Bintray-Version:${version}" https://api.bintray.com/content/kpro4/mysql-query-profiler/mysql-query-profiler-${version}.deb
curl -v -T release/mysql-query-profiler-${version}.x86_64.rpm -ukpro4:${BT_TOKEN} -H "X-Bintray-Package:mysql-query-profiler" -H "X-Bintray-Version:${version}" https://api.bintray.com/content/kpro4/mysql-query-profiler/mysql-query-profiler-${version}.rpm
curl -v -T release/'MySQL Query Profiler Setup '${version}.exe -ukpro4:${BT_TOKEN} -H "X-Bintray-Package:mysql-query-profiler" -H "X-Bintray-Version:${version}" https://api.bintray.com/content/kpro4/mysql-query-profiler/mysql-query-profiler-${version}.exe
curl -v -T release/'MySQL Query Profiler'-${version}-mac.zip -ukpro4:${BT_TOKEN} -H "X-Bintray-Package:mysql-query-profiler" -H "X-Bintray-Version:${version}" https://api.bintray.com/content/kpro4/mysql-query-profiler/mysql-query-profiler-${version}-mac.zip
# Publishing the new version
curl -v -X POST -ukpro4:${BT_TOKEN} https://api.bintray.com/content/kpro4/mysql-query-profiler/mysql-query-profiler/${version}/publish

