import React from 'react';
import { createMuiTheme, ThemeProvider } from '@material-ui/core';
import { HashRouter as Router } from 'react-router-dom';
import TabContainer from './containers/TabContainer';
import { TabProvider } from './context/TabContext';

const theme = createMuiTheme({
  palette: {
    type: 'dark',
    error: {
      main: '#FF9292',
    },
    primary: {
      light: '#769CFF',
      main: '#769CFF',
      dark: '#404040', // Dark 1
      contrastText: '#FFFFFF',
    },
    text: {
      primary: 'rgba(255, 255, 255, 0.9)',
      secondary: 'rgba(255, 255, 255, 0.7)',
      disabled: 'rgba(255, 255, 255, 0.6)',
    },
    background: {
      default: '#404040', // Dark 2
      paper: '#404040', // Dark 1
    },
  },
  typography: {
    fontFamily: ['Raleway', 'Arial'].join(','),
    button: {
      textTransform: 'initial',
    },
    subtitle2: {
      color: 'rgba(255, 255, 255, 0.7)',
    },
  },
  spacing: 8,
  props: {
    MuiInputBase: {
      margin: 'dense',
    },
    MuiInputLabel: {
      margin: 'dense',
    },
  },
  overrides: {
    MuiSnackbarContent: {
      root: {
        backgroundColor: '#4B4B4B', // Dark 3
        color: 'rgba(255, 255, 255, 0.9)',
      },
    },
  },
});

/**
 * The top component for the app. Sets the app theme, contains the router and tabcontainer. Provides tab context
 */
export default function App() {
  return (
    <div className="content">
      <ThemeProvider theme={theme}>
        <Router>
          <TabProvider>
            <TabContainer />
          </TabProvider>
        </Router>
      </ThemeProvider>
    </div>
  );
}
