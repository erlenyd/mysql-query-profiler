/* eslint-disable no-plusplus */
/* eslint-disable class-methods-use-this */
import { ResultValue } from '@mysql/xdevapi';
import lodash from 'lodash';
import { Result } from '../utils/mysql';
import { ChartColors } from './types/ChartColors';
import { ChartData } from './types/ChartData';
import { ExplainAnalyzeNode } from './types/ExplainAnalyzeNode';
import { MemoryPerformance } from './types/MemoryPerformance';
import { MemoryPerformanceDataPoint } from './types/MemoryPerformanceDataPoint';
import { StageTimes } from './types/StageTimes';

/**
 * A class responsible for processing the raw data from a recording into something which can be easily displayed
 */
export default class DataProcessor {
  static processMemoryPerformance(performanceData?: Result) {
    // TODO: Add more restrictive tests
    if (performanceData && performanceData.labels && performanceData.values) {
      const { values } = performanceData;
      const result: MemoryPerformance = [
        {
          eventName: 'total',
          isBytesUsedZero: true,
          times: [],
          bytesUsed: [],
        },
      ];
      values.forEach((resultValue: ResultValue[]) => {
        const foundTimeline = result.find(
          (value) => value.eventName === resultValue[1]
        );
        // Sorting through and adding times and bytes used to timelines
        if (foundTimeline) {
          foundTimeline.times.push(Number(resultValue[0]));
          foundTimeline.bytesUsed.push(Number(resultValue[2]));
        } else {
          result.push({
            eventName: String(resultValue[1]),
            isBytesUsedZero: true,
            times: [Number(resultValue[0])],
            bytesUsed: [Number(resultValue[2])],
          });
        }
        // Adding bytes to total timeline
        const foundTotalTimeline = result.find(
          (value) => value.eventName === 'total'
        );
        if (foundTotalTimeline) {
          // Adding timestamps not in the total timeline
          if (!foundTotalTimeline.times.includes(Number(resultValue[0]))) {
            foundTotalTimeline.times.push(Number(resultValue[0]));
            foundTotalTimeline.bytesUsed.push(0); // Starting at 0 bytes, will increment
          }
          // Incrementing the bytes used for the current timestamp
          foundTotalTimeline.bytesUsed[
            foundTotalTimeline.times.length - 1
          ] += Number(resultValue[2]);
        }
      });
      // Checking if some timelines do not have memory use, setting a flag if no bytes used
      result.forEach((timeLine) => {
        timeLine.isBytesUsedZero =
          timeLine.bytesUsed.reduce((a, b) => a + b, 0) === 0;
      });
      // return DataProcessor.getChartData(result);
      return result;
    }
    throw new Error('DataProcessor: performanceData had wrong format');
  }

  static getChartData(memoryPerformance: MemoryPerformance) {
    const result: ChartData = [];

    memoryPerformance[0].bytesUsed.forEach((_, index) => {
      const memoryPerformanceDataPoint: MemoryPerformanceDataPoint = {
        relative_time: memoryPerformance[0].times[index],
      };

      memoryPerformance.forEach((performanceTimeline) => {
        if (!performanceTimeline.isBytesUsedZero) {
          memoryPerformanceDataPoint[performanceTimeline.eventName] =
            performanceTimeline.bytesUsed[index];
        }
      });
      result.push(memoryPerformanceDataPoint);
    });
    // DataProcessor.log(`Memory data points: ${result.length}`);
    return result;
  }

  static processStageTimes(stageTimesRaw?: Result) {
    if (stageTimesRaw && stageTimesRaw.labels && stageTimesRaw.values) {
      const { values } = stageTimesRaw;
      const result: StageTimes = [];
      values.forEach((resultValue: ResultValue[]) => {
        result.push({
          stage: String(resultValue[0]),
          source: String(resultValue[1]),
          startTime: Number(resultValue[2]),
          endTime: Number(resultValue[3]),
        });
      });
      return result;
    }
    throw new Error('DataProcessor: stageTimesRaw had wrong format');
  }

  static formatDate(d: Date) {
    let month = `${d.getMonth() + 1}`;
    let day = `${d.getDate()}`;
    const year = d.getFullYear();

    if (month.length < 2) month = `0${month}`;
    if (day.length < 2) day = `0${day}`;

    return [year, month, day].join('-');
  }

  static calculateRowWidths({
    values,
    labels,
    types,
  }: {
    values: ResultValue[][];
    labels: string[];
    types: number[];
  }): number[] {
    const highest = labels.map((label) => label.length);
    for (let i = 0; i < values.length; i++) {
      const currentObject = values[0];
      for (let j = 0; j < currentObject.length; j++) {
        const datatype = types[j];
        highest[j] = Math.max(
          highest[j],
          datatype === 12
            ? DataProcessor.formatDate(new Date(Number(currentObject[j])))
                .length
            : String(currentObject[j]).length
        );
      }
    }
    return highest.map((stringlength) => stringlength * 15);
  }

  static processOptimizerTrace(optimizerTrace?: Result) {
    let traceResult;
    if (optimizerTrace && optimizerTrace.values) {
      traceResult = optimizerTrace;
      if (traceResult.values.length > 0 && traceResult.values[0].length > 1) {
        traceResult.values[0][1] = JSON.parse(String(traceResult.values[0][1]));
      }
      return traceResult.values;
    }
    throw new Error('DataProcessor: optimizerTrace had wrong format');
  }

  static assignColor(key: string) {
    const tempObject: ChartColors = {};
    tempObject[key] = `#${(0x1000000 + Math.random() * 0xffffff)
      .toString(16)
      .substr(1, 6)}`;
    return tempObject;
  }

  static processChartColors(chartData: ChartData) {
    const chartColors: ChartColors = {};
    const dataKeys = lodash.keys(chartData[0]).slice(1);
    lodash.forEach(dataKeys, (key) =>
      lodash.assign(chartColors, DataProcessor.assignColor(key))
    );
    return chartColors;
  }

  static processTop3ChartData(memoryPerformance: MemoryPerformance) {
    const sortedChartData = lodash.sortBy(memoryPerformance, (o) =>
      lodash.sum(o.bytesUsed)
    );
    const top3ChartData = lodash.takeRight(sortedChartData, 3);
    const top3List: string[] = [];
    lodash.forEach(top3ChartData, (o) => top3List.push(o.eventName));
    return top3List;
  }

  // -> Limit: 10 row(s)  (actual time=0.098..0.103 rows=10 loops=1)
  //   -> Table scan on employees  (cost=30192.25 rows=299600) (actual time=0.095..0.099 rows=10 loops=1)

  // " Table scan on ints  (actual time=0.006..0.029 rows=5 loops=1)"
  // " Materialize  (actual time=2.662..2.697 rows=5 loops=1)       "
  // " Rows fetched before execution  (actual time=0.00

  static parentDict: { [id: string]: string } = {};

  static processExplainAnalyze(explainAnalyze?: Result) {
    if (
      !explainAnalyze ||
      explainAnalyze?.values.length === 0 ||
      explainAnalyze.values[0].length === 0
    ) {
      throw Error('DataProcessor: Explain analyze had no values');
    }
    const explainText = String(explainAnalyze?.values[0][0]);
    const rootNode: ExplainAnalyzeNode = {
      id: '0',
      offset: -1,
      name: 'root',
      time: '',
      rows: -1,
      value: 0,
      timeFirstRow: 0,
      timeAllRows: 0,
      loops: -1,
      additionalData: [],
      children: [],
    };
    const regexp = /([ ]*)?-> (Hash|(.+?)( \(cost=([\d.]+) rows=(\d+)\))? \(actual time=([\d.]+) rows=(\d+) loops=(\d+)\))/gm;
    const matches = [...explainText.matchAll(regexp)];
    const nodes: ExplainAnalyzeNode[] = [rootNode];
    // Create object for each line. Root has ID 0
    let idIterator = 1;
    matches.forEach((match) => {
      const node = DataProcessor.createNode(match, idIterator);
      if (node) {
        nodes.push(node);
      } else {
        throw Error('DataProcessor: Failed to create node');
      }
      idIterator += 1;
    });

    const parents: { [id: string]: string } = {};

    // Assign children of each object
    // Iterate through all objects
    nodes.forEach((node, index) => {
      if (index === 0) return;
      if (nodes[index - 1].offset < node.offset) {
        parents[node.id] = nodes[index - 1].id;
        nodes[index - 1].children.push(node);
      } else {
        // If current node's offset is smaller then it cannot be child of the previous node
        // Iterate backwards from current node until a node with smaller offset has been reached
        for (let j = index - 1; index > 0; j -= 1) {
          if (nodes[j].offset < nodes[index].offset) {
            parents[nodes[index].id] = nodes[j].id;
            nodes[j].children.push(nodes[index]);
            break;
          }
        }
      }
    });

    DataProcessor.parentDict = parents;
    DataProcessor.fixNodeTimes(rootNode);
    DataProcessor.scanForInvisibleNodes(rootNode);

    return rootNode;
  }

  /**
   * Makes sure total time is represented accurately in the flame graph
   * If a node has smaller total time than its children, the total time of its children are added
   * @param node
   */
  static fixNodeTimes(node: ExplainAnalyzeNode) {
    // const threshold = 0.05;
    if (!node.children.length) {
      return node;
    }
    let sumChildren = 0;
    node.children.forEach((element) => {
      const nextNode = DataProcessor.fixNodeTimes(element);
      sumChildren += nextNode.value;
    });
    // If parent nodes's total time (value) is a certain degree (threshold) smaller than sum of childrens' times,
    // then parent's total time is set to THE SAME as childrens' total time
    // If it's smaller than that the childrens' total time is ADDED to parent's total time
    /* if (node.value < sumChildren * (1 - threshold)) {
      const prev = String(node.value);
      node.value += sumChildren;
      console.log(
        `${node.name} total time changed from ${prev} to ${String(
          node.value
        )} (added to old value)`
      );
    } else */
    if (node.value < sumChildren) {
      const prev = node.value;
      node.value += sumChildren;
      node.additionalData.push({
        description: 'Total time change',
        data: `Total time changed from ${prev} ms to ${String(node.value)} ms`,
      });
    }
    return node;
  }

  static scanForInvisibleNodes(node: ExplainAnalyzeNode) {
    const threshold = 1 / 50;
    node.children.forEach((child) => {
      if (child.value <= node.value * threshold) {
        node.additionalData.push({
          description: 'Invisible node',
          data: `Child node '${child.name}' might be invisible on the graph`,
        });
      }
      DataProcessor.scanForInvisibleNodes(child);
    });
  }

  private static createNode(
    match: RegExpMatchArray,
    idIterator: number
  ): ExplainAnalyzeNode | null {
    if (match[2] === 'Hash') {
      return {
        id: idIterator.toString(),
        offset: match[1] ? match[1].length : 0,
        name: match[2],
        time: '',
        value: 0,
        timeFirstRow: 0,
        timeAllRows: 0,
        rows: 0,
        loops: 0,
        additionalData: [],
        children: [],
      };
    }
    const timeResult = match[7]
      ? match[7].match(/(\d*\.?\d*)\.\.(\d*\.?\d*)/)
      : undefined;
    if (timeResult) {
      return {
        id: idIterator.toString(),
        offset: match[1] ? match[1].length : 0,
        name: match[3],
        cost_est: match[5] ? Number(match[5]) : undefined,
        rows_est: match[6] ? Number(match[6]) : undefined,
        time: match[7],
        // Value = Total time = Time all rows * loops
        value: Number(timeResult[2]) * Number(match[9]),
        timeFirstRow: Number(timeResult[1]),
        timeAllRows: Number(timeResult[2]),
        rows: Number(match[8]),
        loops: Number(match[9]),
        additionalData: [],
        children: [],
      };
    }
    return null;
  }

  static log(message: string) {
    console.log(`DataProcessor: ${String(message)}`);
  }
}
