export interface TableElement {
  description: string;
  data: string;
}
