export interface MemoryPerformanceDataPoint {
  relative_time: number;
  [eventName: string]: number;
}
