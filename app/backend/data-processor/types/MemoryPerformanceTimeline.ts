export interface MemoryPerformanceTimeline {
  eventName: string;
  isBytesUsedZero: boolean;
  times: Array<number>;
  bytesUsed: Array<number>;
}
