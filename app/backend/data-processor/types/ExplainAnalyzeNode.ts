import { TableElement } from './TableElement';

export interface ExplainAnalyzeNode {
  id: string;
  offset: number;
  name: string;
  cost_est?: number;
  rows_est?: number;
  time: string;
  value: number;
  timeFirstRow: number;
  timeAllRows: number;
  rows: number;
  loops: number;
  // Each object in additionalData is added to the table associated to each node
  additionalData: TableElement[];
  children: ExplainAnalyzeNode[];
}
