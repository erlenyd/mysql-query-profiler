import { StageTime } from './StageTime';

export type StageTimes = Array<StageTime>;
