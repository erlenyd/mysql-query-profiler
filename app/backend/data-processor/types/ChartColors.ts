export interface ChartColors {
  [key: string]: string;
}
