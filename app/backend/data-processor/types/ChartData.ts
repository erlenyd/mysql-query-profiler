import { MemoryPerformanceDataPoint } from './MemoryPerformanceDataPoint';

export type ChartData = Array<MemoryPerformanceDataPoint>;
