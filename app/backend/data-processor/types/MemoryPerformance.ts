import { MemoryPerformanceTimeline } from './MemoryPerformanceTimeline';

export type MemoryPerformance = Array<MemoryPerformanceTimeline>;
