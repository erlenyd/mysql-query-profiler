export interface StageTime {
  stage: string;
  source: string;
  startTime: number;
  endTime: number;
}
