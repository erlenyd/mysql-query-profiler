# As mentioned in video and repo (as well as seen here) this will dump geometry memory usage as well as total memory usage. We could also divide other total memory by looking at different events.
SET @min_ts = (SELECT UNIX_TIMESTAMP(MIN(TS)) FROM monitoring_data);
(SELECT 'wall_clock', 'relative time', 'geom', CAST('total' AS CHAR))
UNION
(SELECT
   TS AS the_ts,
   UNIX_TIMESTAMP(TS) - @min_ts,
   (SELECT CURRENT_NUMBER_OF_BYTES_USED FROM monitoring_data WHERE EVENT_NAME='memory/sql/Geometry::ptr_and_wkb_data' AND TS=the_ts),
   SUM(CURRENT_NUMBER_OF_BYTES_USED)
 FROM monitoring_data
 GROUP BY the_ts
)
INTO OUTFILE '/tmp/memory.csv'
  FIELDS TERMINATED BY ','
  OPTIONALLY ENCLOSED BY '"'
;
