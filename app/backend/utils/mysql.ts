import mysqlx, {
  URI,
  Session,
  PoolingOptions,
  Client,
  ResultValue,
  Column,
} from '@mysql/xdevapi';

// mysqlx docs: https://dev.mysql.com/doc/dev/connector-nodejs/8.0/tutorial-Connecting_to_a_Server.html

export interface Result {
  labels: Array<string>;
  types: Array<number>;
  values: Array<Array<ResultValue>>;
}

export type ColumnTypes = {
  BIT: 1;
  TINYINT: 2;
  SMALLINT: 3;
  MEDIUMINT: 4;
  INT: 5;
  BIGINT: 6;
  FLOAT: 7;
  DECIMAL: 8;
  DOUBLE: 9;
  JSON: 10;
  STRING: 11;
  BYTES: 12;
  TIME: 13;
  DATE: 14;
  DATETIME: 15;
  TIMESTAMP: 16;
  SET: 17;
  ENUM: 18;
  GEOMETRY: 19;
};

/**
 * Run a query using a Session
 * @param session (Session)
 * @param query (string)
 * @returns Array<ResultSet>
 */
export async function runQuery(query: string, session?: Session) {
  try {
    if (!session) throw new Error('No session was passed to runQuery');
    const result = await session.sql(query).execute();
    const columns = result.getColumns();
    const types = columns.map((column) => column.getType());
    const fetchedResultSets = result.fetchAll();
    const results: Result = {
      labels: columns.map((value: Column) => value.getColumnLabel()),
      types,
      values: fetchedResultSets.map((valueArray: Array<ResultValue>) =>
        valueArray.map((value: ResultValue) => value)
      ),
    };
    return { results, error: undefined };
  } catch (error) {
    console.error(`Error while running query: ${error}\nQuery was: ${query}`);
    return { results: undefined, error };
  }
}

export async function startTransaction(session?: Session) {
  try {
    if (!session) throw new Error('No session was passed to startTransaction');
    await session.startTransaction();
  } catch (error) {
    console.error(`Error while running startTransaction: ${error}`);
    throw error;
  }
}

export async function commit(session?: Session) {
  try {
    if (!session) throw new Error('No session was passed to commit');
    await session.commit();
  } catch (error) {
    console.error(`Error while running commit: ${error}`);
    throw error;
  }
}

/**
 * Create a connection to a MySQL server and return a Client
 * @param connection (URI | string): Connection config or a connectionstring
 * @param poolingOptions (PoolingOptions): Options for pooling connections, allowing one connection per thread
 * @returns Client: A client which handles the connection pool
 */
export async function createConnection(
  connection?: URI | string,
  poolingOptions: PoolingOptions = {
    pooling: { enabled: true, maxSize: 3 },
  }
) {
  try {
    if (!connection) {
      throw new Error(
        'No connection information was passed to createConnection'
      );
    }
    return await mysqlx.getClient(connection, poolingOptions);
  } catch (error) {
    console.error(`Error while creating connection: ${error}`);
    throw error;
  }
}

/**
 * Create a session from a client
 * @param client (Client): A client which is returned from createConnection()
 * @param database (string?): Optionally specify database to connect to
 * @returns Session
 */
export async function createSession(client?: Client, database?: string) {
  if (!client || !database) {
    throw new Error(
      'Either client or database was not passed to createSession'
    );
  }
  const session = await client.getSession();
  if (database) {
    const result = await runQuery(`USE ${database};`, session);
    if (result.error) throw result.error;
  }
  return session;
}

/**
 * Close the connection, destroying the pool, closing and cleaning up all connections in the pool
 * @param client
 */
export async function closeConnection(client?: Client) {
  try {
    if (!client) throw new Error('No client passed');
    await client.close();
  } catch (error) {
    console.error(`Error while disconnecting: ${error}`);
    throw error;
  }
}
