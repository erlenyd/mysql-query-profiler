import * as fs from 'fs';
import path from 'path';
import process from 'process';
import keytar from 'keytar';
import { Recording } from '../../types/Recording';
import { LoginDetails } from '../types/LoginDetails';

// fs docs: https://nodejs.org/api/fs.html#fs_file_paths

/**
 * A class responsible for writing and reading files
 */
export default class FileIO {
  static async saveLoginDetails(loginDetails: LoginDetails) {
    const loginDetailsLocation = path.join(
      this.getAppDataLocation(),
      'loginDetails.json'
    );
    if (
      loginDetails.savePassword &&
      loginDetails.user &&
      loginDetails.password
    ) {
      await keytar.setPassword(
        'MySQL Query Profiler',
        loginDetails.user,
        loginDetails.password
      );
    }
    const loginDetailsWithoutPass = { ...loginDetails, password: '' };
    if (!fs.existsSync(this.getAppDataLocation())) {
      console.log('Folder did not exist, creating');
      await fs.promises.mkdir(this.getAppDataLocation());
    }
    await fs.promises.writeFile(
      loginDetailsLocation,
      JSON.stringify(loginDetailsWithoutPass, undefined, 2)
    );
    return true;
  }

  static async loadLoginDetails(): Promise<LoginDetails | undefined> {
    const loginDetailsLocation = path.join(
      this.getAppDataLocation(),
      'loginDetails.json'
    );
    if (fs.existsSync(loginDetailsLocation)) {
      const data = await fs.promises.readFile(loginDetailsLocation);
      const loginDetails = JSON.parse(String(data));
      if (loginDetails.savePassword) {
        const password = await keytar.getPassword(
          'MySQL Query Profiler',
          loginDetails.user
        );
        loginDetails.password = password;
      }
      return loginDetails;
    }
    console.log(`${loginDetailsLocation} does not exist!`);
    return undefined;
  }

  static async saveRecordingDetails(recDetails: Recording, filename: string) {
    const dest = path.join(this.getAppDataLocation(), 'Recordings/');
    const savePath = `${dest + filename}.json`;
    if (!fs.existsSync(dest)) {
      fs.mkdirSync(dest);
    }
    await fs.promises.writeFile(
      savePath,
      JSON.stringify(recDetails, undefined, 2)
    );
    return savePath;
  }

  static async loadRecording(filepath: string): Promise<Recording> {
    try {
      if (fs.existsSync(filepath)) {
        const data = await fs.promises.readFile(filepath);
        const object = JSON.parse(String(data));
        const castObject = object as Recording;
        if (
          !(
            castObject.uuid &&
            castObject.explainAnalyze !== undefined &&
            castObject.label !== undefined &&
            castObject.query !== undefined
          )
        ) {
          throw new Error('File was not a recording');
        }
        return castObject;
      }
      throw new Error(`${filepath} does not exist! `);
    } catch (error) {
      throw new Error('Filetype not supported');
    }
  }

  static getAppDataLocation() {
    const { platform } = process;
    if (platform === 'darwin' && process.env.HOME) {
      return path.join(
        process.env.HOME,
        'Library',
        'Application Support',
        'MySQL Query Profiler'
      );
    }
    if (platform === 'win32' && process.env.APPDATA) {
      return path.join(process.env.APPDATA, 'MySQL Query Profiler');
    }
    if (platform === 'linux' && process.env.HOME) {
      return path.join(process.env.HOME, '.MySQL Query Profiler');
    }

    console.log(
      'Unrecognized platform: App data will be written to current location if possible'
    );
    return '';
  }
}
