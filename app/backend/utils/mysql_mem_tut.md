# Memory profiling

Watch https://www.youtube.com/watch?v=hKGWdqlEt98 for reference.

Goal: 1. Execute query 2. Use other connection to monitor executing query 3. Use this other connection to pull information out of performance schema to analyse memory usage of executing query

## How to

During the following explination **a** refers to the connection beeing monitored and **b** to the monitoring connection.

### Understand whats going on

- You need the `connection_id` of **a**
  - `select connection_id()` ;
- From **b** save this id to a variable
  - `set @pid = $number$`;
  - _Sidenote: id's for all connections can be found by using "`show processlist;`"_
- Performance schema works with thread id's not connection/process id's, so we need to find the corresponding thread id
  - `select * from performance_schema.threads where processlist_id=@pid\G`
- Assign thread id to variable
  - `set @tid = (select thread_id from performance_schema.threads where processlist_id=@pid\G)`
- To see memory usage we will look at the field `current_number_of_bytes_used` found in the table `performance_schema.memory_summary_by_thread_by_event_name`
- The following query will list `current_number_of_bytes_used `for all events the thread with id `@tid` is involved with
  - `select event_name, current_number_of_bytes_used from performance_schema.memory_summary_by_thread_by_event_name where thread_id=@tid`

### How to do monitoring and data dump

- Time for monitoring (from **b**)
  - use content of `monitor_connection.sql`
  - `call monitor_connection(@pid)`
- example query to monitor creates lots of geometry memory (executed by **a**)
  - `select sleep(2), st_buffer(point(0,0), 1000, st_buffer_strategy('point_circle', i)) from (values row(1), row(10), row(1000), row(10000), row(50000)) as ints(i);`
- Followed by content of `dump_data.sql` which will create a csv file

- `monitor_connection.sql` will also create a table named `monitoring_stages` which can be looked into to see the stages of execution
  - `select * from monitoring_stages`

NB: In some cases memory can be freed by another thread than the one who allocated it which can result in weird numbers in the dataset. This should not be the case for geometry memory according to Norvald.
