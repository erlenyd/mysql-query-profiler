import { Session, Client } from '@mysql/xdevapi';
import { createSession, runQuery } from '../utils/mysql';

/**
 * A class responsible for running queries which will be recorded
 */
export default class SqlRunner {
  session?: Session;

  connectionID?: string;

  threadID?: string;

  async connect(client?: Client, database?: string) {
    this.session = await createSession(client, database);
    let resultSet = await runQuery(`SELECT connection_id();`, this.session);
    this.connectionID = String(resultSet?.results?.values[0]);
    resultSet = await runQuery(
      `select thread_id from performance_schema.threads where processlist_id=${this.connectionID}`,
      this.session
    );
    this.threadID = String(resultSet?.results?.values[0]);
    // console.log(
    //   `SqlRunner: ConnectionID: ${this.connectionID}, ThreadID: ${this.threadID}`
    // );
    return client;
  }

  async executeQuery(query: string, explainAnalyze?: boolean) {
    await runQuery('SET optimizer_trace="enabled=on";', this.session);
    if (explainAnalyze && !query.toLowerCase().includes('explain analyze')) {
      const newQuery = `EXPLAIN ANALYZE ${query}`;
      return runQuery(newQuery, this.session);
    }
    return runQuery(query, this.session);
  }

  async getOptimizerTrace() {
    return runQuery(
      'SELECT * FROM INFORMATION_SCHEMA.OPTIMIZER_TRACE;',
      this.session
    );
  }
}
