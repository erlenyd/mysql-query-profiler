/* eslint-disable @typescript-eslint/no-explicit-any */
import { Client, ResultValue } from '@mysql/xdevapi';
import FileIO from '../utils/FileIO';
import SqlRunner from './SqlRunner';
import SqlMonitor from './SqlMonitor';
import { closeConnection, Result } from '../utils/mysql';
import { ChartData } from '../data-processor/types/ChartData';
import { StageTimes } from '../data-processor/types/StageTimes';
import DataProcessor from '../data-processor/DataProcessor';
import SqlAgent from './SqlAgent';
import { ChartColors } from '../data-processor/types/ChartColors';
import { ExplainAnalyzeNode } from '../data-processor/types/ExplainAnalyzeNode';
import { LoginDetails } from '../types/LoginDetails';

export interface RawRecording {
  result?: { results?: Result; error?: any; columnWidths?: number[] };
  memoryPerformance?: ChartData;
  chartColors?: ChartColors;
  top3Memory?: string[];
  error?: any;
  optimizerTrace?: ResultValue[][];
  stageTimes?: StageTimes;
  explainAnalyzeRoot?: ExplainAnalyzeNode;
  explainAnalyzeParents?: { [id: string]: string };
  tabID: string;
}

/**
 * A class responsible for managing connections and performing recordings
 */
export default class SqlManager {
  connections: {
    [key: string]: {
      loginDetails: LoginDetails;
      client?: Client;
      runner: SqlRunner;
      monitor: SqlMonitor;
      agent: SqlAgent;
      running: boolean;
      cancelled: boolean;
    };
  } = {};

  async connect(tabID: string, serial?: number, loginDetails?: LoginDetails) {
    if (!loginDetails) return;
    // SqlManager.log(JSON.stringify(loginDetails, undefined, 2));
    if (!this.connections[tabID]) {
      this.connections[tabID] = {
        loginDetails,
        runner: new SqlRunner(),
        monitor: new SqlMonitor(),
        agent: new SqlAgent(),
        running: false,
        cancelled: false,
      };
    }
    const connection = this.connections[tabID];
    connection.client = await connection.agent.connect(loginDetails);
    await connection.runner.connect(connection.client, loginDetails?.database);
    await connection.monitor.connect(
      connection.client,
      loginDetails?.database,
      serial
    );
    await FileIO.saveLoginDetails({
      ...loginDetails,
      password: loginDetails.savePassword ? loginDetails.password : '',
    });
  }

  async record(
    query: string,
    tabID: string,
    setUserQueryIsRunning: (running: boolean) => void,
    stateCallback: (state: string) => void,
    monitorTimeStep: number,
    explainAnalyze?: boolean
  ): Promise<RawRecording> {
    const connection = this.connections[tabID];
    const { runner, monitor, agent } = connection;
    // console.log(
    //   `timestep: ${monitorTimeStep},
    //   query: ${query},
    //   tabID: ${tabID},
    //   runnerCID: ${runner.connectionID},
    //   runnerTID: ${runner.threadID},
    //   monitorCID: ${monitor.connectionID},
    //   monitorTID: ${monitor.threadID}`
    // );
    if (
      !runner.connectionID ||
      !runner.threadID ||
      !monitor.connectionID ||
      !monitor.threadID ||
      !monitorTimeStep ||
      query === undefined ||
      !tabID ||
      !setUserQueryIsRunning
    ) {
      throw new Error(
        'SqlManager.record: Runner, monitor, agent or connectionID was missing. Maybe you forgot to call connect()?'
      );
    }
    const stageIgnoreBeforeResult = await monitor.getStageIgnoreBeforeResult(
      runner.threadID
    );
    const monitoring = monitor.monitorConnection(
      runner.connectionID,
      stageIgnoreBeforeResult,
      monitorTimeStep
    );

    stateCallback('Waiting for monitor to be ready');
    await agent.waitRunning(monitor.threadID);

    stateCallback('Running query');
    setUserQueryIsRunning(true);
    const t0 = performance.now();
    const result = await runner.executeQuery(query, explainAnalyze);
    const t1 = performance.now();
    setUserQueryIsRunning(false);
    if (connection.cancelled) {
      connection.cancelled = false;
      stateCallback('');
      return { error: 'cancelled', tabID };
    }

    // Handling error, aborting
    if (result?.error) {
      const optimizerTrace = await runner.getOptimizerTrace();
      stateCallback('Error detected, aborting monitoring');
      await this.reset(tabID, monitor.serial);
      stateCallback('');
      return {
        result: {
          results: result.results,
          error: result.error,
          columnWidths: result.results
            ? DataProcessor.calculateRowWidths(result.results)
            : undefined,
        },
        error: result.error,
        explainAnalyzeRoot:
          explainAnalyze && result.results
            ? DataProcessor.processExplainAnalyze(result.results)
            : undefined,
        explainAnalyzeParents:
          explainAnalyze && result.results
            ? DataProcessor.parentDict
            : undefined,
        optimizerTrace: optimizerTrace.results
          ? DataProcessor.processOptimizerTrace(optimizerTrace.results)
          : undefined,
        tabID,
      };
    }

    stateCallback('Query done');
    const queryTime = t1 - t0;
    // SqlManager.log(
    //   `Query took ${queryTime} ms, required at least ${
    //     monitorTimeStep * 1000 * 2
    //   } ms`
    // );

    if (queryTime < monitorTimeStep * 1000 * 2) {
      stateCallback('Too short query, aborting monitoring');
      const optimizerTrace = await runner.getOptimizerTrace();
      await this.reset(tabID, monitor.serial);

      stateCallback('');
      return {
        result: {
          results: result.results,
          error: result.error,
          columnWidths: result.results
            ? DataProcessor.calculateRowWidths(result.results)
            : undefined,
        },
        explainAnalyzeRoot:
          explainAnalyze && result.results
            ? DataProcessor.processExplainAnalyze(result.results)
            : undefined,
        explainAnalyzeParents:
          explainAnalyze && result.results
            ? DataProcessor.parentDict
            : undefined,
        optimizerTrace: optimizerTrace.results
          ? DataProcessor.processOptimizerTrace(optimizerTrace.results)
          : undefined,
        tabID,
      };
    }

    stateCallback('Getting optimizer trace');
    const optimizerTrace = await runner.getOptimizerTrace();

    stateCallback(
      'Waiting for monitoring to finish (may take up to 5 minutes)'
    );
    await monitoring;
    stateCallback('Getting memory performance');
    const memoryPerformance = await monitor.dumpData();
    stateCallback('Getting stage times');
    const stageTimes = await monitor.getStageTimes();
    stateCallback('');
    const mPerformance = DataProcessor.processMemoryPerformance(
      memoryPerformance.results
    );
    const chartData = DataProcessor.getChartData(mPerformance);
    return {
      result: {
        results: result.results,
        error: result.error,
        columnWidths: result.results
          ? DataProcessor.calculateRowWidths(result.results)
          : undefined,
      },
      memoryPerformance: chartData,
      chartColors: DataProcessor.processChartColors(chartData),
      top3Memory: DataProcessor.processTop3ChartData(mPerformance),
      error: undefined,
      optimizerTrace: DataProcessor.processOptimizerTrace(
        optimizerTrace.results
      ),
      stageTimes: DataProcessor.processStageTimes(stageTimes.results),
      explainAnalyzeRoot:
        explainAnalyze && result.results
          ? DataProcessor.processExplainAnalyze(result.results)
          : undefined,
      explainAnalyzeParents:
        explainAnalyze && result.results ? DataProcessor.parentDict : undefined,
      tabID,
    };
  }

  async reset(tabID: string, serial?: number) {
    await this.disconnect(tabID);
    await this.connect(tabID, serial, this.connections[tabID].loginDetails);
  }

  async disconnect(tabID: string) {
    const connection = this.connections[tabID];
    if (!connection) return;
    const { agent, monitor, runner, client } = connection;

    // Kill monitor manually from agent as client.close won't interfere if monitor has called its procedure
    try {
      await agent?.killConnection(monitor?.connectionID);
      await agent?.killConnection(runner?.connectionID);
      await closeConnection(client);
      // SqlManager.log('disconnect(): Disconnected successfully');
    } catch (error) {
      console.error(error);
    }
  }

  async abortQueries(tabID: string) {
    const connection = this.connections[tabID];
    const { agent, monitor, runner } = connection;
    await agent?.killQuery(runner?.connectionID);
    await agent?.killQuery(monitor?.connectionID);
  }

  async cancelRecording(tabID: string) {
    this.connections[tabID].cancelled = true;
    await this.abortQueries(tabID);
  }

  async removeConnection(tabID: string) {
    await this.disconnect(tabID);
    delete this.connections[tabID];
    // console.log(`Connection for tab ${tabID} removed`);
  }

  async checkThreadStates(tabID: string) {
    const connection = this.connections[tabID];
    const { agent, monitor, runner } = connection;
    const runnerState = await agent?.checkThreadState(runner?.threadID || '');
    const monitorState = await agent?.checkThreadState(monitor?.threadID || '');
    SqlManager.log(
      `Runner: ${runnerState?.results?.values}\nMonitor: ${monitorState?.results?.values}`
    );
  }

  async executeQuery(query: string, tabID: string) {
    const connection = this.connections[tabID];
    const { runner } = connection;
    const result = await runner.executeQuery(query);
    return result;
  }

  static log(message: any) {
    console.log(`SqlManager: ${JSON.stringify(message)}`);
  }
}
