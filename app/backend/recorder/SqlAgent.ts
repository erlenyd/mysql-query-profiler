import { Session, Client, ResultValue } from '@mysql/xdevapi';
import { createSession, createConnection, runQuery } from '../utils/mysql';
import { LoginDetails } from '../types/LoginDetails';
import SqlMonitor from './SqlMonitor';

/**
 * A class responsible for performing actions when Runner and Monitor are busy.
 * Example actions:
 * - Aborting a query
 * - Checking whether a procedure is running
 * - Checking the health of the other sessions
 */
export default class SqlAgent {
  session?: Session;

  connectionID?: string;

  threadID?: string;

  /**
   * Connects the Agent to a session through a given client
   * @param client
   * @param database
   */
  async connect(loginDetails?: LoginDetails) {
    const client = await createConnection(loginDetails, {
      pooling: { enabled: true },
    });
    this.session = await createSession(client, loginDetails?.database);
    const missingPrivileges = await this.hasRequiredPrivileges(
      loginDetails?.database
    );
    if (missingPrivileges.length !== 0) {
      throw new Error(
        `Insufficient DB-user privileges ${missingPrivileges.join(', ')}`
      );
    }
    let resultSet = await this.executeQuery(`SELECT connection_id();`);
    this.connectionID = String(resultSet?.results?.values[0]);
    resultSet = await this.executeQuery(
      `select thread_id from performance_schema.threads where processlist_id=${this.connectionID}`
    );
    this.threadID = String(resultSet?.results?.values[0]);
    // console.log(
    //   `SqlAgent: ConnectionID: ${this.connectionID}, ThreadID: ${this.threadID}`
    // );
    return client;
  }

  /**
   * Checks if the database user has required privileges to do monitoring.
   * @param database
   * @returns missingPrivileges
   */
  async hasRequiredPrivileges(database?: string) {
    const privilegesResultSet = await this.executeQuery(`show grants`);
    const privilegesResultSetValues = privilegesResultSet.results?.values;
    return this.getMissingPriveleges(privilegesResultSetValues, database);
  }

  getMissingPriveleges = (
    privilegesResultSetValues: ResultValue[][] | undefined,
    database?: string
  ) => {
    console.log(JSON.stringify(privilegesResultSetValues));
    console.log(database);
    const privileges = privilegesResultSetValues?.map((privilege) => {
      const privilegeString = privilege[0] as string;
      const dbtable = privilegeString.substring(
        privilegeString.indexOf(' ON') + 4,
        privilegeString.indexOf(' TO')
      );
      const operations = privilegeString
        .substring(6, privilegeString.indexOf(' ON'))
        .split(', ');
      return {
        dbtable,
        operations,
      };
    });

    const hasSetupInstrumentsPrivilege = (privilege: {
      dbtable: string;
      operations: string[];
    }) => {
      if (
        privilege.dbtable === '*.*' ||
        privilege.dbtable === '`performance_schema`.*' ||
        privilege.dbtable === '`performance_schema`.`setup_instruments`'
      ) {
        if (
          privilege.operations.includes('ALL PRIVILEGES') ||
          privilege.operations.includes('UPDATE')
        ) {
          return true;
        }
        return false;
      }
      return false;
    };

    const hasSetupConsumersPrivilege = (privilege: {
      dbtable: string;
      operations: string[];
    }) => {
      if (
        privilege.dbtable === '*.*' ||
        privilege.dbtable === '`performance_schema`.*' ||
        privilege.dbtable === '`performance_schema`.`setup_consumers`'
      ) {
        if (
          privilege.operations.includes('ALL PRIVILEGES') ||
          privilege.operations.includes('UPDATE')
        ) {
          return true;
        }
        return false;
      }
      return false;
    };

    const hasSelectEventsStagesHistoryLongPrivilege = (privilege: {
      dbtable: string;
      operations: string[];
    }) => {
      if (
        privilege.dbtable === '*.*' ||
        privilege.dbtable === '`performance_schema`.*' ||
        privilege.dbtable ===
          '`performance_schema`.`events_stages_history_long`'
      ) {
        if (
          privilege.operations.includes('ALL PRIVILEGES') ||
          privilege.operations.includes('SELECT')
        ) {
          return true;
        }
        return false;
      }
      return false;
    };

    const hasSelectMemorySummarybyThreadByEventPrivilege = (privilege: {
      dbtable: string;
      operations: string[];
    }) => {
      if (
        privilege.dbtable === '*.*' ||
        privilege.dbtable === '`performance_schema`.*' ||
        privilege.dbtable ===
          '`performance_schema`.`memory_summary_by_thread_by_event`'
      ) {
        if (
          privilege.operations.includes('ALL PRIVILEGES') ||
          privilege.operations.includes('SELECT')
        ) {
          return true;
        }
        return false;
      }
      return false;
    };

    const hasSelectThreadIdPrivilege = (privilege: {
      dbtable: string;
      operations: string[];
    }) => {
      if (
        privilege.dbtable === '*.*' ||
        privilege.dbtable === '`performance_schema`.*' ||
        privilege.dbtable === '`performance_schema`.`threads`'
      ) {
        if (
          privilege.operations.includes('ALL PRIVILEGES') ||
          privilege.operations.includes('SELECT')
        ) {
          return true;
        }
        return false;
      }
      return false;
    };

    const hasCurrentDBPrivileges = (privilege: {
      dbtable: string;
      operations: string[];
    }) => {
      if (
        privilege.dbtable === '*.*' ||
        privilege.dbtable === `\`${database}\`.*`
      ) {
        if (
          privilege.operations.includes('ALL PRIVILEGES') ||
          (privilege.operations.includes('SELECT') &&
            privilege.operations.includes('INSERT') &&
            privilege.operations.includes('CREATE') &&
            privilege.operations.includes('CREATE ROUTINE') &&
            privilege.operations.includes('EXECUTE') &&
            privilege.operations.includes('DROP'))
        ) {
          return true;
        }
        return false;
      }
      return false;
    };

    const hasPrivileges: { [key: string]: boolean | undefined } = {};

    hasPrivileges[
      'UPDATE ON `performance_schema`.`setup_instruments`'
    ] = privileges?.some(hasSetupInstrumentsPrivilege);
    hasPrivileges[
      'UPDATE ON `performance_schema`.`setup_consumers`'
    ] = privileges?.some(hasSetupConsumersPrivilege);
    hasPrivileges[
      'SELECT ON `performance_schema`.`threads`'
    ] = privileges?.some(hasSelectThreadIdPrivilege);
    hasPrivileges[
      'SELECT ON `performance_schema`.`memory_summary_by_thread_by_event`'
    ] = privileges?.some(hasSelectMemorySummarybyThreadByEventPrivilege);
    hasPrivileges[
      'SELECT ON `performance_schema`.`events_stages_history_long`'
    ] = privileges?.some(hasSelectEventsStagesHistoryLongPrivilege);
    hasPrivileges[
      `SELECT, INSERT, CREATE, CREATE ROUTINE, EXECUTE, DROP ON \`${database}\`.*`
    ] = privileges?.some(hasCurrentDBPrivileges);

    const missingPrivileges: string[] = [];

    Object.keys(hasPrivileges).forEach((privilege) => {
      if (!hasPrivileges[privilege]) {
        missingPrivileges.push(privilege);
      }
    });
    console.log(missingPrivileges);
    return missingPrivileges;
  };

  /**
   * Waits for a session with the given connectionID to begin running the specified query
   * @param connectionID
   * @param query
   */
  async waitRunning(threadID: string) {
    await this.executeQuery('DROP PROCEDURE IF EXISTS wait_running;');
    await this.executeQuery(`
    CREATE PROCEDURE wait_running(
      IN thd_id BIGINT UNSIGNED
    )
    BEGIN
      DECLARE state VARCHAR(16);
      REPEAT
        SET state = (SELECT PROCESSLIST_COMMAND FROM performance_schema.threads WHERE THREAD_ID=thd_id);
      UNTIL state = 'Query' END REPEAT;
    END
    `);
    await this.executeQuery(`CALL wait_running(${threadID});`);
  }

  async checkThreadState(threadID: string) {
    return this.executeQuery(
      `SELECT PROCESSLIST_COMMAND FROM performance_schema.threads WHERE THREAD_ID=${threadID}`
    );
  }

  async forceSleepMonitor(
    sqlMonitor: SqlMonitor,
    client?: Client,
    database?: string
  ) {
    await this.killQuery(sqlMonitor.connectionID || '');
    await sqlMonitor.connect(client, database);
  }

  async killQuery(connectionID?: string) {
    if (!connectionID) return { results: undefined, error: undefined };
    return this.executeQuery(`KILL QUERY ${connectionID}`);
  }

  async killConnection(connectionID?: string) {
    if (!connectionID) return { results: undefined, error: undefined };
    return this.executeQuery(`KILL CONNECTION ${connectionID};`);
  }

  async executeQuery(query: string) {
    return runQuery(query, this.session);
  }
}
