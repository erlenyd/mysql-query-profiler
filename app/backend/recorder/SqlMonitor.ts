import { Session, Client } from '@mysql/xdevapi';
import { createSession, runQuery } from '../utils/mysql';

/**
 * A class responsible for monitoring the resource usage of another session
 */
export default class SqlMonitor {
  session?: Session;

  connectionID?: string;

  threadID?: string;

  serial?: number;

  async connect(client?: Client, database?: string, serial?: number) {
    // SqlMonitor.log('Connecting');
    this.session = await createSession(client, database);
    let resultSet = await this.executeQuery(`SELECT connection_id();`);
    this.connectionID = String(resultSet?.results?.values[0]);
    resultSet = await this.executeQuery(
      `SELECT thread_id FROM performance_schema.threads WHERE processlist_id=${this.connectionID}`
    );
    this.threadID = String(resultSet?.results?.values[0]);
    this.serial = serial;
    await this.enableStageLogging();
    await this.createMonitorProcedure(serial);
  }

  async enableStageLogging() {
    // Allow logging of stages
    await this.executeQuery(
      `UPDATE performance_schema.setup_instruments SET enabled="YES", timed="yes" WHERE name LIKE "stage/%";`
    );
    await this.executeQuery(
      `UPDATE performance_schema.setup_consumers SET enabled="YES" WHERE name LIKE "events_stages%";`
    );
  }

  async createMonitorProcedure(serial?: number) {
    await this.executeQuery(
      `DROP PROCEDURE IF EXISTS monitor_connection${serial};`
    );
    // Create monitoring procedure
    await this.executeQuery(
      `CREATE PROCEDURE monitor_connection${serial}(
        IN conn_id BIGINT UNSIGNED,
        IN stage_ignore_before BIGINT UNSIGNED,
        IN timestep_size FLOAT
      )
      BEGIN
        DECLARE thd_ID BIGINT UNSIGNED;
        DECLARE state VARCHAR(16);
        DECLARE stage_min_ts BIGINT UNSIGNED;

        SET thd_id = (SELECT thread_id FROM performance_schema.threads WHERE processlist_id=conn_id);

        DROP TABLE IF EXISTS monitoring_data${serial};
        CREATE TABLE monitoring_data${serial} (
            TS DATETIME(6),
            THREAD_ID BIGINT UNSIGNED,
            EVENT_NAME VARCHAR(128),
            COUNT_ALLOC BIGINT UNSIGNED,
            COUNT_FREE BIGINT UNSIGNED,
            SUM_NUMBER_OF_BYTES_ALLOC BIGINT UNSIGNED,
            SUM_NUMBER_OF_BYTES_FREE BIGINT UNSIGNED,
            LOW_COUNT_USED BIGINT,
            CURRENT_COUNT_USED BIGINT,
            HIGH_COUNT_USED BIGINT,
            LOW_NUMBER_OF_BYTES_USED BIGINT,
            CURRENT_NUMBER_OF_BYTES_USED BIGINT,
            HIGH_NUMBER_OF_BYTES_USED BIGINT
        );

        REPEAT
          SET state = (SELECT PROCESSLIST_COMMAND FROM performance_schema.threads WHERE THREAD_ID=thd_id);
        UNTIL state = 'Query' END REPEAT;

        REPEAT
          SET state = (SELECT PROCESSLIST_COMMAND FROM performance_schema.threads WHERE THREAD_ID=thd_id);
          INSERT INTO monitoring_data${serial}
            SELECT
              NOW(6) AS 'TS',
              THREAD_ID,
              EVENT_NAME,
              COUNT_ALLOC,
              COUNT_FREE,
              SUM_NUMBER_OF_BYTES_ALLOC,
              SUM_NUMBER_OF_BYTES_FREE,
              LOW_COUNT_USED,
              CURRENT_COUNT_USED,
              HIGH_COUNT_USED,
              LOW_NUMBER_OF_BYTES_USED,
              CURRENT_NUMBER_OF_BYTES_USED,
              HIGH_NUMBER_OF_BYTES_USED
            FROM performance_schema.memory_summary_by_thread_by_event_name
            WHERE THREAD_ID = thd_id;
          DO SLEEP(timestep_size);
        UNTIL state = 'Sleep' END REPEAT;

        SET stage_min_ts = (SELECT MIN(timer_start) FROM performance_schema.events_stages_history_long WHERE THREAD_ID=thd_id AND timer_start > stage_ignore_before);

        DROP TABLE IF EXISTS monitoring_stages${serial};
        CREATE TABLE monitoring_stages${serial} AS
          SELECT
            EVENT_NAME,
            SOURCE,
            timer_start,
            timer_end,
            (timer_start - stage_min_ts) / 1000000000000 AS start,
            (timer_end - stage_min_ts) / 1000000000000 AS end
          FROM performance_schema.events_stages_history_long
          WHERE THREAD_ID = thd_id AND timer_start > stage_ignore_before ORDER BY timer_start;
      END`
    );
  }

  async getStageIgnoreBeforeResult(threadID: string) {
    const stageIgnoreBeforeResult = await this.executeQuery(
      `SELECT MAX(TIMER_END) FROM performance_schema.events_stages_history_long WHERE THREAD_ID=${threadID};`
    );
    return Number(stageIgnoreBeforeResult.results?.values[0]);
  }

  async monitorConnection(
    connectionID: string,
    stageIgnoreBeforeResult: number,
    timestep: number
  ) {
    // SqlMonitor.log(
    //   `Monitoring with timestep of ${timestep} seconds, connectionID ${connectionID}`
    // );
    await this.executeQuery(
      `call monitor_connection${this.serial}(${connectionID}, ${stageIgnoreBeforeResult}, ${timestep});`
    );
  }

  async dumpData() {
    await this.executeQuery(
      `SET @min_ts = (SELECT UNIX_TIMESTAMP(MIN(TS)) FROM monitoring_data${this.serial});`
    );

    const data = await this.executeQuery(
      `SELECT UNIX_TIMESTAMP(TS) - @min_ts, EVENT_NAME, CURRENT_NUMBER_OF_BYTES_USED FROM monitoring_data${this.serial} ORDER BY 1;`
    );
    await this.executeQuery(
      `DROP TABLE IF EXISTS monitoring_data${this.serial};`
    );
    return data;
  }

  async getStageTimes() {
    const stageTimesRaw = await this.executeQuery(
      `select event_name, source, start, end from monitoring_stages${this.serial};`
    );
    await this.executeQuery(
      `DROP TABLE IF EXISTS monitoring_stages${this.serial};`
    );
    return stageTimesRaw;
  }

  async executeQuery(query: string) {
    return runQuery(query, this.session);
  }

  static log(message: unknown) {
    console.log(`SqlMonitor: ${String(message)}`);
  }
}
