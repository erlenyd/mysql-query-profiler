import SqlManager from './SqlManager';

interface Singleton {
  instance: SqlManager | undefined;
  createInstance: () => SqlManager;
  getInstance: () => SqlManager;
}

/**
 * An object responsible for ensuring only one instance of the SqlManager is made
 */
const SqlManagerSingleton: Singleton = {
  instance: undefined,

  createInstance: () => {
    const manager = new SqlManager();
    return manager;
  },

  getInstance: () => {
    if (!SqlManagerSingleton.instance) {
      SqlManagerSingleton.instance = SqlManagerSingleton.createInstance();
    }
    return SqlManagerSingleton.instance;
  },
};

export default SqlManagerSingleton;
