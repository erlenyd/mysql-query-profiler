/* eslint-disable @typescript-eslint/no-explicit-any */
import { Result } from '../utils/mysql';

export interface QueryOutput {
  results?: Result;
  error?: any;
  columnWidths?: number[];
}
