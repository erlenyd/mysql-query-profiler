import { URI } from '@mysql/xdevapi';

export interface LoginDetails extends URI {
  database?: string;
  savePassword?: boolean;
}
