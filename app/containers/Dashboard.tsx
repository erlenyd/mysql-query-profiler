import React, { useContext } from 'react';
import { makeStyles } from '@material-ui/core';
import PanelGroup from 'react-panelgroup';
import { v4 as uuid } from 'uuid';
import QueryRecorder from '../components/dashboard/QueryRecorder';
import ResultView from './ResultView';
import Recordings from '../components/dashboard/Recordings';
import ErrorView from '../components/dashboard/ErrorView';
import IDInfo from '../components/dashboard/IDInfo';
import { TabContext } from '../context/TabContext';
import { GlobalSnackbarContext } from '../context/GlobalSnackbarContext';
import { Recording } from '../types/Recording';
import { RecordingUpdate } from '../types/RecordingUpdate';
import { RecordingListItem } from '../types/RecordingListItem';

const useStyles = makeStyles((theme) => ({
  wrapper: {
    minHeight: '100%',
    height: '100%',
    maxHeight: '100vh',
  },
  column: {
    display: 'flex',
    flexFlow: 'column nowrap',
    maxHeight: window.innerHeight - 56,
    height: '100%',
    padding: `${theme.spacing(1)}px ${theme.spacing(2)}px`,
    width: '100%',
    overflowY: 'auto',
  },
  item: {
    margin: `${theme.spacing(1)}px ${theme.spacing(0)}`,
  },
}));

/**
 * The main part of the app, allowing users to record queries and view and manipulate recordings
 */
export default function Dashboard() {
  const { dispatch } = useContext(TabContext);
  const globalSnackbarContext = useContext(GlobalSnackbarContext);

  function handleNewRecording(recording: RecordingUpdate) {
    const {
      result,
      error,
      explainAnalyze,
      query,
      label,
      elapsed,
      color,
      tabID,
    } = recording;
    const recordingID = uuid();
    const tempChartData = result?.memoryPerformance || [];
    const chartDataLength = tempChartData?.length || 0;
    const totalTime =
      tempChartData[chartDataLength - 1]?.relative_time * 1000 || NaN;
    const newRecording: Recording = {
      queryOutput: result?.result || {},
      error,
      stageTimes: result?.stageTimes,
      chartData: result?.memoryPerformance,
      optimizerTrace: result?.optimizerTrace,
      chartColors: result?.chartColors,
      top3Memory: result?.top3Memory,
      explainAnalyze:
        explainAnalyze || query.toLowerCase().includes('explain analyze'),
      explainAnalyzeRoot: result?.explainAnalyzeRoot,
      explainAnalyzeParents: result?.explainAnalyzeParents,
      label,
      uuid: recordingID,
      isSaved: false,
      query,
      elapsed: totalTime || elapsed || -1,
    };
    let formattedLabel;
    formattedLabel = error ? 'Error' : '';
    formattedLabel = label.length ? label : formattedLabel;
    const newListItem: RecordingListItem = {
      query,
      elapsed: totalTime || elapsed || -1,
      label: formattedLabel,
      color,
      uuid: recordingID,
      viewing: true,
      isSaved: false,
    };
    dispatch({
      type: 'ADD_RECORDING',
      payload: {
        tabID: tabID || '',
        recording: newRecording,
        recordingListItem: newListItem,
        recordingID,
      },
    });
    globalSnackbarContext?.setPopupMessage(
      `Recording finished in ${(elapsed || -1).toFixed(1)} ms`
    );
    globalSnackbarContext?.setPopupSeverity('success');
    globalSnackbarContext?.setPopupOpen(true);
  }

  const classes = useStyles();

  return (
    <div className={classes.wrapper}>
      <PanelGroup
        borderColor="#404040"
        spacing={16}
        panelWidths={[
          { minSize: 200, size: 700, resize: 'dynamic' },
          { minSize: 200, resize: 'dynamic' },
        ]}
      >
        <div className={classes.column}>
          <div className={classes.item}>
            <IDInfo />
          </div>
          <div className={classes.item}>
            <QueryRecorder onNewRecording={handleNewRecording} />
          </div>
          <div className={classes.item}>
            <ErrorView />
          </div>
          <div className={classes.item}>
            <Recordings />
          </div>
        </div>
        <div className={classes.column}>
          <ResultView />
        </div>
      </PanelGroup>
    </div>
  );
}
