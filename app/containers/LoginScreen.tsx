import React, { useEffect, useState, useContext } from 'react';
import { Typography, makeStyles } from '@material-ui/core';
import Login from '../components/login/Login';
import FileIO from '../backend/utils/FileIO';
import { TabContext } from '../context/TabContext';

const useStyles = makeStyles((theme) => ({
  wrapper: {
    maxWidth: '100vw',
    height: '100vh',
    overflowY: 'auto',
  },
  content: {
    display: 'flex',
    flexFlow: 'column nowrap',
    alignItems: 'center',
    justifyContent: 'space-between',
    padding: `${theme.spacing(6)}px ${theme.spacing(16)}px`,
  },
  title: {
    marginBottom: theme.spacing(6),
    textAlign: 'center',
  },
}));

/**
 * The welcome screen prompting the user to log in to a database. Loads the recent login details for prefill
 */
export default function LoginScreen() {
  const [loading, setLoading] = useState(true);

  const { state: tabState, dispatch } = useContext(TabContext);
  const { activeTabID } = tabState;

  useEffect(() => {
    FileIO.loadLoginDetails()
      .then((response) => {
        if (response) {
          dispatch({
            type: 'SET_LOGIN_DETAILS',
            payload: {
              id: activeTabID,
              loginDetails: response,
            },
          });
        }
        setLoading(false);
        return true;
      })
      .catch((error) => console.error(error));
  }, []);

  const classes = useStyles();

  return (
    <div className={classes.wrapper}>
      {!loading && (
        <div className={classes.content}>
          <Typography className={classes.title} variant="h3">
            Welcome to MySQL query profiler!
          </Typography>
          <Login />
        </div>
      )}
    </div>
  );
}
