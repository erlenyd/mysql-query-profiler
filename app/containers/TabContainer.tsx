import React, { useContext, useEffect } from 'react';
import { Switch, Route, useLocation, useHistory } from 'react-router-dom';
import routes from '../routes';
import LoginScreen from './LoginScreen';
import Dashboard from './Dashboard';
import TabBar from '../components/tabs/TabBar';
import { TabContext } from '../context/TabContext';
import { GlobalSnackbarProvider } from '../context/GlobalSnackbarContext';
import GlobalSnackbar from '../components/dashboard/GlobalSnackbar';

/**
 * The container for all the tabs and the active tab content
 */
export default function TabContainer() {
  const { state: tabState } = useContext(TabContext);
  const { activeTabID, manager } = tabState;

  const location = useLocation();
  const history = useHistory();

  function switchToTab(tabID: string, isNew = false) {
    const connection = manager.connections[tabID];
    if (
      isNew ||
      (location.pathname === routes.dashboard && !connection?.client)
    ) {
      history.push(routes.login);
    } else if (connection?.client && location.pathname !== routes.dashboard) {
      history.push(routes.dashboard);
    }
  }

  // Need to ensure we are in a legal state at the start (no unconnected dashboard)
  useEffect(() => {
    switchToTab(activeTabID);
  }, [activeTabID]);

  return (
    <GlobalSnackbarProvider>
      <GlobalSnackbar />
      <TabBar />
      <Switch>
        <Route exact path={routes.login}>
          <LoginScreen />
        </Route>
        <Route path={routes.dashboard}>
          <Dashboard />
        </Route>
      </Switch>
    </GlobalSnackbarProvider>
  );
}
