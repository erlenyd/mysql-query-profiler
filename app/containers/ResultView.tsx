import {
  makeStyles,
  Checkbox,
  FormControlLabel,
  Paper,
  Typography,
  Tooltip,
} from '@material-ui/core';
import React, { useContext, useState } from 'react';
import ExplainAnalyze from '../components/results/ExplainAnalyze';
import MemoryPerformance from '../components/results/MemoryPerformance/MemoryPerformance';
import OptimizerTrace from '../components/results/OptimizerTrace';
import ResultGrid from '../components/results/ResultGrid';
import { TabContext } from '../context/TabContext';
import { MemoryPerformanceProvider } from '../context/MemoryPerformanceContext';

const useStyles = makeStyles((theme) => ({
  wrapper: {
    display: 'flex',
    flexFlow: 'column nowrap',
    alignContent: 'center',
    paddingBottom: 300,
  },
  item: {
    margin: `${theme.spacing(1)}px ${theme.spacing(0)}`,
  },
  title: {
    textOverflow: 'ellipsis',
    whiteSpace: 'nowrap',
    overflow: 'hidden',
  },
}));

/**
 * The results from a recording. The user can toggle what to be shown. Its state is not exclusive to each tab
 */
export default function ResultView() {
  const [showMemoryChart, setShowMemoryChart] = useState(true);
  const [showOptimizerTrace, setShowOptimizerTrace] = useState(true);
  const [showResultTable, setShowResultTable] = useState(false);
  const [showExplainAnalyze, setShowExplainAnalyze] = useState(true);

  const { state: tabState } = useContext(TabContext);
  const { activeTabID, tabs } = tabState;
  const tab = tabs[activeTabID];
  const recordings = tab?.recordings || {};
  const activeRecordingID = tab?.activeRecordingID || '';

  const classes = useStyles();

  return (
    <div className={classes.wrapper} style={{ minWidth: '200px' }}>
      <div className={classes.item}>
        <Tooltip title={recordings[activeRecordingID]?.label || ''}>
          <Typography variant="h4" className={classes.title}>
            {`Results for recording ${
              recordings[activeRecordingID]?.label || ''
            }`}
          </Typography>
        </Tooltip>
      </div>
      <div>
        <FormControlLabel
          label="Show result table"
          control={
            <Checkbox
              color="primary"
              checked={showResultTable}
              onChange={(event) => setShowResultTable(event.target.checked)}
            />
          }
        />
        <FormControlLabel
          label="Show memory performance"
          control={
            <Checkbox
              color="primary"
              checked={showMemoryChart}
              onChange={(event) => setShowMemoryChart(event.target.checked)}
            />
          }
        />
        <FormControlLabel
          label="Show optimizer trace"
          control={
            <Checkbox
              color="primary"
              checked={showOptimizerTrace}
              onChange={(event) => setShowOptimizerTrace(event.target.checked)}
            />
          }
        />
        {recordings[activeRecordingID]?.explainAnalyze && (
          <FormControlLabel
            label="Show explain analyze"
            control={
              <Checkbox
                color="primary"
                checked={showExplainAnalyze}
                onChange={(event) =>
                  setShowExplainAnalyze(event.target.checked)
                }
              />
            }
          />
        )}
      </div>
      {showResultTable && (
        <Paper className={classes.item}>
          <ResultGrid />
        </Paper>
      )}
      {showMemoryChart && (
        <Paper
          className={classes.item}
          style={{ paddingBottom: 8, paddingTop: 8 }}
        >
          {/* <MemoryChart /> */}
          <MemoryPerformanceProvider>
            <MemoryPerformance />
          </MemoryPerformanceProvider>
        </Paper>
      )}
      {showOptimizerTrace && recordings[activeRecordingID]?.optimizerTrace && (
        <Paper className={classes.item} style={{ padding: 8 }}>
          <OptimizerTrace />
        </Paper>
      )}
      {showExplainAnalyze &&
        recordings[activeRecordingID]?.explainAnalyze &&
        recordings[activeRecordingID]?.explainAnalyzeRoot && (
          <Paper className={classes.item} style={{ padding: 8 }}>
            <ExplainAnalyze />
          </Paper>
        )}
    </div>
  );
}
