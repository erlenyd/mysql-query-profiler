/* eslint-disable no-case-declarations */
import { keys, valuesIn } from 'lodash';
import React, { createContext, useReducer } from 'react';
import { v4 as uuid } from 'uuid';
import SqlManager from '../backend/recorder/SqlManager';
import SqlManagerSingleton from '../backend/recorder/SqlManagerSingleton';
import { LoginDetails } from '../backend/types/LoginDetails';
import { DisplayTab } from '../types/DisplayTab';
import { Recording } from '../types/Recording';
import { RecordingListItem } from '../types/RecordingListItem';
import { Tab } from '../types/Tab';

interface ChildrenType {
  children: React.ReactNode;
}

export interface RecordingPayload {
  tabID: string;
  recordingID?: string;
  recording?: Recording;
  recordingListItem?: RecordingListItem;
  value?: string | number | boolean;
}

export interface DisplayTabLabelPayload {
  id: string;
  label: string;
}

export interface TabLoginDetailsPayload {
  id: string;
  loginDetails: LoginDetails;
}

export interface TabState {
  activeTabID: string;
  createdTabs: number;
  tabs: { [key: string]: Tab };
  displayTabs: { [key: string]: DisplayTab };
  manager: SqlManager;
}

export interface TabAction {
  type: // Recording actions
  | 'ADD_RECORDING'
    | 'DELETE_RECORDING'
    | 'SET_RECORDING_LABEL'
    | 'SET_RECORDING_COLOR'
    | 'SET_ACTIVE_RECORDING'
    // Queryrecorder state actions
    | 'SET_RECORDING_STATE'
    | 'SET_INPUT_QUERY'
    | 'SET_SHOW_ADVANCED_OPTIONS'
    | 'SET_EXPLAIN_ANALYZE'
    | 'SET_TIMESTEP'
    | 'SET_RECORDING_INPUT_LABEL'
    | 'SET_RECORDING_INPUT_COLOR'
    | 'SET_QUERY_RUNNING'
    | 'SET_RECORDING_RUNNING'
    // Tab actions
    | 'SET_DISPLAYTAB_LABEL'
    | 'SET_LOGIN_DETAILS'
    | 'ADD_TAB'
    | 'SET_ACTIVE_TAB'
    | 'DELETE_TAB';
  payload?:
    | Tab
    | DisplayTab
    | string
    | boolean
    | number
    | DisplayTabLabelPayload
    | RecordingPayload
    | TabLoginDetailsPayload;
}

const firstTabID = uuid();

const initialState: TabState = {
  activeTabID: firstTabID,
  createdTabs: 1,
  tabs: {
    [firstTabID]: {
      tabID: firstTabID,
      serial: 1,
      recordings: {},
      recordingListItems: {},
      recordingRunning: false,
      queryRunning: false,
      inputQuery: '',
      inputLabel: '',
      inputColor: '',
      explainAnalyze: false,
      timestep: 0.1,
      showAdvancedOptions: false,
      recordingState: '',
    },
  },
  displayTabs: {
    [firstTabID]: { tabID: firstTabID, tabLabel: 'New connection' },
  },
  manager: SqlManagerSingleton.getInstance(),
};

const tabReducer = (tabState: TabState, action: TabAction): TabState => {
  switch (action.type) {
    case 'ADD_TAB':
      const tabID = uuid();
      const tabLabel = 'New connection';
      const changedDisplayTabs = { ...tabState.displayTabs };
      changedDisplayTabs[tabID] = {
        tabID,
        tabLabel,
      };
      const changedTabs = { ...tabState.tabs };
      changedTabs[tabID] = {
        tabID,
        serial: tabState.createdTabs + 1,
        recordings: {},
        recordingListItems: {},
        recordingRunning: false,
        queryRunning: false,
        inputQuery: '',
        inputLabel: '',
        inputColor: '',
        explainAnalyze: false,
        timestep: 0.1,
        showAdvancedOptions: false,
        recordingState: '',
      };
      return {
        ...tabState,
        tabs: changedTabs,
        displayTabs: changedDisplayTabs,
        activeTabID: tabID,
      };
    case 'DELETE_TAB':
      if (typeof action.payload === 'string') {
        const id = action.payload;
        const tabs = { ...tabState.tabs };
        const displayTabs = { ...tabState.displayTabs };
        delete tabs[id];
        delete displayTabs[id];
        const ids = keys(displayTabs);
        let newIndex = ids.length - 1; // First assuming the tab was the rightmost one
        if (ids.length < 1) return tabState; // We do not delete the last tab (for now)
        if (newIndex < 0) newIndex = 0; // Avoid index out of bounds
        const activeTabID = ids[newIndex];
        tabState.manager.removeConnection(id);
        return { ...tabState, activeTabID, tabs, displayTabs };
      }
      return tabState;
    case 'SET_ACTIVE_TAB':
      if (typeof action.payload === 'string') {
        const activeTabID = action.payload;
        return { ...tabState, activeTabID };
      }
      return tabState;
    case 'SET_DISPLAYTAB_LABEL':
      if (
        (action.payload as DisplayTabLabelPayload)?.id &&
        (action.payload as DisplayTabLabelPayload)?.label !== undefined
      ) {
        const { id, label } = action.payload as DisplayTabLabelPayload;
        const displayTabs = { ...tabState.displayTabs };
        displayTabs[id].tabLabel = label;
        return { ...tabState, displayTabs };
      }
      return tabState;
    case 'ADD_RECORDING':
      if (
        (action.payload as RecordingPayload).recording &&
        (action.payload as RecordingPayload).recordingListItem &&
        (action.payload as RecordingPayload).recordingID &&
        (action.payload as RecordingPayload).tabID
      ) {
        const {
          tabID: id,
          recording,
          recordingListItem,
          recordingID,
        } = action.payload as RecordingPayload;
        if (!recording || !recordingListItem || !recordingID) return tabState;
        const tabs = { ...tabState.tabs };
        const tab = tabs[id];
        const recordings = { ...tab.recordings };
        const recordingListItems = { ...tab.recordingListItems };
        recordings[recordingID] = recording;
        recordingListItems[recordingID] = recordingListItem;
        tab.recordings = recordings;
        tab.recordingListItems = recordingListItems;
        tab.activeRecordingID = recordingID;
        tab.inputLabel = '';
        tab.recordingRunning = false;
        tab.queryRunning = false;
        tab.explainAnalyze = false;
        tabs[id] = tab;
        return { ...tabState, tabs };
      }
      return tabState;
    case 'SET_ACTIVE_RECORDING':
      if (
        (action.payload as RecordingPayload).recordingID &&
        (action.payload as RecordingPayload).tabID
      ) {
        const { tabID: id, recordingID } = action.payload as RecordingPayload;
        const tabs = { ...tabState.tabs };
        const tab = tabs[id];
        tab.activeRecordingID = recordingID;
        tabs[id] = tab;
        return { ...tabState, tabs };
      }
      return tabState;
    case 'SET_RECORDING_STATE':
      if (
        (action.payload as RecordingPayload).tabID &&
        (action.payload as RecordingPayload).value !== undefined
      ) {
        const { tabID: id, value } = action.payload as RecordingPayload;
        if (typeof value !== 'string') return tabState;
        const tabs = { ...tabState.tabs };
        const tab = tabs[id];
        tab.recordingState = value;
        tabs[id] = tab;
        return { ...tabState, tabs };
      }
      return tabState;
    case 'SET_INPUT_QUERY':
      if (
        (action.payload as RecordingPayload).tabID &&
        (action.payload as RecordingPayload).value !== undefined
      ) {
        const { tabID: id, value } = action.payload as RecordingPayload;
        if (typeof value !== 'string') return tabState;
        const tabs = { ...tabState.tabs };
        const tab = tabs[id];
        tab.inputQuery = value;
        tabs[id] = tab;
        return { ...tabState, tabs };
      }
      return tabState;
    case 'SET_SHOW_ADVANCED_OPTIONS':
      if (
        (action.payload as RecordingPayload).tabID &&
        (action.payload as RecordingPayload).value !== undefined
      ) {
        const { tabID: id, value } = action.payload as RecordingPayload;
        if (typeof value !== 'boolean') return tabState;
        const tabs = { ...tabState.tabs };
        const tab = tabs[id];
        tab.showAdvancedOptions = value;
        tabs[id] = tab;
        return { ...tabState, tabs };
      }
      return tabState;
    case 'SET_EXPLAIN_ANALYZE':
      if (
        (action.payload as RecordingPayload).tabID &&
        (action.payload as RecordingPayload).value !== undefined
      ) {
        const { tabID: id, value } = action.payload as RecordingPayload;
        if (typeof value !== 'boolean') return tabState;
        const tabs = { ...tabState.tabs };
        const tab = tabs[id];
        tab.explainAnalyze = value;
        tabs[id] = tab;
        return { ...tabState, tabs };
      }
      return tabState;
    case 'SET_TIMESTEP':
      if (
        (action.payload as RecordingPayload).tabID &&
        (action.payload as RecordingPayload).value !== undefined
      ) {
        const { tabID: id, value } = action.payload as RecordingPayload;
        if (typeof value !== 'number') return tabState;
        const tabs = { ...tabState.tabs };
        const tab = tabs[id];
        tab.timestep = value;
        tabs[id] = tab;
        return { ...tabState, tabs };
      }
      return tabState;
    case 'SET_RECORDING_INPUT_LABEL':
      if (
        (action.payload as RecordingPayload).tabID &&
        (action.payload as RecordingPayload).value !== undefined
      ) {
        const { tabID: id, value } = action.payload as RecordingPayload;
        if (typeof value !== 'string') return tabState;
        const tabs = { ...tabState.tabs };
        const tab = tabs[id];
        tab.inputLabel = value;
        tabs[id] = tab;
        return { ...tabState, tabs };
      }
      return tabState;
    case 'SET_RECORDING_INPUT_COLOR':
      if (
        (action.payload as RecordingPayload).tabID &&
        (action.payload as RecordingPayload).value !== undefined
      ) {
        const { tabID: id, value } = action.payload as RecordingPayload;
        if (typeof value !== 'string') return tabState;
        const tabs = { ...tabState.tabs };
        const tab = tabs[id];
        tab.inputColor = value;
        tabs[id] = tab;
        return { ...tabState, tabs };
      }
      return tabState;
    case 'SET_QUERY_RUNNING':
      if (
        (action.payload as RecordingPayload).tabID &&
        (action.payload as RecordingPayload).value !== undefined
      ) {
        const { tabID: id, value } = action.payload as RecordingPayload;
        if (typeof value !== 'boolean') return tabState;
        const tabs = { ...tabState.tabs };
        const tab = tabs[id];
        tab.queryRunning = value;
        tabs[id] = tab;
        return { ...tabState, tabs };
      }
      return tabState;
    case 'SET_RECORDING_RUNNING':
      if (
        (action.payload as RecordingPayload).tabID &&
        (action.payload as RecordingPayload).value !== undefined
      ) {
        const { tabID: id, value } = action.payload as RecordingPayload;
        if (typeof value !== 'boolean') return tabState;
        const tabs = { ...tabState.tabs };
        const tab = tabs[id];
        tab.recordingRunning = value;
        tabs[id] = tab;
        return { ...tabState, tabs };
      }
      return tabState;
    case 'SET_RECORDING_LABEL':
      if (
        (action.payload as RecordingPayload).tabID &&
        (action.payload as RecordingPayload).recordingID &&
        (action.payload as RecordingPayload).value !== undefined
      ) {
        const {
          tabID: id,
          value,
          recordingID,
        } = action.payload as RecordingPayload;
        if (typeof value !== 'string' || !recordingID) return tabState;
        const tabs = { ...tabState.tabs };
        const tab = tabs[id];
        const recordings = { ...tab.recordings };
        const recordingListItems = { ...tab.recordingListItems };
        recordings[recordingID].label = value;
        recordingListItems[recordingID].label = value;
        tab.recordings = recordings;
        tab.recordingListItems = recordingListItems;
        tabs[id] = tab;
        return { ...tabState, tabs };
      }
      return tabState;
    case 'SET_RECORDING_COLOR':
      if (
        (action.payload as RecordingPayload).tabID &&
        (action.payload as RecordingPayload).recordingID &&
        (action.payload as RecordingPayload).value !== undefined
      ) {
        const {
          tabID: id,
          value,
          recordingID,
        } = action.payload as RecordingPayload;
        if (typeof value !== 'string' || !recordingID) return tabState;
        const tabs = { ...tabState.tabs };
        const tab = tabs[id];
        const recordingListItems = { ...tab.recordingListItems };
        recordingListItems[recordingID].color = value;
        tab.recordingListItems = recordingListItems;
        tabs[id] = tab;
        return { ...tabState, tabs };
      }
      return tabState;
    case 'DELETE_RECORDING':
      if (
        (action.payload as RecordingPayload).tabID &&
        (action.payload as RecordingPayload).recordingID
      ) {
        const { tabID: id, recordingID } = action.payload as RecordingPayload;
        if (!recordingID) return tabState;
        const tabs = { ...tabState.tabs };
        const tab = tabs[id];
        const recordings = { ...tab.recordings };
        const recordingListItems = { ...tab.recordingListItems };
        delete recordings[recordingID];
        delete recordingListItems[recordingID];
        const listItemsArray = valuesIn(tab.recordingListItems);
        const index = listItemsArray.findIndex(
          (item) => item.uuid === recordingID
        );
        let activeRecordingID;
        if (recordingID === tab?.activeRecordingID) {
          let newIndex = index - 1;
          if (newIndex < 0) {
            newIndex = 0;
          }
          if (listItemsArray.length < 1) {
            newIndex = -1;
          } else if (listItemsArray.length === 1) {
            newIndex = 0;
          }
          if (newIndex !== -1 && listItemsArray[newIndex]) {
            activeRecordingID =
              newIndex !== -1 ? listItemsArray[newIndex].uuid : undefined;
          }
        }
        tab.recordingListItems = recordingListItems;
        tab.recordings = recordings;
        tab.activeRecordingID = activeRecordingID;
        tabs[id] = tab;
        return { ...tabState, tabs };
      }
      return tabState;
    case 'SET_LOGIN_DETAILS':
      if (
        (action.payload as TabLoginDetailsPayload).id &&
        (action.payload as TabLoginDetailsPayload).loginDetails
      ) {
        const { id, loginDetails } = action.payload as TabLoginDetailsPayload;
        if (!loginDetails) return tabState;
        const tabs = { ...tabState.tabs };
        const tab = tabs[id];
        tab.loginDetails = loginDetails;
        tabs[id] = tab;
        return { ...tabState, tabs };
      }
      return tabState;
    default:
      return tabState;
  }
};

/**
 * The context for all the tabs.
 * @property activeTabID: string; The uuid of the active tab
 * @property createdTabs: number; The number of tabs created thus far
 * @property tabs: { [key: string]: Tab }; A dictionary of all the tabs. Each tab contains all the state necessary for a tab, including recordings, queryrecorder state, and login details
 * @property displayTabs: { [key: string]: DisplayTab }; A dictionary of the tab information needed to display the tabs at the top of the app
 * @property manager: SqlManager; The SqlManager. It's easier to keep it here than to get it through SqlManagerSingleton every time
 */
export const TabContext = createContext<{
  state: TabState;
  dispatch: React.Dispatch<TabAction>;
}>({ state: initialState, dispatch: () => null });

export const TabProvider = ({ children }: ChildrenType) => {
  const [state, dispatch] = useReducer(tabReducer, initialState);

  return (
    <TabContext.Provider
      value={{
        state,
        dispatch,
      }}
    >
      {children}
    </TabContext.Provider>
  );
};
