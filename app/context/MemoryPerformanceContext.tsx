import React, { useState, createContext } from 'react';

type ChildrenType = {
  children: React.ReactNode;
};

type ContextType = {
  disabled: Array<string>;
  setDisabled: (value: Array<string>) => void;
  validStages: Array<string>;
  setValidStages: (value: Array<string>) => void;
  openEvent: boolean;
  setOpenEvent: (value: boolean) => void;
  openStage: boolean;
  setOpenStage: (value: boolean) => void;
  eventsChecked: Array<string>;
  setEventsChecked: (value: Array<string>) => void;
  stagesChecked: Array<string>;
  setStagesChecked: (value: Array<string>) => void;
  filteredEvents: Array<string>;
  setFilteredEvents: (value: Array<string>) => void;
  filteredStages: Array<string>;
  setFilteredStages: (value: Array<string>) => void;
};

/**
 * The context for the memory performance component.
 * @property disabled: The disabled events
 * @property validStages: The stages to display
 * @property openEvent: Whether to open the dialog for editing what events to show
 * @property openStage: Whether to open the dialog for editing what stages to show
 * @property eventsChecked: What events are checked in the dialog to be shown
 * @property stagesChecked: What stages are checked in the dialog to be shown
 * @property filteredEvents: The events to be shown in the dialog after a search
 * @property filteredStages: The stages to be shown in the dialog after a search
 */
export const MemoryPerformanceContext = createContext<ContextType | undefined>(
  undefined
);

export const MemoryPerformanceProvider = ({ children }: ChildrenType) => {
  const [disabled, setDisabled] = useState<Array<string>>([]);
  const [validStages, setValidStages] = useState<Array<string>>([]);
  const [openEvent, setOpenEvent] = useState(false);
  const [openStage, setOpenStage] = useState(false);
  const [eventsChecked, setEventsChecked] = useState<Array<string>>([]);
  const [stagesChecked, setStagesChecked] = useState<Array<string>>([]);
  const [filteredEvents, setFilteredEvents] = useState<Array<string>>([]);
  const [filteredStages, setFilteredStages] = useState<Array<string>>([]);

  return (
    <MemoryPerformanceContext.Provider
      value={{
        disabled,
        setDisabled,
        validStages,
        setValidStages,
        openEvent,
        setOpenEvent,
        openStage,
        setOpenStage,
        eventsChecked,
        setEventsChecked,
        stagesChecked,
        setStagesChecked,
        filteredEvents,
        setFilteredEvents,
        filteredStages,
        setFilteredStages,
      }}
    >
      {children}
    </MemoryPerformanceContext.Provider>
  );
};
