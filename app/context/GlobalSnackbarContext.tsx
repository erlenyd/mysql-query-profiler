import React, { useState, createContext } from 'react';

type ChildrenType = {
  children: React.ReactNode;
};

type PopupSeverity = 'success' | 'info' | 'warning' | 'error' | undefined;

type ContextType = {
  popupMessage?: string;
  popupSeverity: PopupSeverity;
  popupOpen: boolean;
  setPopupMessage: (message: string) => void;
  setPopupSeverity: (severity: PopupSeverity) => void;
  setPopupOpen: (open: boolean) => void;
};

/**
 * Contains the state necessary for a global snackbar
 * @property popupMessage: The message to be displayed
 * @property popupSeverity: The severity of the message, affecting the appearance of the snackbar. Info is blue, warning is yellow, error is red, success is green
 * @property popupOpen: Whether to have the snackbar open
 */
export const GlobalSnackbarContext = createContext<ContextType | undefined>(
  undefined
);

export const GlobalSnackbarProvider = ({ children }: ChildrenType) => {
  const [popupMessage, setPopupMessage] = useState<string>();
  const [popupSeverity, setPopupSeverity] = useState<PopupSeverity>();
  const [popupOpen, setPopupOpen] = useState<boolean>(false);

  return (
    <GlobalSnackbarContext.Provider
      value={{
        popupMessage,
        popupSeverity,
        popupOpen,
        setPopupMessage,
        setPopupSeverity,
        setPopupOpen,
      }}
    >
      {children}
    </GlobalSnackbarContext.Provider>
  );
};
