import React, { useContext } from 'react';
import { Snackbar } from '@material-ui/core';
import { Alert } from '@material-ui/lab';
import { GlobalSnackbarContext } from '../../context/GlobalSnackbarContext';

/**
 * A general purpose snackbar for showing feedback and alerts to the user
 */
export default function GlobalSnackbar() {
  const globalSnackbarContext = useContext(GlobalSnackbarContext);

  return (
    <Snackbar
      anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }}
      autoHideDuration={5000}
      open={globalSnackbarContext?.popupOpen}
      onClose={() => globalSnackbarContext?.setPopupOpen(false)}
    >
      <Alert
        severity={globalSnackbarContext?.popupSeverity}
        onClose={() => globalSnackbarContext?.setPopupOpen(false)}
      >
        {globalSnackbarContext?.popupMessage}
      </Alert>
    </Snackbar>
  );
}
