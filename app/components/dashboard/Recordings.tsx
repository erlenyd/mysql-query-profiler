import {
  Button,
  ListItem,
  ListItemText,
  makeStyles,
  Paper,
  TextField,
  Tooltip,
  Typography,
} from '@material-ui/core';
import React, { CSSProperties, useContext, useRef, useState } from 'react';
import InputAdornment from '@material-ui/core/InputAdornment';
import SearchIcon from '@material-ui/icons/Search';
import { keys, valuesIn } from 'lodash';
import { FixedSizeList } from 'react-window';
import useDeepCompareEffect from 'use-deep-compare-effect';
import { TabContext } from '../../context/TabContext';
import { RecordingListItem } from '../../types/RecordingListItem';
import RecordingOptions from './RecordingOptions/RecordingOptions';
import SaveRecording from './SaveRecording';
import { GlobalSnackbarContext } from '../../context/GlobalSnackbarContext';
import FileIO from '../../backend/utils/FileIO';

const useStyles = makeStyles((theme) => ({
  wrapper: {
    // maxWidth: 600 + theme.spacing(4),
  },
  content: {
    display: 'flex',
    flexFlow: 'column nowrap',
    padding: `${theme.spacing(1)}px ${theme.spacing(2)}px`,
  },
  listItemTextPrimary: {
    textOverflow: 'ellipsis',
    whiteSpace: 'nowrap',
    overflow: 'hidden',
  },
  listItemTextRoot: {
    margin: 0,
    padding: '12px 0px',
  },
  listItemRoot: {
    padding: 0,
  },
  actionBar: {
    display: 'flex',
    flexFlow: 'row nowrap',
    justifyContent: 'space-between',
  },
}));

/**
 * The list of all recordings in this tab, allowing the user to switch between them, save, load or remove them, or change their labels or colors. The list is searchable
 */
export default function Recordings() {
  const { state: tabState, dispatch } = useContext(TabContext);
  const { activeTabID, tabs } = tabState;
  const tab = tabs[activeTabID];
  const recordings = { ...tab?.recordingListItems };

  const [displayedRecordings, setDisplayedRecordings] = useState(recordings);

  const [activeRecordingID, setActiveRecordingID] = useState<
    string | undefined
  >(tab?.activeRecordingID);

  useDeepCompareEffect(() => {
    // Using useDeepCompareEffect because we want this to happen when 'recordings'
    // changes, even if it is an array (useEffect only supports primitive values)
    const newActiveRecordingID = tab?.activeRecordingID;
    setActiveRecordingID(newActiveRecordingID);
    setDisplayedRecordings(recordings);
    console.log('Ran recordings update');
  }, [recordings]);

  const classes = useStyles();

  function handleRecordingClick(id: string) {
    if (displayedRecordings[id]) {
      let newActiveRecordingID = activeRecordingID;
      setActiveRecordingID(id);
      if (tab?.recordings) {
        newActiveRecordingID = id;
      }
      dispatch({
        type: 'SET_ACTIVE_RECORDING',
        payload: {
          tabID: activeTabID,
          recordingID: newActiveRecordingID || '',
        },
      });
    }
  }

  const globalSnackbarContext = useContext(GlobalSnackbarContext);

  const inputRef = useRef<HTMLInputElement>(null);

  function openFileSelector() {
    inputRef.current?.click();
  }

  function loadRecording() {
    if (inputRef.current?.files) {
      const file = inputRef.current.files[0];
      if (file) {
        FileIO.loadRecording(file.path)
          .then((response) => {
            if (response) {
              const tempChartData = response.chartData || [];
              const chartDataLength = tempChartData?.length || 0;
              const totalTime =
                tempChartData[chartDataLength - 1]?.relative_time * 1000 || NaN;
              const newListItem: RecordingListItem = {
                query: response.query || 'Query lost',
                elapsed: totalTime || response.elapsed || -1,
                label: file.name,
                color: '',
                uuid: response.uuid,
                viewing: true,
                isSaved: true,
              };
              response.label = file.name;
              dispatch({
                type: 'ADD_RECORDING',
                payload: {
                  tabID: tabState.activeTabID,
                  recording: response,
                  recordingListItem: newListItem,
                  recordingID: response.uuid,
                },
              });
            }
            return true;
          })
          .catch((error) => {
            console.error(error);
            globalSnackbarContext?.setPopupMessage(String(error));
            globalSnackbarContext?.setPopupSeverity('error');
            globalSnackbarContext?.setPopupOpen(true);
          });
      }
    }
  }

  function formatElapsed(elapsed: number) {
    // Assuming a query will not run for longer than a few hours
    if (elapsed > 1000 * 60 * 60) {
      return `${(elapsed / (1000 * 60 * 60)).toFixed(1)} h`;
    }
    if (elapsed > 1000 * 60) {
      return `${(elapsed / (1000 * 60)).toFixed(1)} min`;
    }
    if (elapsed > 1000) {
      return `${(elapsed / 1000).toFixed(1)} s`;
    }
    return `${elapsed.toFixed(1)} ms`;
  }

  function filterRecordings(query: string) {
    let filtered: { [key: string]: RecordingListItem } = {};
    keys(recordings).forEach((id) => {
      if (recordings[id].label.toLowerCase().includes(query.toLowerCase())) {
        filtered[id] = recordings[id];
      }
    });
    if (query === '') filtered = recordings;
    setDisplayedRecordings(filtered);
  }

  function renderRow(propss: { index: number; style: CSSProperties }) {
    const { index, style } = propss;
    const recording = valuesIn(displayedRecordings)[index];

    if (!recording) {
      return null;
    }

    return (
      <ListItem
        classes={{ root: classes.listItemRoot }}
        style={style}
        key={index}
        button
        selected={recording.uuid === activeRecordingID}
      >
        <Tooltip title={index + 1}>
          <ListItemText
            primary={index + 1}
            classes={{
              primary: classes.listItemTextPrimary,
              root: classes.listItemTextRoot,
            }}
            style={{
              flexGrow: 1,
              flexShrink: 1,
              flexBasis: 30,
              height: 30,
              padding: 0,
              marginRight: 4,
              backgroundColor: recording.color,
              borderRadius: 100,
              textAlign: 'center',
              display: 'flex',
              flexFlow: 'column nowrap',
              justifyContent: 'center',
            }}
            onClick={() => handleRecordingClick(recording.uuid)}
          />
        </Tooltip>
        <Tooltip title={recording.label}>
          <ListItemText
            primary={recording.label}
            classes={{
              primary: classes.listItemTextPrimary,
              root: classes.listItemTextRoot,
            }}
            style={{
              flexGrow: 4,
              flexShrink: 1,
              flexBasis: 226,
              paddingRight: 4,
            }}
            onClick={() => handleRecordingClick(recording.uuid)}
          />
        </Tooltip>
        <Tooltip title={formatElapsed(recording.elapsed)}>
          <ListItemText
            primary={formatElapsed(recording.elapsed)}
            classes={{
              primary: classes.listItemTextPrimary,
              root: classes.listItemTextRoot,
            }}
            style={{
              flexGrow: 2,
              flexShrink: 1,
              flexBasis: 70,
              paddingRight: 4,
            }}
            onClick={() => handleRecordingClick(recording.uuid)}
          />
        </Tooltip>
        <Tooltip title={recording.query}>
          <ListItemText
            primary={recording.query}
            classes={{
              primary: classes.listItemTextPrimary,
              root: classes.listItemTextRoot,
            }}
            style={{ flexGrow: 1, flexShrink: 1, flexBasis: 150 }}
            onClick={() => handleRecordingClick(recording.uuid)}
          />
        </Tooltip>
        <SaveRecording
          recordinguuid={recording.uuid}
          isSaved={recording.isSaved}
        />
        <RecordingOptions recordingID={recording.uuid} />
      </ListItem>
    );
  }

  return (
    <div className={classes.wrapper}>
      <Paper className={classes.content} elevation={1}>
        <Typography variant="h5">Recordings</Typography>
        <div className={classes.actionBar}>
          <TextField
            id="outlined-search"
            label="Search label..."
            type="search"
            variant="outlined"
            onChange={(event) => filterRecordings(event.target.value)}
            onKeyPress={(event) => {
              if (event.key === 'Enter') event.preventDefault();
            }}
            InputProps={{
              endAdornment: (
                <InputAdornment position="start">
                  <SearchIcon />
                </InputAdornment>
              ),
            }}
            style={{
              marginTop: 8,
              width: 170,
            }}
          />
          <div>
            <input
              type="file"
              ref={inputRef}
              style={{ display: 'none' }}
              accept=".json"
              onChange={() => loadRecording()}
            />
            <Button
              variant="outlined"
              color="primary"
              onClick={() => openFileSelector()}
              style={{
                marginTop: 8,
                height: 40,
              }}
            >
              Load Recording
            </Button>
          </div>
        </div>
        <ListItem classes={{ root: classes.listItemRoot }}>
          <ListItemText
            primary="#"
            classes={{
              primary: classes.listItemTextPrimary,
              root: classes.listItemTextRoot,
            }}
            style={{
              flexGrow: 1,
              flexShrink: 1,
              flexBasis: 30,
              height: 30,
              padding: 0,
              marginRight: 4,
              borderRadius: 100,
              textAlign: 'center',
              display: 'flex',
              flexFlow: 'column nowrap',
              justifyContent: 'center',
            }}
          />
          <ListItemText
            primary="Label"
            classes={{
              primary: classes.listItemTextPrimary,
              root: classes.listItemTextRoot,
            }}
            style={{
              flexGrow: 4,
              flexShrink: 1,
              flexBasis: 226,
              paddingRight: 4,
            }}
          />
          <ListItemText
            primary="Elapsed"
            classes={{
              primary: classes.listItemTextPrimary,
              root: classes.listItemTextRoot,
            }}
            style={{
              flexGrow: 2,
              flexShrink: 1,
              flexBasis: 70,
              paddingRight: 4,
            }}
          />
          <ListItemText
            primary="Query"
            classes={{
              primary: classes.listItemTextPrimary,
              root: classes.listItemTextRoot,
            }}
            style={{
              flexGrow: 1,
              flexShrink: 1,
              flexBasis: 150,
            }}
          />
          <div style={{ minWidth: 120, maxWidth: 120, height: 48 }} />
        </ListItem>
        <FixedSizeList
          height={400}
          width="100%"
          itemSize={48}
          itemCount={valuesIn(displayedRecordings).length}
        >
          {renderRow}
        </FixedSizeList>
      </Paper>
    </div>
  );
}
