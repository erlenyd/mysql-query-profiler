import React, { useState } from 'react';
import { ChromePicker, ColorResult } from 'react-color';
import { isNaN } from 'lodash';
import { IconButton, Icon, Popover, TextField } from '@material-ui/core';

/**
 * A component for picking colors.
 * @param props state for the current color and a callback to change it
 */
export default function ColorPicker(props: {
  currentColor: string;
  setColor: (value: string) => void;
}) {
  const [colorPickerAnchor, setColorPickerAnchor] = useState<Element | null>(
    null
  );
  const { setColor, currentColor } = props;

  const [colorError, setColorError] = useState('');

  function isHexColor(hex: string) {
    return hex.length === 6 && !isNaN(Number(`0x${hex}`));
  }

  function handleChangedColor(color?: ColorResult, colorString?: string) {
    if (color) {
      setColor(color.hex);
      setColorError('');
    } else if (colorString) {
      if (isHexColor(colorString.replace('#', ''))) {
        const convertedColorString = colorString.includes('#')
          ? colorString
          : `#${colorString}`;
        setColor(convertedColorString);
        setColorError('');
      } else {
        setColorError('Must be valid hex color');
      }
    }
  }

  return (
    <div>
      <TextField
        variant="filled"
        label="Color"
        value={currentColor}
        error={colorError.length > 0}
        helperText={colorError}
        onFocus={() => setColorError('')}
        onChange={(event) => handleChangedColor(undefined, event.target.value)}
      />
      <IconButton
        onClick={(event) => setColorPickerAnchor(event.currentTarget)}
      >
        <Icon>palette</Icon>
      </IconButton>
      <Popover
        open={Boolean(colorPickerAnchor)}
        anchorEl={colorPickerAnchor}
        onClose={() => setColorPickerAnchor(null)}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'center',
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'center',
        }}
      >
        <ChromePicker
          color={currentColor}
          onChange={(color) => handleChangedColor(color)}
          onChangeComplete={(color) => handleChangedColor(color)}
        />
      </Popover>
    </div>
  );
}
