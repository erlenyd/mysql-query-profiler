/* eslint-disable @typescript-eslint/no-explicit-any */
import React, { useContext } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
  Paper,
  Button,
  TextField,
  Typography,
  FormControlLabel,
  Checkbox,
  CircularProgress,
  Slider,
} from '@material-ui/core';
import Switch from '@material-ui/core/Switch';
import Collapse from '@material-ui/core/Collapse';
import clsx from 'clsx';
import { RawRecording } from '../../backend/recorder/SqlManager';
import ColorPicker from './ColorPicker';
import { TabContext } from '../../context/TabContext';
import { RecordingUpdate } from '../../types/RecordingUpdate';

const useStyles = makeStyles((theme) => ({
  container: {
    // maxWidth: 600 + theme.spacing(4),
  },
  content: {
    display: 'flex',
    flexFlow: 'column nowrap',
    padding: `${theme.spacing(1)}px ${theme.spacing(2)}px`,
  },
  element: {
    marginBottom: theme.spacing(1),
  },
  input: {
    display: 'flex',
    flexFlow: 'column nowrap',
    flexGrow: 1,
  },
  recordButtonWrapper: {
    display: 'flex',
    flexFlow: 'row nowrap',
    alignItems: 'center',
  },
  output: {
    userSelect: 'none',
  },
  code: {
    fontFamily: 'Roboto Mono !important',
  },
  slider: {
    marginRight: theme.spacing(2),
    marginLeft: theme.spacing(2),
  },
}));

const defaultTimestep = 100; // ms

interface QuerRecorderProps {
  onNewRecording: (recording: RecordingUpdate) => void;
}

/**
 * A component for allowing the user to enter a query and record it. Supports adding a label or color, toggling explain analyze and configuring the recording timestep
 * @param props onNewRecording, a callback to add the new recording to the tab context
 */
export default function QueryRecorder(props: QuerRecorderProps) {
  const { onNewRecording } = props;

  const { state: tabState, dispatch } = useContext(TabContext);
  const { activeTabID, tabs, manager } = tabState;
  const tab = tabs[activeTabID];

  function changeRecordingState(state: string) {
    dispatch({
      type: 'SET_RECORDING_STATE',
      payload: { tabID: activeTabID, value: state },
    });
  }

  function changeQuery(query: string) {
    dispatch({
      type: 'SET_INPUT_QUERY',
      payload: { tabID: activeTabID, value: query },
    });
  }

  function changeShowAdvancedOptions(show: boolean) {
    dispatch({
      type: 'SET_SHOW_ADVANCED_OPTIONS',
      payload: { tabID: activeTabID, value: show },
    });
  }

  function changeExplainAnalyze(explainAnalyze: boolean) {
    dispatch({
      type: 'SET_EXPLAIN_ANALYZE',
      payload: { tabID: activeTabID, value: explainAnalyze },
    });
  }

  function changeTimestep(timestep: number) {
    dispatch({
      type: 'SET_TIMESTEP',
      payload: { tabID: activeTabID, value: timestep / 1000 },
    });
  }

  function changeRecordingLabel(label: string) {
    dispatch({
      type: 'SET_RECORDING_INPUT_LABEL',
      payload: { tabID: activeTabID, value: label },
    });
  }

  function changeRecordingColor(color: string) {
    dispatch({
      type: 'SET_RECORDING_INPUT_COLOR',
      payload: { tabID: activeTabID, value: color },
    });
  }

  function changeQueryRunning(isRunning: boolean) {
    dispatch({
      type: 'SET_QUERY_RUNNING',
      payload: { tabID: activeTabID, value: isRunning },
    });
  }

  function changeRecordingRunning(isRunning: boolean) {
    dispatch({
      type: 'SET_RECORDING_RUNNING',
      payload: { tabID: activeTabID, value: isRunning },
    });
  }

  function updateContext(result?: RawRecording, elapsed?: number, error?: any) {
    if (error && String(error).includes('cancelled')) {
      return;
    }
    const update = {
      result,
      elapsed,
      label: tab.inputLabel,
      color: tab.inputColor,
      error,
      explainAnalyze: tab.explainAnalyze,
      query: tab.inputQuery,
      tabID: result?.tabID,
    };
    onNewRecording(update);
  }

  async function record() {
    changeRecordingRunning(true);
    if (!manager.connections[activeTabID].client) {
      console.error('QueryRecorder: Not connected.');
      changeRecordingRunning(false);
      return;
    }
    try {
      // TODO: Get time elapsed from result
      const t0 = performance.now();
      const result = await manager.record(
        tab.inputQuery,
        activeTabID,
        changeQueryRunning,
        changeRecordingState,
        tab.timestep,
        tab.explainAnalyze ||
          tab.inputQuery.toLowerCase().includes('explain analyze')
      );
      const t1 = performance.now();
      if (result?.error) {
        updateContext(result, t1 - t0, result?.error);
      } else {
        updateContext(result, t1 - t0);
      }
    } catch (error) {
      console.error(`QueryRecorder: Error: ${error}`);
      updateContext(undefined, undefined, error);
    }
  }

  const classes = useStyles();

  function checkShortcut(e: { key: string; ctrlKey: boolean }) {
    if (e.key === 'Enter' && e.ctrlKey && !tab.recordingRunning) {
      record();
    }
  }

  function handleSliderChange(_: unknown, newTimeStep: number | number[]) {
    changeTimestep(newTimeStep as number);
  }

  function getSliderValueText(value: number) {
    return `${value} ms`;
  }

  const marks = [
    {
      value: 100,
      label: '100 ms',
    },
    {
      value: 1000,
      label: '1000 ms',
    },
  ];

  function toggleAdvancedOptions() {
    changeShowAdvancedOptions(!tab.showAdvancedOptions);
  }

  async function cancelRecording() {
    await manager.cancelRecording(activeTabID);
    changeQueryRunning(false);
    changeRecordingRunning(false);
  }

  return (
    <div className={classes.container}>
      <Paper className={classes.content} elevation={1}>
        <div className={classes.element}>
          <Typography variant="h5">Record performance of a query</Typography>
        </div>
        <div className={clsx(classes.element, classes.input)}>
          <TextField
            autoFocus
            multiline
            InputProps={{
              classes: {
                input: classes.code,
              },
            }}
            variant="filled"
            label="Query"
            rows={2}
            rowsMax={10}
            value={tab.inputQuery}
            onChange={(event) => changeQuery(event.target.value)}
            onKeyUp={checkShortcut}
            helperText="Ctrl + Enter: Run"
          />
        </div>
        <div className={classes.element}>
          <FormControlLabel
            control={
              <Checkbox
                color="primary"
                checked={tab.explainAnalyze}
                onChange={(event) => changeExplainAnalyze(event.target.checked)}
              />
            }
            label="Explain analyze"
          />
        </div>
        <div className={classes.element}>
          <FormControlLabel
            control={
              <Switch
                checked={tab.showAdvancedOptions}
                onChange={toggleAdvancedOptions}
                onKeyUp={(event) =>
                  event.key === 'Enter' && toggleAdvancedOptions()
                }
                color="primary"
                inputProps={{ 'aria-label': 'primary checkbox' }}
              />
            }
            label="Advanced options"
          />
        </div>
        <div className={classes.element}>
          <Collapse in={tab.showAdvancedOptions}>
            <div className={classes.element}>
              <div>
                <Typography variant="subtitle1">
                  Recording timestep (ms)
                </Typography>
              </div>
              <div className={classes.slider}>
                <Slider
                  defaultValue={defaultTimestep}
                  aria-labelledby="discrete-slider-small-steps"
                  getAriaValueText={getSliderValueText}
                  onChange={handleSliderChange}
                  step={50}
                  marks={marks}
                  min={100}
                  max={1000}
                  valueLabelDisplay="auto"
                />
              </div>
            </div>
            <div className={classes.element}>
              <TextField
                variant="filled"
                label="Recording label"
                value={tab.inputLabel}
                onChange={(event) => changeRecordingLabel(event.target.value)}
                onKeyUp={checkShortcut}
              />
            </div>
            <ColorPicker
              currentColor={tab.inputColor}
              setColor={changeRecordingColor}
            />
          </Collapse>
        </div>

        <div className={clsx(classes.element, classes.recordButtonWrapper)}>
          <Button
            disabled={tab.recordingRunning}
            variant="contained"
            color="primary"
            onClick={() => record()}
          >
            Record
          </Button>
          <Button
            style={{ margin: '8px' }}
            disabled={!tab.queryRunning}
            variant="outlined"
            color="primary"
            onClick={() => cancelRecording()}
          >
            Cancel
          </Button>

          {tab.recordingRunning && (
            <CircularProgress
              style={{ marginLeft: '16px' }}
              size="24px"
              color="primary"
            />
          )}
        </div>
        {tab?.recordingState && tab.recordingState.length > 0 && (
          <div className={classes.element}>
            <Typography variant="subtitle2">{tab.recordingState}</Typography>
          </div>
        )}
      </Paper>
    </div>
  );
}
