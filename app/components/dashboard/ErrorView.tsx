import { Paper, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import React, { useContext } from 'react';
import { TabContext } from '../../context/TabContext';

const useStyles = makeStyles((theme) => ({
  content: {
    display: 'flex',
    flexFlow: 'column nowrap',
    padding: `${theme.spacing(1)}px ${theme.spacing(2)}px`,
  },
}));

/**
 * A component for showing the errors from a recording to the user
 */
export default function ErrorView() {
  const { state: tabState } = useContext(TabContext);
  const { activeTabID, tabs } = tabState;
  const tab = tabs[activeTabID];
  const recordings = { ...tab?.recordings };
  const error = recordings[tab?.activeRecordingID || '']?.error;

  let errorMessage;
  if (error?.info) {
    const errorcode = JSON.stringify(error?.info?.code, undefined, 2);
    const errormsg = JSON.stringify(error?.info?.msg, undefined, 2);
    errorMessage = `Errorcode: ${errorcode}\n${errormsg}`;
  } else if (error === 'cancelled') {
    errorMessage = 'Query was cancelled';
  } else {
    errorMessage = String(error);
  }

  const classes = useStyles();

  return (
    <div>
      {error && (
        <Paper className={classes.content} elevation={1}>
          <Typography variant="h5">Errors</Typography>
          <Typography
            variant="subtitle1"
            style={{
              whiteSpace: 'pre-line',
            }}
            color="error"
          >
            {errorMessage}
          </Typography>
        </Paper>
      )}
    </div>
  );
}
