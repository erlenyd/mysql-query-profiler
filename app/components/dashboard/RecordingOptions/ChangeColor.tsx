import React, { useState, useContext } from 'react';
import { Dialog, DialogTitle, Button, MenuItem } from '@material-ui/core';
import ColorPicker from '../ColorPicker';
import { TabContext } from '../../../context/TabContext';

/**
 * A button and dialog for changing the recording color
 * @param props The ID of the recording this is manipulating and a callback to close the menu
 */
export default function ChangeColor(props: {
  recordingID: string;
  handleIconMenuClose: () => void;
}) {
  const { recordingID, handleIconMenuClose } = props;

  const [changedColor, setChangedColor] = useState('');
  const [changeColorDialogOpen, setChangeColorDialogOpen] = useState(false);

  const { state: tabState, dispatch } = useContext(TabContext);
  const { activeTabID } = tabState;

  function handleChangeColorDialogClose() {
    setChangeColorDialogOpen(false);
  }

  function handleChangeColorOptionClick() {
    handleIconMenuClose();
    setChangeColorDialogOpen(true);
  }

  function handleChangeColorButtonClick() {
    dispatch({
      type: 'SET_RECORDING_COLOR',
      payload: { tabID: activeTabID, recordingID, value: changedColor },
    });

    handleChangeColorDialogClose();
  }

  return (
    <div>
      <MenuItem onClick={handleChangeColorOptionClick}>Change color</MenuItem>
      <Dialog
        onClose={handleChangeColorDialogClose}
        open={changeColorDialogOpen}
      >
        <DialogTitle>Change color</DialogTitle>
        <ColorPicker currentColor={changedColor} setColor={setChangedColor} />
        <Button
          variant="contained"
          color="primary"
          onClick={() => handleChangeColorButtonClick()}
        >
          Change
        </Button>
      </Dialog>
    </div>
  );
}
