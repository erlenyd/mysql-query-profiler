import React, { useState, useContext } from 'react';
import {
  Icon,
  IconButton,
  Menu,
  MenuItem,
  ListItemIcon,
} from '@material-ui/core';
import ChangeLabel from './ChangeLabel';
import ChangeColor from './ChangeColor';
import { GlobalSnackbarContext } from '../../../context/GlobalSnackbarContext';
import { TabContext } from '../../../context/TabContext';

interface RecordingOptionsProps {
  recordingID: string;
}

/**
 * A menu for manipulating recordings
 * @param props The ID of the recording this menu was opened for
 */
export default function RecordingOptions(props: RecordingOptionsProps) {
  const [iconAnchorEl, setIconAnchorEl] = useState<null | HTMLElement>(null);

  const { recordingID } = props;

  const { state: tabState, dispatch } = useContext(TabContext);
  const { activeTabID, tabs } = tabState;
  const tab = tabs[activeTabID];
  const recordingListItems = tab?.recordingListItems || {};

  const globalSnackbarContext = useContext(GlobalSnackbarContext);

  function handleIconButtonClick(event: React.MouseEvent<HTMLButtonElement>) {
    setIconAnchorEl(event.currentTarget);
  }

  function handleIconMenuClose() {
    setIconAnchorEl(null);
  }

  function handleCopyQuery() {
    const recordingListItem = recordingListItems[recordingID];
    navigator.clipboard.writeText(recordingListItem?.query || '');
    handleIconMenuClose();
    globalSnackbarContext?.setPopupMessage('Query copied to clipboard!');
    globalSnackbarContext?.setPopupSeverity('info');
    globalSnackbarContext?.setPopupOpen(true);
  }

  function handleRemoveRecording() {
    dispatch({
      type: 'DELETE_RECORDING',
      payload: { tabID: activeTabID, recordingID },
    });

    handleIconMenuClose();
  }

  return (
    <ListItemIcon>
      <IconButton
        edge="end"
        aria-controls="icon-menu"
        aria-haspopup="true"
        onClick={(event) => handleIconButtonClick(event)}
      >
        <Icon>more_vert</Icon>
      </IconButton>
      <Menu
        id="icon-menu"
        anchorEl={iconAnchorEl}
        keepMounted
        open={Boolean(iconAnchorEl)}
        onClose={handleIconMenuClose}
      >
        <ChangeLabel
          recordingID={recordingID}
          handleIconMenuClose={handleIconMenuClose}
        />
        <ChangeColor
          recordingID={recordingID}
          handleIconMenuClose={handleIconMenuClose}
        />
        <MenuItem onClick={handleCopyQuery}>Copy query</MenuItem>
        <MenuItem onClick={handleRemoveRecording}>Remove</MenuItem>
      </Menu>
    </ListItemIcon>
  );
}
