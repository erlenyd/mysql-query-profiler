import React, { useState, useContext } from 'react';
import {
  Dialog,
  DialogTitle,
  TextField,
  Button,
  MenuItem,
} from '@material-ui/core';
import { TabContext } from '../../../context/TabContext';

/**
 * A button and dialog for changing the recording label
 * @param props The ID of the recording this is manipulating and a callback to close the menu
 */
export default function ChangeLabel(props: {
  recordingID: string;
  handleIconMenuClose: () => void;
}) {
  const { recordingID, handleIconMenuClose } = props;
  const [changedLabel, setChangedLabel] = useState('');
  const [changeLabelDialogOpen, setChangeLabelDialogOpen] = useState(false);

  const { state: tabState, dispatch } = useContext(TabContext);
  const { activeTabID } = tabState;

  function handleChangeLabelDialogClose() {
    setChangeLabelDialogOpen(false);
  }

  function handleChangeLabelOptionClick() {
    handleIconMenuClose();
    setChangeLabelDialogOpen(true);
  }

  function handleChangeLabelButtonClick() {
    dispatch({
      type: 'SET_RECORDING_LABEL',
      payload: { tabID: activeTabID, recordingID, value: changedLabel },
    });

    handleChangeLabelDialogClose();
  }

  function checkShortcut(key: string) {
    if (key === 'Enter') {
      handleChangeLabelButtonClick();
    }
  }

  return (
    <div>
      <MenuItem onClick={handleChangeLabelOptionClick}>Change label</MenuItem>
      <Dialog
        onClose={handleChangeLabelDialogClose}
        open={changeLabelDialogOpen}
      >
        <DialogTitle>Change label</DialogTitle>
        <TextField
          autoFocus
          variant="filled"
          label="New label"
          value={changedLabel}
          onChange={(event) => setChangedLabel(event.target.value)}
          onKeyDown={(event) => checkShortcut(event.key)}
        />
        <Button
          variant="contained"
          color="primary"
          onClick={() => handleChangeLabelButtonClick()}
        >
          Change
        </Button>
      </Dialog>
    </div>
  );
}
