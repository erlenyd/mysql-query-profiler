import React, { useState, useContext } from 'react';
import { Button, Dialog, DialogTitle, TextField } from '@material-ui/core';
import FileIO from '../../backend/utils/FileIO';
import { GlobalSnackbarContext } from '../../context/GlobalSnackbarContext';
import { TabContext } from '../../context/TabContext';

/**
 * A component containing a save button and a dialog for saving a recording with a file name
 * @param props The id of the recording and wheter it is saved
 */
export default function SaveRecording(props: {
  recordinguuid: string;
  isSaved: boolean;
}) {
  const { recordinguuid, isSaved } = props;

  const [btnDisabled, setBtnDisabled] = useState(isSaved);
  const [filename, setFilename] = useState('');
  const [openFileDialog, setOpenFileDialog] = useState(false);

  const { state: tabState } = useContext(TabContext);
  const { activeTabID, tabs } = tabState;
  const tab = tabs[activeTabID];
  const { recordings, recordingListItems } = tab;
  const recording = recordings[recordinguuid || ''];
  const recordingListItem = recordingListItems[recordinguuid || ''];

  const globalSnackbarContext = useContext(GlobalSnackbarContext);

  async function handleSaveFile() {
    if (filename && recording && recordingListItem) {
      recording.isSaved = true;
      recordingListItem.isSaved = true;
      const path = await FileIO.saveRecordingDetails(recording, filename);
      setOpenFileDialog(false);
      setBtnDisabled(true);
      globalSnackbarContext?.setPopupMessage(`Recording saved to ${path}`);
      globalSnackbarContext?.setPopupSeverity('success');
      globalSnackbarContext?.setPopupOpen(true);
    }
  }

  function handleKeyUp(e: { key: string }) {
    if (e.key === 'Enter') {
      handleSaveFile();
    }
  }

  return (
    <div>
      <Button
        variant="outlined"
        color="primary"
        disabled={btnDisabled}
        onClick={() => setOpenFileDialog(true)}
      >
        Save
      </Button>
      <Dialog
        open={openFileDialog}
        onClose={() => setOpenFileDialog(false)}
        aria-labelledby="form-dialog-title"
      >
        <DialogTitle id="form-dialog-title">Enter Filename</DialogTitle>
        <TextField
          autoFocus
          variant="filled"
          label="Filename"
          value={filename}
          onKeyUp={handleKeyUp}
          onChange={(event) => setFilename(event.target.value)}
        />
        <Button onClick={handleSaveFile} variant="contained" color="primary">
          Save
        </Button>
      </Dialog>
    </div>
  );
}
