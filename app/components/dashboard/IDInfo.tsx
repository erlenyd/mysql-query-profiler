import React, { useContext } from 'react';
import { makeStyles, Paper, Typography, Tooltip } from '@material-ui/core';
import { TabContext } from '../../context/TabContext';

const useStyles = makeStyles((theme) => ({
  wrapper: {
    padding: `${theme.spacing(1)}px ${theme.spacing(2)}px`,
  },
  line: {
    display: 'flex',
    flexFlow: 'row nowrap',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  itemTitle: {
    marginRight: theme.spacing(1),
  },
}));

/**
 * A component responsible for showing the thread and connection IDs for the SqlAgent, SqlRunner and the SqlMonitor in this tab.
 */
export default function IDInfo() {
  const { state: tabState } = useContext(TabContext);
  const { activeTabID, manager } = tabState;
  const connection = manager.connections[activeTabID];

  const classes = useStyles();

  return (
    <Paper elevation={1} className={classes.wrapper}>
      <Tooltip title="Runner is responsible for running queries which will be recorded">
        <div className={classes.line}>
          <Typography className={classes.itemTitle} variant="subtitle1">
            Runner
          </Typography>
          <Typography variant="subtitle2">
            {`ConnectionID: ${connection?.runner?.connectionID} ThreadID: ${connection?.runner?.threadID}`}
          </Typography>
        </div>
      </Tooltip>
      <Tooltip title="Monitor is responsible for monitoring the resource usage of another session">
        <div className={classes.line}>
          <Typography className={classes.itemTitle} variant="subtitle1">
            Monitor
          </Typography>
          <Typography variant="subtitle2">
            {`ConnectionID: ${connection?.monitor?.connectionID}, ThreadID: ${connection?.monitor?.threadID}`}
          </Typography>
        </div>
      </Tooltip>
      <Tooltip title="Agent is responsible for performing actions when Runner and Monitor are busy">
        <div className={classes.line}>
          <Typography className={classes.itemTitle} variant="subtitle1">
            Agent
          </Typography>
          <Typography variant="subtitle2">
            {`ConnectionID: ${connection?.agent?.connectionID} ThreadID: ${connection?.agent?.threadID}`}
          </Typography>
        </div>
      </Tooltip>
    </Paper>
  );
}
