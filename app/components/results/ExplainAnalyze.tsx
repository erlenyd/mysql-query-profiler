import {
  Button,
  Checkbox,
  FormControlLabel,
  makeStyles,
  Typography,
} from '@material-ui/core';
import React, { useContext, useRef, useState } from 'react';
import { FlameGraph } from 'react-flame-graph';
import { AutoSizer } from 'react-virtualized';
import TreeView from '@material-ui/lab/TreeView';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import TreeItem from '@material-ui/lab/TreeItem';
import { ExplainAnalyzeNode } from '../../backend/data-processor/types/ExplainAnalyzeNode';
import { TabContext } from '../../context/TabContext';
import ExplainAnalyzeTable from './ExplainAnalyzeTable';
import useSmartTooltip from '../../useSmartToolTip';

type ToolTipState = {
  mouseX: number;
  mouseY: number;
  text: string;
};

function getMousePos(relativeContainer: any, mouseEvent: MouseEvent) {
  if (relativeContainer) {
    const rect = relativeContainer.getBoundingClientRect();
    const mouseX = mouseEvent.clientX - rect.left;
    const mouseY = mouseEvent.clientY - rect.top;
    return { mouseX, mouseY };
  }
  return { mouseX: 0, mouseY: 0 };
}

const useStyles = makeStyles((theme) => ({
  header: {
    paddingBottom: theme.spacing(1),
    paddingLeft: theme.spacing(1),
    paddingRight: theme.spacing(1),
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  flamegraphWrapper: {
    height: 200,
    resize: 'vertical',
    overflow: 'hidden',
    marginBottom: theme.spacing(2),
  },
  tooltip: {
    position: 'absolute',
    borderRadius: 4,
    zIndex: 3,
    backgroundColor: theme.palette.grey.A400,
    color: '#fff',
    padding: '0.4rem',
    marginTop: 6,
    marginLeft: 2,
    fontSize: 13,
    fontFamily: ['Raleway', 'Arial'].join(','),
  },
  element: {
    paddingLeft: theme.spacing(1),
    paddingBottom: theme.spacing(1),
  },
  root: {
    flexGrow: 1,
  },
  table: {
    flexGrow: 1,
    height: 225,
    resize: 'vertical',
    overflowY: 'auto',
    width: '100% !important',
    marginBottom: theme.spacing(2),
  },
  treeview: {
    flexGrow: 1,
    paddingBottom: theme.spacing(1),
  },
}));

/**
 * A flame graph and tree view of an explain analyze result
 */
export default function ExplainAnalyze() {
  const [selectedNode, setSelectedNode] = useState<ExplainAnalyzeNode | null>(
    null
  );
  const [unpaired, setUnpaired] = useState<boolean>(false);
  const [hoveredNode, setHoveredNode] = useState<ExplainAnalyzeNode | null>(
    null
  );
  const [tooltipState, setTooltipState] = useState<ToolTipState | null>(null);
  const tooltipRef = useSmartTooltip({
    mouseX: tooltipState === null ? 0 : tooltipState.mouseX,
    mouseY: tooltipState === null ? 0 : tooltipState.mouseY,
  });
  const classes = useStyles();
  const { state: tabState } = useContext(TabContext);
  const { activeTabID, tabs } = tabState;
  const tab = tabs[activeTabID];
  const recordings = { ...tab?.recordings };
  const activeRecordingID = tab?.activeRecordingID || '';
  const data = recordings[activeRecordingID]?.explainAnalyzeRoot;
  const parentDict = recordings[activeRecordingID]?.explainAnalyzeParents;
  const containerRef = useRef(null);

  // Controls the tree view:
  const [expandedIds, setExpandedIds] = useState<string[]>([]);
  const [selectedIds, setSelectedIds] = useState<string[]>([]);

  const [expandAll, setExpandAll] = useState<boolean | undefined>(undefined);

  const RenderTree = (nodes: ExplainAnalyzeNode) => (
    <TreeItem key={nodes.id} nodeId={nodes.id} label={nodes.name}>
      {Array.isArray(nodes.children)
        ? nodes.children.map((node) => RenderTree(node))
        : null}
    </TreeItem>
  );

  const RenderTreeWithoutRoot = (root?: ExplainAnalyzeNode) => {
    const elements: JSX.Element[] = [];
    root?.children.forEach((child) => {
      elements.push(RenderTree(child));
    });
    return elements;
  };

  function returnParents(nodeId: string, parents: { [id: string]: string }) {
    /*
      Input: node id, dictionary of parent of each node
      Output: All parent IDs of a node
    */
    const nodeIds: string[] = [];
    let currentNode = nodeId;
    let nextNode = parents[currentNode];
    while (nextNode) {
      nodeIds.push(nextNode);
      currentNode = nextNode;
      nextNode = parents[currentNode];
    }
    return nodeIds;
  }

  function searchTree(
    node: ExplainAnalyzeNode,
    id: string
  ): ExplainAnalyzeNode | null {
    /*
      Function: Returns node in a tree that matches a certain ID
      Input: Root node, id
      Output: Node
    */
    if (node.id === id) {
      return node;
    }
    if (node.children) {
      let i;
      let result = null;
      for (i = 0; result === null && i < node.children.length; i += 1) {
        result = searchTree(node.children[i], id);
      }
      return result;
    }
    return null;
  }

  function getAllIds(
    node: ExplainAnalyzeNode | undefined,
    arr: string[]
  ): string[] {
    /*
      Function: Appends IDs of all nodes in a tree to an array and returns the array
      Input: Root node, array
      Output: Array of IDs
    */
    if (!node) {
      return [];
    }
    if (node.children === []) {
      arr.push(node.id);
      return arr;
    }
    arr.push(node.id);
    node.children.forEach((child) => getAllIds(child, arr));
    return arr;
  }

  const handleToggle = (event: any, nodeIds: string[]) => {
    // Fired when items in tree view are expanded/collapsed.
    if (!event.ctrlKey) {
      setExpandedIds(nodeIds);
    }
  };

  const handleSelect = (
    _event: React.ChangeEvent<unknown>,
    nodeIds: string[]
  ) => {
    // Fired when items in tree view are selected/unselected.
    if (data && !unpaired) {
      const node = searchTree(data, nodeIds[0]);
      setSelectedNode(node);
      setHoveredNode(node);
    }
  };

  function handleButtonClick() {
    // Expands/minimizes all items in tree view
    if (!expandAll) {
      setExpandedIds(getAllIds(data, []));
      setExpandAll(true);
      return;
    }
    setExpandedIds([]);
    setExpandAll(false);
  }

  return (
    <div>
      <div className={classes.header}>
        <Typography variant="h6">Explain Analyze Flame Graph</Typography>
      </div>
      <div className={classes.flamegraphWrapper} ref={containerRef}>
        <AutoSizer>
          {({ width, height }) => (
            <FlameGraph
              data={data}
              height={height}
              width={width}
              enableTooltips
              disableDefaultTooltips
              onChange={(node: { source: ExplainAnalyzeNode }) => {
                // Fired when flame graph item has been clicked
                setSelectedNode(node.source);
                if (!unpaired && parentDict) {
                  setExpandedIds(returnParents(node.source.id, parentDict));
                  setSelectedIds([node.source.id]);
                }
              }}
              onMouseOver={(_event: never, itemData: ExplainAnalyzeNode) => {
                setHoveredNode(itemData);
                setTooltipState({
                  text:
                    itemData.name !== 'root'
                      ? `Total time: ${itemData.value} ms`
                      : 'root',
                  ...getMousePos(containerRef.current, _event),
                });
              }}
              onMouseMove={(_event: never, itemData: ExplainAnalyzeNode) => {
                setTooltipState({
                  text:
                    itemData.name !== 'root'
                      ? `Total time: ${itemData.value} ms`
                      : 'root',
                  ...getMousePos(containerRef.current, _event),
                });
              }}
              onMouseOut={() => {
                setHoveredNode(selectedNode);
                setTooltipState(null);
              }}
            />
          )}
        </AutoSizer>
        {tooltipState !== null && (
          <div ref={tooltipRef} className={classes.tooltip}>
            {tooltipState.text}
          </div>
        )}
      </div>
      <div className={classes.table}>
        <ExplainAnalyzeTable node={hoveredNode} />
      </div>
      <div>
        <div className={classes.header}>
          <Typography variant="h6">Explain Analyze Tree View</Typography>
          <div className={classes.element}>
            <FormControlLabel
              control={
                <Checkbox
                  color="primary"
                  checked={unpaired}
                  onChange={(event) => setUnpaired(event.target.checked)}
                />
              }
              label="Unpair"
            />
          </div>
          <Button
            variant="contained"
            color="primary"
            size="small"
            onClick={() => handleButtonClick()}
          >
            {expandAll ? 'Collapse all' : 'Expand all'}
          </Button>
        </div>
        <TreeView
          className={classes.treeview}
          defaultCollapseIcon={<ExpandMoreIcon />}
          defaultExpandIcon={<ChevronRightIcon />}
          expanded={expandedIds}
          selected={unpaired ? [] : selectedIds}
          onNodeToggle={handleToggle}
          onNodeSelect={handleSelect}
        >
          {[data ? RenderTreeWithoutRoot(data) : null]}
        </TreeView>
      </div>
    </div>
  );
}
