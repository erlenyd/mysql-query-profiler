/* eslint-disable no-plusplus */
/* eslint-disable linebreak-style */
import React, { CSSProperties, useContext } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Typography } from '@material-ui/core';

import { AutoSizer, MultiGrid } from 'react-virtualized';
import { TabContext } from '../../context/TabContext';

const useStyles = makeStyles((theme) => ({
  tableWrapper: {
    minHeight: 200,
    height: '100%',
    width: '100%',
    resize: 'vertical',
    overflowY: 'hidden',
    scrollbarWidth: 'none',
    paddingBottom: theme.spacing(2),
  },
  gridCell: {
    whiteSpace: 'nowrap',
    width: '100%',
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    paddingLeft: '1em',
    paddingRight: '1em',
    overflowX: 'auto',
    justifyContent: 'center',
  },
}));

/**
 * A table showing the results of a query. Virtualized
 */
export default function ResultGrid() {
  const { state: tabState } = useContext(TabContext);
  const { activeTabID, tabs } = tabState;
  const tab = tabs[activeTabID];
  const recordings = tab?.recordings || {};
  const activeRecordingID = tab?.activeRecordingID || '';
  const result = recordings[activeRecordingID]?.queryOutput;
  const classes = useStyles();

  function formatDate(d: Date) {
    let month = `${d.getMonth() + 1}`;
    let day = `${d.getDate()}`;
    const year = d.getFullYear();

    if (month.length < 2) month = `0${month}`;
    if (day.length < 2) day = `0${day}`;

    return [year, month, day].join('-');
  }

  function format(index: number, entry: unknown, inputTypes: number[]): string {
    let datatype = 0;
    if (inputTypes) {
      datatype = inputTypes[index];
    }
    if (datatype === 12) {
      if (typeof entry === 'number') {
        const date = new Date(Number(entry));
        return formatDate(date);
      }
      const date = new Date(String(entry));
      return formatDate(date);
    }
    return String(entry);
  }

  if (!result || !result.results) {
    return (
      <Typography style={{ padding: '8px 16px' }} variant="subtitle1">
        No data to show
      </Typography>
    );
  }

  const { labels, types, values } = result.results;
  const { columnWidths } = result;

  const STYLE: CSSProperties = {
    width: '100%',
    fontFamily: 'raleway',
    resize: 'none',
  };

  type CellRendererType = {
    rowIndex: number;
    columnIndex: number;
    style: React.CSSProperties;
  };

  function cellRenderer({ rowIndex, columnIndex, style }: CellRendererType) {
    const content =
      rowIndex === 0
        ? labels[columnIndex]
        : format(columnIndex, values[rowIndex - 1][columnIndex], types);
    return (
      <div
        style={style}
        key={`${columnIndex}-${rowIndex}`}
        className={classes.gridCell}
      >
        {content}
      </div>
    );
  }

  function getColumnWidth({ index }: { index: number }) {
    console.log(columnWidths);
    return Math.max(columnWidths ? columnWidths[index] : 100, 50);
  }

  return (
    <div className={classes.tableWrapper}>
      <div
        style={{
          minHeight: 201,
          height: '100%',
          width: '100%',
          paddingBottom: 1,
        }}
      >
        <AutoSizer>
          {({ height, width }) => (
            <MultiGrid
              cellRenderer={cellRenderer}
              columnWidth={getColumnWidth}
              columnCount={labels.length}
              height={height}
              rowHeight={48}
              rowCount={values.length + 1}
              fixedRowCount={1}
              width={width}
              style={STYLE}
              styleTopRightGrid={{
                fontWeight: 'bold',
                width: '100%',
                backgroundColor: '#404040',
                fontSize: 14,
              }}
              styleBottomRightGrid={{
                outline: 'none',
              }}
              overscanColumnCount={0}
              overscanRowCount={2}
            />
          )}
        </AutoSizer>
      </div>
    </div>
  );
}
