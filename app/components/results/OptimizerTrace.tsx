import { Button, Typography } from '@material-ui/core';
import React, { useState, useContext } from 'react';
import ReactJson from 'react-json-view';
import { makeStyles } from '@material-ui/core/styles';
import { TabContext } from '../../context/TabContext';

/**
 * Responsible for showing the optimizer trace of a recording as json, allowing it to be expanded or collapsed
 */
export default function OptimizerTrace() {
  const [expandAll, setExpandAll] = useState<boolean | undefined>(undefined);

  function getHeight() {
    if (typeof expandAll === 'undefined' || expandAll) return 211;
    return 'auto';
  }

  const useStyles = makeStyles((theme) => ({
    container: {
      resize: 'vertical',
      overflowY: 'scroll',
      minHeight: 70,
    },
    header: {
      display: 'flex',
      flexDirection: 'row',
      paddingBottom: theme.spacing(1),
      paddingLeft: theme.spacing(1),
      paddingRight: theme.spacing(1),
      justifyContent: 'space-between',
      position: 'sticky',
      top: 0,
      zIndex: 1,
      backgroundColor: theme.palette.background.paper,
    },
    trace: { zIndex: 0 },
  }));

  const { state: tabState } = useContext(TabContext);
  const { activeTabID, tabs } = tabState;
  const tab = tabs[activeTabID];
  const recordings = tab?.recordings || {};
  const activeRecordingID = tab?.activeRecordingID || '';
  const optimizerTrace = recordings[activeRecordingID]?.optimizerTrace;

  function handleButtonClick() {
    if (typeof expandAll === 'undefined') {
      setExpandAll(true);
      return;
    }
    setExpandAll(!expandAll);
  }

  function getCollapseLevel() {
    if (typeof expandAll === 'undefined') return 5;
    return !expandAll;
  }

  const classes = useStyles();

  return (
    <div
      style={{
        height: getHeight(),
      }}
      className={classes.container}
    >
      <div className={classes.header}>
        <Typography variant="h6">Optimizer trace</Typography>
        <div>
          <Button
            variant="contained"
            color="primary"
            onClick={() => handleButtonClick()}
          >
            {expandAll ? 'Collapse all' : 'Expand all'}
          </Button>
        </div>
      </div>
      <div className={classes.trace}>
        {optimizerTrace && (
          <ReactJson
            src={optimizerTrace}
            theme="twilight"
            name="OptimizerTrace"
            collapsed={getCollapseLevel()}
            indentWidth={2}
          />
        )}
      </div>
    </div>
  );
}
