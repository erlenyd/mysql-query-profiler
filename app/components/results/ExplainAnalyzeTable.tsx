import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { ExplainAnalyzeNode } from '../../backend/data-processor/types/ExplainAnalyzeNode';
import { TableElement } from '../../backend/data-processor/types/TableElement';

const useStyles = makeStyles((theme) => ({
  table: {
    backgroundColor: theme.palette.grey.A700,
  },
}));

function createData(description: string, data: string): TableElement {
  return { description, data };
}

function addData(node: ExplainAnalyzeNode | null) {
  /*
    Function: Returns table rows containing detailed information about a node
    Input: Node
    Output: Array of rows
  */
  if (!node || node.name === 'root') {
    const rows = [
      createData('Command', ''),
      createData('Actual time', ''),
      createData('Rows', ''),
      createData('Loops', ''),
    ];
    return rows;
  }
  if (node.name === 'Hash') {
    const rows = [createData('Command', node.name)];
    rows.push(...node.additionalData);
    return rows;
  }
  const rows = [
    createData('Command', node.name),
    createData('Actual time', node.time),
    createData('Rows', node.rows.toString()),
    createData('Loops', node.loops.toString()),
  ];

  if (node.cost_est && node.rows_est) {
    rows.splice(
      1,
      0,
      createData('Est. cost', node.cost_est?.toString()),
      createData('Est. rows', node.rows_est?.toString())
    );
  }

  rows.push(...node.additionalData);
  return rows;
}

/**
 * A table for showing more information about an explain analyze node
 * @param data ExplainAnalyzeNode | null
 */
export default function ExplainAnalyzeTable(data: {
  node: ExplainAnalyzeNode | null;
}) {
  const { node } = data;
  const rows = addData(node);
  const classes = useStyles();
  return (
    <TableContainer component={Paper}>
      <Table className={classes.table} size="small" aria-label="dense table">
        <TableBody>
          {rows.map((row: TableElement) => (
            <TableRow key={row.description}>
              <TableCell>{row.description}</TableCell>
              <TableCell align="left">{row.data}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}
