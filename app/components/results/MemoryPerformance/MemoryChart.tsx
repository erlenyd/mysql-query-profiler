import {
  Backdrop,
  Button,
  Checkbox,
  Fade,
  ListItem,
  ListItemIcon,
  ListItemText,
  makeStyles,
  Modal,
  TextField,
  Typography,
} from '@material-ui/core';
import _ from 'lodash';
import React, { useState, useContext, CSSProperties, useEffect } from 'react';
import { FixedSizeList } from 'react-window';
import {
  LineChart,
  Line,
  CartesianGrid,
  XAxis,
  YAxis,
  Tooltip,
  ResponsiveContainer,
  ReferenceLine,
  Label,
} from 'recharts';
import { TabContext } from '../../../context/TabContext';

const useStyles = makeStyles((theme) => ({
  lineChart: {
    zIndex: 999,
  },
  buttonWrapper: {
    display: 'flex',
    flexFlow: 'row nowrap',
    padding: `${theme.spacing(1)}px ${theme.spacing(2)}px`,
  },
  element: {
    marginBottom: theme.spacing(1),
  },
  paper: {
    position: 'absolute',
    width: 500,
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(2, 4, 3),
    outline: 'none',
    borderRadius: 4,
  },
  listItemRoot: {
    padding: 0,
  },
}));

const modalStyle = {
  top: '50%',
  left: '50%',
  transform: 'translate(-50%,-50%)',
};

/**
 * @deprecated The old component for showing memory usage over time
 */
export default function MemoryChart() {
  const classes = useStyles();
  const { state: tabState } = useContext(TabContext);
  const { activeTabID, tabs } = tabState;
  const tab = tabs[activeTabID];
  const recordings = tab?.recordings || {};
  const activeRecordingID = tab?.activeRecordingID || '';
  const stageTimes = recordings[activeRecordingID]?.stageTimes;
  const chartData = recordings[activeRecordingID]?.chartData;
  const eventColors = recordings[activeRecordingID]?.chartColors;
  const top3List = recordings[activeRecordingID]?.top3Memory;

  const [disabled, setDisabled] = useState<Array<string>>([]);
  const [validStages, setValidStages] = useState<Array<string>>([]);
  const [openEvent, setOpenEvent] = useState(false);
  const [openStage, setOpenStage] = useState(false);
  const [eventsChecked, setEventsChecked] = useState<Array<string>>([]);
  const [stagesChecked, setStagesChecked] = useState<Array<string>>([]);
  const [filteredEvents, setFilteredEvents] = useState<Array<string>>([]);
  const [filteredStages, setFilteredStages] = useState<Array<string>>([]);
  const [eventCheckAll, setEventCheckAll] = useState(true);
  const [stageCheckAll, setStageCheckAll] = useState(false);

  useEffect(() => {
    if (top3List && chartData) {
      const notTop3 = _.filter(
        _.keys(chartData[0]).slice(1),
        (o) => !_.includes(top3List, o)
      );
      setDisabled(notTop3);
      setEventsChecked(notTop3);
    }
  }, [top3List]);

  function getMaxTime() {
    // eslint-disable-next-line prefer-spread
    return Math.max.apply(Math, _.map(chartData, 'relative_time'));
  }

  function formatBytes(byte: number) {
    if (Math.abs(byte) > 10000000) {
      return `${Math.floor(byte / 1024 / 1024)} MB`;
    }
    if (Math.abs(byte) > 10000) {
      return `${Math.floor(byte / 1024)} KB`;
    }
    return `${Math.floor(byte)} B`;
  }

  // When a legend is clicked the datakey is added to the disabled list, which will remove it form the line chart
  function eventToggle(dataKey: string) {
    if (_.includes(disabled, dataKey)) {
      setDisabled(disabled.filter((obj) => obj !== dataKey));
    } else {
      setDisabled(disabled.concat(dataKey));
    }
  }

  function validStageToggle(stage: string) {
    if (_.includes(validStages, stage)) {
      setValidStages(validStages.filter((obj) => obj !== stage));
    } else {
      setValidStages(validStages.concat(stage));
    }
  }

  function handleEventToggle(value: string) {
    const curIndex = eventsChecked.indexOf(value);
    const newChecked = [...eventsChecked];

    if (curIndex === -1) {
      eventToggle(value);
      newChecked.push(value);
    } else {
      eventToggle(value);
      newChecked.splice(curIndex, 1);
    }
    setEventsChecked(newChecked);
  }

  function handleStageToggle(value: string) {
    const curIndex = stagesChecked.indexOf(value);
    const newChecked = [...stagesChecked];

    if (curIndex === -1) {
      validStageToggle(value);
      newChecked.push(value);
    } else {
      validStageToggle(value);
      newChecked.splice(curIndex, 1);
    }
    setStagesChecked(newChecked);
  }

  // Turns on/off all events
  function handleEventCheckAllToggle() {
    if (eventCheckAll === true) {
      setEventCheckAll(false);
      if (chartData) {
        setEventsChecked(_.keys(chartData[0]).slice(1));
        setDisabled(_.keys(chartData[0]).slice(1));
      }
    } else {
      setEventCheckAll(true);
      setEventsChecked([]);
      setDisabled([]);
    }
  }

  // Turns on/off all stages
  function handleStageCheckAllToggle() {
    if (stageCheckAll === true) {
      setStageCheckAll(false);
      setStagesChecked([]);
      setValidStages([]);
    } else {
      setStageCheckAll(true);
      if (stageTimes) {
        setStagesChecked(_.map(_.uniqBy(stageTimes, 'stage'), 'stage'));
        setValidStages(_.map(_.uniqBy(stageTimes, 'stage'), 'stage'));
      }
    }
  }

  // Opens the modal to select event
  function handleOpenEvent() {
    if (chartData) {
      setFilteredEvents(_.keys(chartData[0]).slice(1));
    }
    setOpenEvent(true);
  }

  function handleCloseEvent() {
    setOpenEvent(false);
  }

  // Opens the modal to select event
  function handleOpenStage() {
    if (stageTimes) {
      setFilteredStages(_.map(_.uniqBy(stageTimes, 'stage'), 'stage'));
    }
    setOpenStage(true);
  }

  function handleCloseStage() {
    setOpenStage(false);
  }

  // Sets filteredEvents with a list of the datakeys that matches the text in the search bar
  function searchBarFilter(search: string, memory: boolean) {
    let filteredList = [''];
    if (memory) {
      if (chartData) {
        filteredList = _.filter(_.keys(chartData[0]).slice(1), (o) =>
          _.includes(o.toLowerCase(), search.toLowerCase())
        );
      }
      setFilteredEvents(filteredList);
    } else {
      if (stageTimes) {
        filteredList = _.filter(
          _.map(_.uniqBy(stageTimes, 'stage'), 'stage'),
          (o) => _.includes(o.toLowerCase(), search.toLowerCase())
        );
      }
      setFilteredStages(filteredList);
    }
  }

  function renderEventRow(props: { index: number; style: CSSProperties }) {
    const { index, style } = props;

    const eventType = filteredEvents[index];

    const labelId = `checkbox-list-label-${eventType}`;
    return (
      <ListItem
        classes={{ root: classes.listItemRoot }}
        style={style}
        key={eventType}
        role={undefined}
        dense
        button
        onClick={() => handleEventToggle(eventType)}
      >
        <ListItemIcon>
          <Checkbox
            edge="start"
            checked={eventsChecked.indexOf(eventType) === -1}
            tabIndex={-1}
            disableRipple
            inputProps={{ 'aria-labelledby': labelId }}
            color="primary"
          />
        </ListItemIcon>
        <ListItemText id={labelId} primary={`${eventType}`} />
      </ListItem>
    );
  }

  function renderStageRow(props: { index: number; style: CSSProperties }) {
    const { index, style } = props;

    const stage = filteredStages[index];

    const labelId = `checkbox-list-label-${stage}`;
    return (
      <ListItem
        classes={{ root: classes.listItemRoot }}
        style={style}
        key={stage}
        role={undefined}
        dense
        button
        onClick={() => handleStageToggle(stage)}
      >
        <ListItemIcon>
          <Checkbox
            edge="start"
            checked={stagesChecked.indexOf(stage) !== -1}
            tabIndex={-1}
            disableRipple
            inputProps={{ 'aria-labelledby': labelId }}
            color="primary"
          />
        </ListItemIcon>
        <ListItemText id={labelId} primary={`${stage}`} />
      </ListItem>
    );
  }

  return (
    <div className={classes.lineChart}>
      <div className={classes.buttonWrapper}>
        <div style={{ flexGrow: 1 }}>
          <Typography variant="h6">Memory performance</Typography>
        </div>
        <Button
          variant="outlined"
          color="primary"
          onClick={handleOpenEvent}
          style={{ marginRight: 16 }}
        >
          Pick Events
        </Button>
        <Modal
          open={openEvent}
          onClose={handleCloseEvent}
          aria-labelledby="simple-modal-title"
          closeAfterTransition
          BackdropComponent={Backdrop}
          BackdropProps={{
            timeout: 500,
          }}
        >
          <Fade in={openEvent}>
            <div style={modalStyle} className={classes.paper}>
              <div className={classes.element}>
                <Typography variant="h5">Pick events to show</Typography>
              </div>
              <form>
                <TextField
                  id="filled-basic"
                  label="Search event..."
                  variant="filled"
                  onChange={(e) => searchBarFilter(e.target.value, true)}
                  onKeyPress={(e) => {
                    if (e.key === 'Enter') e.preventDefault();
                  }}
                />
              </form>
              <Checkbox
                edge="start"
                checked={eventCheckAll}
                tabIndex={-1}
                disableRipple
                inputProps={{ 'aria-labelledby': 'eventCheckAll' }}
                color="primary"
                onClick={() => handleEventCheckAllToggle()}
              />
              <FixedSizeList
                height={400}
                width="100%"
                itemSize={48}
                itemCount={filteredEvents.length}
              >
                {renderEventRow}
              </FixedSizeList>
            </div>
          </Fade>
        </Modal>
        {/* Stage modal */}
        <Button variant="outlined" color="primary" onClick={handleOpenStage}>
          Pick Stages
        </Button>
        <Modal
          open={openStage}
          onClose={handleCloseStage}
          aria-labelledby="simple-modal-title"
          closeAfterTransition
          BackdropComponent={Backdrop}
          BackdropProps={{
            timeout: 500,
          }}
        >
          <Fade in={openStage}>
            <div style={modalStyle} className={classes.paper}>
              <div className={classes.element}>
                <Typography variant="h5">Pick stages to show</Typography>
              </div>
              <form>
                <TextField
                  id="filled-basic"
                  label="Search stage"
                  variant="filled"
                  onChange={(e) => searchBarFilter(e.target.value, false)}
                  onKeyPress={(e) => {
                    if (e.key === 'Enter') e.preventDefault();
                  }}
                />
              </form>
              <Checkbox
                edge="start"
                checked={stageCheckAll}
                tabIndex={-1}
                disableRipple
                inputProps={{ 'aria-labelledby': 'eventCheckAll' }}
                color="primary"
                onClick={() => handleStageCheckAllToggle()}
              />
              <FixedSizeList
                height={400}
                width="100%"
                itemSize={48}
                itemCount={filteredStages.length}
              >
                {renderStageRow}
              </FixedSizeList>
            </div>
          </Fade>
        </Modal>
      </div>

      {chartData && chartData.length > 2 ? (
        <div>
          <ResponsiveContainer height={400}>
            <LineChart
              width={800}
              height={500}
              data={chartData}
              margin={{ top: 20, right: 30, left: 20, bottom: 5 }}
              style={{ fontFamily: 'Roboto, sans-serif' }}
            >
              <CartesianGrid strokeDasharray="3 10" />
              {/* TODO: Create our own Axis tick component like this: http://recharts.org/en-US/examples/CustomizedLabelLineChart */}
              <XAxis
                dataKey="relative_time"
                type="number"
                domain={[0, getMaxTime()]}
                tickCount={20}
                stroke="#FFFFFF"
              />
              <YAxis
                type="number"
                tickFormatter={formatBytes}
                stroke="#FFFFFF"
              />
              <Tooltip
                formatter={(value) => formatBytes(Number(value))}
                label="relative_time"
                labelFormatter={(name) => `Time Taken: ${name}`}
                offset={150}
              />

              {/* Creates an array from the key:value pairs in chartColors.
             The keys that are not disabled becomes a line */}
              {_.toPairs(eventColors)
                .filter((pair) => !_.includes(disabled, pair[0]))
                .map((pair) => (
                  <Line
                    type="monotone"
                    dataKey={pair[0]}
                    key={pair[0]}
                    stroke={pair[1]}
                    dot={false}
                  />
                ))}
              {/* Filters stageTimes to only include stages that are in validStages and only the first instance of them.
                  en creates reference lines from the data that remains */}

              {_.forEach(_.uniqBy(stageTimes, 'stage'))
                ?.filter((stage) => _.includes(validStages, stage.stage))
                ?.map((item) => (
                  <ReferenceLine
                    x={item.startTime}
                    stroke="#769CFF"
                    key={_.uniqueId('ref_')}
                  >
                    <Label
                      value={item.stage}
                      position="top"
                      style={{ color: '#FFFFFF' }}
                    />
                  </ReferenceLine>
                ))}
            </LineChart>
          </ResponsiveContainer>
        </div>
      ) : (
        <div>
          {chartData ? (
            <Typography variant="subtitle1" style={{ padding: '8px 16px' }}>
              There are not enough data points to show a meaningful graph
            </Typography>
          ) : (
            <Typography variant="subtitle1" style={{ padding: '8px 16px' }}>
              No data
            </Typography>
          )}
        </div>
      )}
    </div>
  );
}
