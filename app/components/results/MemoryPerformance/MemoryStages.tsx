import React, { useContext, CSSProperties, useState } from 'react';
import _ from 'lodash';
import { FixedSizeList } from 'react-window';
import {
  Backdrop,
  Button,
  Checkbox,
  Fade,
  FormControlLabel,
  ListItem,
  makeStyles,
  Modal,
  TextField,
  Typography,
} from '@material-ui/core';
import { MemoryPerformanceContext } from '../../../context/MemoryPerformanceContext';
import { TabContext } from '../../../context/TabContext';

const useStyles = makeStyles((theme) => ({
  element: {
    marginBottom: theme.spacing(1),
  },
  paper: {
    position: 'absolute',
    width: 500,
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(2, 4, 3),
    outline: 'none',
    borderRadius: 4,
  },
  listItemRoot: {
    // padding: 0,
  },
}));

/**
 * A dialog for changing which memory stages to show
 */
export default function MemoryStages() {
  const classes = useStyles();
  const memoryPerformanceContext = useContext(MemoryPerformanceContext);
  const [validStages, setValidStages] = [
    memoryPerformanceContext?.validStages,
    memoryPerformanceContext?.setValidStages,
  ];
  const [openStage, setOpenStage] = [
    memoryPerformanceContext?.openStage,
    memoryPerformanceContext?.setOpenStage,
  ];
  const [stagesChecked, setStagesChecked] = [
    memoryPerformanceContext?.stagesChecked,
    memoryPerformanceContext?.setStagesChecked,
  ];
  const [filteredStages, setFilteredStages] = [
    memoryPerformanceContext?.filteredStages,
    memoryPerformanceContext?.setFilteredStages,
  ];
  const [allChecked, setAllChecked] = useState(false);

  const { state: tabState } = useContext(TabContext);
  const { activeTabID, tabs } = tabState;
  const tab = tabs[activeTabID];
  const { recordings, activeRecordingID } = tab;
  const recording = recordings[activeRecordingID || ''];
  const stageTimes = recording?.stageTimes;

  const modalStyle = {
    top: '50%',
    left: '50%',
    transform: 'translate(-50%,-50%)',
  };

  function validStageToggle(stage: string) {
    if (_.includes(validStages, stage) && validStages) {
      setValidStages?.call(
        undefined,
        validStages?.filter((obj) => obj !== stage)
      );
    } else if (validStages) {
      setValidStages?.call(undefined, validStages?.concat(stage));
    }
  }

  function handleStageToggle(value: string) {
    const curIndex = stagesChecked?.indexOf(value);
    const newChecked = [...(stagesChecked || '')];

    if (curIndex === -1) {
      validStageToggle(value);
      newChecked.push(value);
    } else {
      validStageToggle(value);

      if (curIndex !== undefined) {
        newChecked.splice(curIndex, 1);
      }
    }
    setStagesChecked?.call(undefined, newChecked);
  }

  function handleStageCheckAllToggle() {
    if (allChecked) {
      setAllChecked(false);
      setStagesChecked?.call(undefined, []);
      setValidStages?.call(undefined, []);
    } else {
      setAllChecked(true);
      if (stageTimes) {
        setStagesChecked?.call(
          undefined,
          _.map(_.uniqBy(stageTimes, 'stage'), 'stage')
        );
        setValidStages?.call(
          undefined,
          _.map(_.uniqBy(stageTimes, 'stage'), 'stage')
        );
      }
    }
  }

  // Opens the modal to select event
  function handleOpenStage() {
    if (stageTimes) {
      setFilteredStages?.call(
        undefined,
        _.map(_.uniqBy(stageTimes, 'stage'), 'stage')
      );
    }
    setOpenStage?.call(undefined, true);
  }

  function handleCloseStage() {
    setOpenStage?.call(undefined, false);
  }

  // Sets filteredEvents with a list of the datakeys that matches the text in the search bar
  function searchBarFilter(search: string) {
    let filteredList = [''];

    if (stageTimes) {
      filteredList = _.filter(
        _.map(_.uniqBy(stageTimes, 'stage'), 'stage'),
        (o) => _.includes(o.toLowerCase(), search.toLowerCase())
      );
    }
    setFilteredStages?.call(undefined, filteredList);
  }

  function renderStageRow(props: { index: number; style: CSSProperties }) {
    const { index, style } = props;

    const stage = filteredStages ? filteredStages[index] : '';

    const labelId = `checkbox-list-label-${stage}`;
    return (
      <ListItem
        classes={{ root: classes.listItemRoot }}
        style={style}
        key={stage}
        role={undefined}
        dense
        button
        onClick={() => handleStageToggle(stage)}
      >
        <FormControlLabel
          control={
            <Checkbox
              edge="start"
              checked={stagesChecked?.indexOf(stage) !== -1}
              tabIndex={-1}
              disableRipple
              inputProps={{ 'aria-labelledby': labelId }}
              color="primary"
            />
          }
          label={`${stage}`}
        />
      </ListItem>
    );
  }
  return (
    <div>
      {/* Stage modal */}
      <Button variant="outlined" color="primary" onClick={handleOpenStage}>
        Pick Stages
      </Button>
      <Modal
        open={openStage !== undefined ? openStage : false}
        onClose={handleCloseStage}
        aria-labelledby="simple-modal-title"
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={openStage}>
          <div style={modalStyle} className={classes.paper}>
            <div className={classes.element}>
              <Typography variant="h5">Pick stages to show</Typography>
            </div>
            <form>
              <TextField
                id="filled-basic"
                label="Search stage"
                variant="filled"
                onChange={(e) => searchBarFilter(e.target.value)}
              />
            </form>
            <FormControlLabel
              style={{ marginLeft: 0 }}
              control={
                <Checkbox
                  edge="start"
                  checked={allChecked}
                  tabIndex={-1}
                  disableRipple
                  inputProps={{ 'aria-labelledby': 'eventCheckAll' }}
                  color="primary"
                  onClick={() => handleStageCheckAllToggle()}
                />
              }
              label="Show all"
            />
            <FixedSizeList
              height={400}
              width="100%"
              itemSize={48}
              itemCount={
                filteredStages !== undefined ? filteredStages?.length : 0
              }
            >
              {renderStageRow}
            </FixedSizeList>
          </div>
        </Fade>
      </Modal>
    </div>
  );
}
