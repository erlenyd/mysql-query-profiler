import React, { useContext, CSSProperties, useState } from 'react';
import _ from 'lodash';
import { FixedSizeList } from 'react-window';
import {
  Backdrop,
  Button,
  Checkbox,
  Fade,
  FormControlLabel,
  ListItem,
  makeStyles,
  Modal,
  TextField,
  Typography,
} from '@material-ui/core';
import { MemoryPerformanceContext } from '../../../context/MemoryPerformanceContext';
import { TabContext } from '../../../context/TabContext';

const useStyles = makeStyles((theme) => ({
  element: {
    marginBottom: theme.spacing(1),
  },
  paper: {
    position: 'absolute',
    width: 500,
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(2, 4, 3),
    outline: 'none',
    borderRadius: 4,
  },
  listItemRoot: {
    // padding: 0,
  },
}));

/**
 * A dialog for changing which memory events to show
 */
export default function MemoryEvents() {
  const classes = useStyles();
  const memoryPerformanceContext = useContext(MemoryPerformanceContext);
  const [disabled, setDisabled] = [
    memoryPerformanceContext?.disabled,
    memoryPerformanceContext?.setDisabled,
  ];
  const [openEvent, setOpenEvent] = [
    memoryPerformanceContext?.openEvent,
    memoryPerformanceContext?.setOpenEvent,
  ];
  const [eventsChecked, setEventsChecked] = [
    memoryPerformanceContext?.eventsChecked,
    memoryPerformanceContext?.setEventsChecked,
  ];
  const [filteredEvents, setFilteredEvents] = [
    memoryPerformanceContext?.filteredEvents,
    memoryPerformanceContext?.setFilteredEvents,
  ];
  const [allChecked, setAllChecked] = useState(false);

  const { state: tabState } = useContext(TabContext);
  const { activeTabID, tabs } = tabState;
  const tab = tabs[activeTabID];
  const { recordings, activeRecordingID } = tab;
  const recording = recordings[activeRecordingID || ''];
  const chartData = recording?.chartData;

  const modalStyle = {
    top: '50%',
    left: '50%',
    transform: 'translate(-50%,-50%)',
  };

  // When a legend is clicked the datakey is added to the disabled list, which will remove it form the line chart
  function eventToggle(dataKey: string) {
    if (disabled) {
      if (_.includes(disabled, dataKey)) {
        setDisabled?.call(
          undefined,
          disabled.filter((obj) => obj !== dataKey)
        );
      } else {
        setDisabled?.call(undefined, disabled.concat(dataKey));
      }
    }
  }

  function handleEventToggle(value: string) {
    const curIndex = eventsChecked?.indexOf(value);
    const newChecked = [...(eventsChecked || '')];

    if (curIndex === -1) {
      eventToggle(value);
      newChecked.push(value);
    } else {
      eventToggle(value);
      if (curIndex !== undefined) {
        newChecked.splice(curIndex, 1);
      }
    }
    setEventsChecked?.call(undefined, newChecked);
  }

  function handleEventCheckAllToggle() {
    if (allChecked) {
      setAllChecked(false);
      if (chartData) {
        setEventsChecked?.call(undefined, _.keys(chartData[0]).slice(1));
        setDisabled?.call(undefined, _.keys(chartData[0]).slice(1));
      }
    } else {
      setAllChecked(true);
      setEventsChecked?.call(undefined, []);
      setDisabled?.call(undefined, []);
    }
  }

  // Opens the modal to select event
  function handleOpenEvent() {
    if (chartData) {
      setFilteredEvents?.call(undefined, _.keys(chartData[0]).slice(1));
    }
    setOpenEvent?.call(undefined, true);
  }

  function handleCloseEvent() {
    setOpenEvent?.call(undefined, false);
  }

  // Sets filteredEvents with a list of the datakeys that matches the text in the search bar
  function searchBarFilter(search: string) {
    let filteredList = [''];
    if (chartData) {
      filteredList = _.filter(_.keys(chartData[0]).slice(1), (o) =>
        _.includes(o.toLowerCase(), search.toLowerCase())
      );
    }
    setFilteredEvents?.call(undefined, filteredList);
  }

  function renderEventRow(props: { index: number; style: CSSProperties }) {
    const { index, style } = props;

    const eventType = filteredEvents ? filteredEvents[index] : '';

    const labelId = `checkbox-list-label-${eventType}`;
    return (
      <ListItem
        classes={{ root: classes.listItemRoot }}
        style={style}
        key={eventType}
        role={undefined}
        dense
        button
        onClick={() => handleEventToggle(eventType)}
      >
        <FormControlLabel
          control={
            <Checkbox
              edge="start"
              checked={eventsChecked?.indexOf(eventType) === -1}
              tabIndex={-1}
              disableRipple
              inputProps={{ 'aria-labelledby': labelId }}
              color="primary"
            />
          }
          label={`${eventType}`}
        />
      </ListItem>
    );
  }

  return (
    <div>
      {' '}
      <Button
        variant="outlined"
        color="primary"
        onClick={handleOpenEvent}
        style={{ marginRight: 16 }}
      >
        Pick Events
      </Button>
      <Modal
        open={openEvent !== undefined ? openEvent : false}
        onClose={handleCloseEvent}
        aria-labelledby="simple-modal-title"
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={openEvent}>
          <div style={modalStyle} className={classes.paper}>
            <div className={classes.element}>
              <Typography variant="h5">Pick events to show</Typography>
            </div>
            <form>
              <TextField
                id="filled-basic"
                label="Search event..."
                variant="filled"
                onChange={(e) => searchBarFilter(e.target.value)}
              />
            </form>
            <FormControlLabel
              style={{ marginLeft: 0 }}
              control={
                <Checkbox
                  edge="start"
                  checked={allChecked}
                  tabIndex={-1}
                  disableRipple
                  inputProps={{ 'aria-labelledby': 'allChecked' }}
                  color="primary"
                  onClick={() => handleEventCheckAllToggle()}
                />
              }
              label="Show all"
            />

            <FixedSizeList
              height={400}
              width="100%"
              itemSize={48}
              itemCount={
                filteredEvents !== undefined ? filteredEvents?.length : 0
              }
            >
              {renderEventRow}
            </FixedSizeList>
          </div>
        </Fade>
      </Modal>
    </div>
  );
}
