/* eslint-disable react/jsx-one-expression-per-line */
import { makeStyles, Typography, Paper, Button } from '@material-ui/core';
import _ from 'lodash';
import React, { useContext, useEffect, useState } from 'react';
import {
  LineChart,
  Line,
  CartesianGrid,
  XAxis,
  YAxis,
  Tooltip,
  ResponsiveContainer,
  ReferenceLine,
  ReferenceArea,
  Label,
} from 'recharts';
import ZoomOutIcon from '@material-ui/icons/ZoomOut';
import MemoryEvents from './MemoryEvents';
import MemoryStages from './MemoryStages';
import { MemoryPerformanceContext } from '../../../context/MemoryPerformanceContext';
import { TabContext } from '../../../context/TabContext';

const useStyles = makeStyles((theme) => ({
  lineChart: {
    zIndex: 999,
  },
  buttonWrapper: {
    display: 'flex',
    flexFlow: 'row nowrap',
    padding: `${theme.spacing(1)}px ${theme.spacing(2)}px`,
  },
  tooltipContent: {
    margin: 5,
    padding: 2,
  },
  dot: {
    height: 10,
    width: 10,
    borderRadius: 50,
    display: 'inline-block',
    marginRight: 5,
  },
}));

/**
 * A graph showing the memory usage over time for a query
 */
export default function MemoryPerformance() {
  const classes = useStyles();

  const memoryPerformanceContext = useContext(MemoryPerformanceContext);
  const { state: tabState } = useContext(TabContext);
  const { activeTabID, tabs } = tabState;
  const tab = tabs[activeTabID];
  const { recordings, activeRecordingID } = tab;
  const recording = recordings[activeRecordingID || ''];
  const stageTimes = recording?.stageTimes;
  const chartData = recording?.chartData;
  const testColors = recording?.chartColors;
  const top3List = recording?.top3Memory;

  const [eventsChecked, setEventsChecked] = [
    memoryPerformanceContext?.eventsChecked,
    memoryPerformanceContext?.setEventsChecked,
  ];
  const [disabled, setDisabled] = [
    memoryPerformanceContext?.disabled,
    memoryPerformanceContext?.setDisabled,
  ];
  const [activeName, setActiveName] = useState('');
  const [activeColor, setActiveColor] = useState('');
  const [activeIndex, setActiveIndex] = useState(0);
  const [activeValue, setActiveValue] = useState(0);
  const [activeTooltip, setActiveTooltip] = useState(false);

  const [top, setTop] = useState(0);
  const [left, setLeft] = useState(0);
  const [right, setRight] = useState(0);

  const validStages = memoryPerformanceContext?.validStages;

  function getCurrentMaxY(data: typeof chartData) {
    const values = _.values(data);
    const filteredValues = _.map(values, (o) =>
      _.omitBy(o, (value, key) => _.includes(eventsChecked, key))
    );
    const valueList = _.flatten(_.map(filteredValues, (o) => _.values(o)));
    return Number(_.max(valueList));
  }

  function getMaxTime() {
    // eslint-disable-next-line prefer-spread
    return Number(_.max(_.map(chartData, 'relative_time')));
  }

  const [refAreaLeft, setRefAreaLeft] = useState<string | undefined>(undefined);
  const [refAreaRight, setRefAreaRight] = useState<string | undefined>(
    undefined
  );

  useEffect(() => {
    if (top3List && chartData) {
      const notTop3 = _.filter(
        _.keys(chartData[0]).slice(1),
        (o) => !_.includes(top3List, o)
      );
      setDisabled?.call(undefined, notTop3);
      setEventsChecked?.call(undefined, notTop3);
      setTop(getCurrentMaxY(chartData));
      setLeft(0);
      setRight(getMaxTime());
    }
  }, [top3List]);

  function formatBytes(byte: number) {
    if (Math.abs(byte) > 10000000) {
      return `${Math.floor(byte / 1024 / 1024)} MB`;
    }
    if (Math.abs(byte) > 10000) {
      return `${Math.floor(byte / 1024)} KB`;
    }
    return `${Math.floor(byte)} B`;
  }

  function formatXAxis(time: number) {
    return `${time.toFixed(2)}s`;
  }

  function getMaxZoomY(leftIndex: number, rightIndex: number) {
    const values = _.values(chartData);

    const indexLeft = _.findIndex(values, (o) => o.relative_time === leftIndex);
    const indexRight = _.findIndex(
      values,
      (o) => o.relative_time === rightIndex
    );

    const zoomedValues = _.values(chartData).slice(indexLeft, indexRight);

    return getCurrentMaxY(zoomedValues);
  }

  function zoom() {
    let [refAreaLeftTemp, refAreaRightTemp] = [
      Number(refAreaLeft),
      Number(refAreaRight),
    ];
    if (refAreaLeft === refAreaRight || refAreaRight === undefined) {
      setRefAreaLeft(undefined);
      setRefAreaRight(undefined);

      return;
    }
    if (
      left !== undefined &&
      right !== undefined &&
      refAreaLeft !== undefined
    ) {
      if (refAreaLeftTemp > refAreaRightTemp) {
        [refAreaLeftTemp, refAreaRightTemp] = [
          refAreaRightTemp,
          refAreaLeftTemp,
        ];
      }
      setLeft(refAreaLeftTemp);
      setRight(refAreaRightTemp);
      setTop(getMaxZoomY(refAreaLeftTemp, refAreaRightTemp));
      if (refAreaLeft !== undefined && refAreaRight !== undefined) {
        setRefAreaLeft(undefined);
        setRefAreaRight(undefined);
      }
    }
  }

  function zoomOut() {
    if (
      refAreaLeft === undefined &&
      refAreaRight === undefined &&
      left !== undefined
    ) {
      setLeft(0);
      setRight(getMaxTime());
      setTop(getCurrentMaxY(chartData));
    }
  }

  function setTooltipInfo(props: any) {
    const color = props.stroke;
    setActiveColor(color);
    setActiveName(_.findKey(testColors, (value) => value === color) || '');
    setActiveValue(props.points[activeIndex].value);
    setActiveTooltip(true);
  }

  useEffect(() => {
    zoomOut();
  }, [eventsChecked]);

  function CustomTooltip(props: any) {
    if (activeTooltip) {
      const { label } = props;
      return (
        <Paper elevation={3}>
          <div className={classes.tooltipContent}>
            <p>Time: {`${formatXAxis(label)}`}</p>
            <p>
              <span
                className={classes.dot}
                style={{ backgroundColor: activeColor }}
              />
              {`${activeName}`}:{`${formatBytes(activeValue)}`}
            </p>
          </div>
        </Paper>
      );
    }
    return null;
  }

  return (
    <div className={classes.lineChart}>
      <div className={classes.buttonWrapper}>
        <div style={{ flexGrow: 1 }}>
          <Typography variant="h6">Memory performance</Typography>
        </div>
        <MemoryEvents />
        <MemoryStages />
      </div>
      {chartData ? (
        <div>
          <Button
            variant="outlined"
            color="primary"
            onClick={zoomOut}
            style={{ marginRight: 16, marginLeft: '8px', marginBottom: '8px' }}
          >
            Zoom out
            <ZoomOutIcon style={{ marginLeft: '4px' }} />
          </Button>
        </div>
      ) : null}

      {chartData && chartData.length > 2 ? (
        <div>
          <ResponsiveContainer height={400}>
            <LineChart
              width={800}
              height={500}
              data={chartData}
              margin={{ top: 20, right: 50, left: 20, bottom: 5 }}
              style={{ fontFamily: 'Roboto, sans-serif' }}
              onMouseDown={(e) => {
                if (e !== null) setRefAreaLeft(e.activeLabel);
              }}
              onMouseMove={(e) => refAreaLeft && setRefAreaRight(e.activeLabel)}
              onMouseUp={zoom}
            >
              <CartesianGrid strokeDasharray="3 10" />
              {/* TODO: Create our own Axis tick component like this: http://recharts.org/en-US/examples/CustomizedLabelLineChart */}
              <XAxis
                allowDataOverflow
                dataKey="relative_time"
                domain={[left, right]}
                type="number"
                tickCount={20}
                stroke="#FFFFFF"
                tickFormatter={formatXAxis}
              />
              <YAxis
                allowDataOverflow
                dataKey="memory"
                yAxisId="1"
                tickFormatter={formatBytes}
                domain={[0, top * 1.05]}
                type="number"
                stroke="#FFFFFF"
              />
              <Tooltip
                allowEscapeViewBox={{ y: true }}
                content={CustomTooltip}
              />

              {/* Creates an array from the key:value pairs in chartColors.
             The keys that are not disabled becomes a line */}
              {_.toPairs(testColors)
                .filter((pair) => !_.includes(disabled, pair[0]))
                .map((pair) => (
                  <Line
                    type="monotone"
                    dataKey={pair[0]}
                    key={pair[0]}
                    stroke={pair[1]}
                    strokeWidth={2}
                    dot={false}
                    yAxisId="1"
                    activeDot={(props) => setActiveIndex(props.index)}
                    onMouseEnter={(props) => setTooltipInfo(props)}
                    onMouseLeave={() => setActiveTooltip(false)}
                  />
                ))}
              {/* Filters stageTimes to only include stages that are in validStages and only the first instance of them.
                  en creates reference lines from the data that remains */}

              {_.forEach(_.uniqBy(stageTimes, 'stage'))
                ?.filter((stage) => _.includes(validStages, stage.stage))
                ?.map((item) => (
                  <ReferenceLine
                    x={item.startTime}
                    stroke="#769CFF"
                    key={_.uniqueId('ref_')}
                    yAxisId="1"
                  >
                    <Label
                      value={item.stage}
                      position="top"
                      style={{ fill: '#ffffff' }}
                    />
                  </ReferenceLine>
                ))}
              {refAreaLeft && refAreaRight ? (
                <ReferenceArea
                  yAxisId="1"
                  x1={refAreaLeft}
                  x2={refAreaRight}
                  strokeOpacity={0.3}
                />
              ) : null}
            </LineChart>
          </ResponsiveContainer>
          <div style={{ marginLeft: '8px' }}>
            <Typography variant="subtitle2">
              Mark an area on the graph to zoom
            </Typography>
          </div>
        </div>
      ) : (
        <div>
          {chartData ? (
            <Typography variant="subtitle1" style={{ padding: '8px 16px' }}>
              There are not enough data points to show a meaningful graph
            </Typography>
          ) : (
            <Typography variant="subtitle1" style={{ padding: '8px 16px' }}>
              No data
            </Typography>
          )}
        </div>
      )}
    </div>
  );
}
