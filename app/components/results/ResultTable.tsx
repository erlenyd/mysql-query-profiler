import React, { useContext } from 'react';
import { makeStyles, withStyles, createStyles } from '@material-ui/core/styles';
import { TableCell, Typography } from '@material-ui/core';
import { AutoSizer, Column, Table } from 'react-virtualized';
import { uniqueId } from 'lodash';
import clsx from 'clsx';
import { TabContext } from '../../context/TabContext';

const useStyles = makeStyles((theme) => ({
  tableWrapper: {
    minHeight: 200,
    height: '100%',
    width: '100%',
    resize: 'both',
    overflowY: 'auto',
    overflowX: 'hidden',
    scrollbarWidth: 'none',
    paddingBottom: theme.spacing(2),
  },
  flexContainer: {
    display: 'flex',
    alignItems: 'center',
    boxSizing: 'border-box',
  },
  tableHeader: {
    flex: '1 1 !important',
    backgroundColor: '#404040',
  },
  tableRow: {
    display: 'flex',
    flex: '1 1 !important',
    textOverflow: 'ellipsis',
    whiteSpace: 'nowrap',
    '& .ReactVirtualized__Table__rowColumn': {
      flex: '1 1 !important',
      textOverflow: 'ellipsis',
      whiteSpace: 'nowrap',
    },
  },
  tableRowHover: {
    '&:hover': {
      backgroundColor: '#404040',
    },
  },
  tableCell: {
    flex: '1 1 !important',
    textOverflow: 'ellipsis',
    whiteSpace: 'nowrap',
    overflowY: 'hidden',
  },
}));

const StyledTableCell = withStyles(() =>
  createStyles({
    head: {
      backgroundColor: '#404040',
    },
    body: {
      fontSize: 14,
    },
  })
)(TableCell);

/**
 * A table showing the results of a query. Virtualized
 */
export default function ResultTable() {
  const { state: tabState } = useContext(TabContext);
  const { activeTabID, tabs } = tabState;
  const tab = tabs[activeTabID];
  const recordings = tab?.recordings || {};
  const activeRecordingID = tab?.activeRecordingID || '';
  const result = recordings[activeRecordingID]?.queryOutput;

  const classes = useStyles();

  if (!result || !result.results) {
    return (
      <Typography style={{ padding: '8px 16px' }} variant="subtitle1">
        No data to show
      </Typography>
    );
  }

  const { labels, types, values } = result?.results;

  function formatDate(d: Date) {
    let month = `${d.getMonth() + 1}`;
    let day = `${d.getDate()}`;
    const year = d.getFullYear();

    if (month.length < 2) month = `0${month}`;
    if (day.length < 2) day = `0${day}`;

    return [year, month, day].join('-');
  }

  function format(index: number, entry: unknown): string {
    let datatype = 0;
    if (types) {
      datatype = types[index];
    }
    if (datatype === 12) {
      if (typeof entry === 'number') {
        const date = new Date(Number(entry));
        return formatDate(date);
      }
      const date = new Date(String(entry));
      return formatDate(date);
    }
    return String(entry);
  }

  function headerRenderer(label: string, columnIndex: number) {
    return (
      <StyledTableCell
        key={columnIndex}
        component="div"
        variant="head"
        className={clsx(classes.tableCell, classes.flexContainer)}
        style={{ height: 48 }}
      >
        {label}
      </StyledTableCell>
    );
  }

  function cellRenderer(rowIndex: number, columnIndex: number) {
    return (
      <TableCell
        key={`${columnIndex}-${rowIndex}`}
        component="div"
        variant="body"
        className={clsx(classes.tableCell, classes.flexContainer)}
      >
        {format(columnIndex, values[rowIndex][columnIndex])}
      </TableCell>
    );
  }

  return (
    <div className={classes.tableWrapper}>
      <div
        style={{
          minHeight: 201,
          height: '100%',
          width: '100%',
          paddingBottom: 1,
        }}
      >
        <AutoSizer>
          {({ height, width }) => (
            <Table
              height={height}
              width={width}
              rowHeight={48}
              headerHeight={48}
              gridStyle={{
                direction: 'inherit',
                outline: 'none',
              }}
              rowCount={values.length}
              estimatedColumnSize={100}
              rowGetter={({ index }) => values[Number(index)]}
              rowClassName={({ index }) =>
                clsx(classes.tableRow, classes.flexContainer, {
                  [classes.tableRowHover]: index !== -1,
                })
              }
            >
              {labels?.map((label, index) => (
                <Column
                  key={`${label}+${uniqueId()}`}
                  headerRenderer={() => headerRenderer(label, index)}
                  headerClassName={classes.tableHeader}
                  className={classes.flexContainer}
                  cellRenderer={({ rowIndex }) => cellRenderer(rowIndex, index)}
                  dataKey={label}
                  width={100}
                />
              ))}
            </Table>
          )}
        </AutoSizer>
      </div>
    </div>
  );
}
