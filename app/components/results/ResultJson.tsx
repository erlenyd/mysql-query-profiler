import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { TextField } from '@material-ui/core';

import clsx from 'clsx';

interface TResultProps {
  result: string;
  textColor: string;
}

/**
 * Unused. Shows the result as pure json
 * @param props The result and textcolor
 */
export default function ResultJson(props: TResultProps) {
  const useStyles = makeStyles((theme) => ({
    container: {
      maxWidth: 600 + theme.spacing(4),
    },
    content: {
      display: 'flex',
      flexFlow: 'column nowrap',
      paddingTop: theme.spacing(1),
      paddingBottom: theme.spacing(1),
      paddingLeft: theme.spacing(2),
      paddingRight: theme.spacing(2),
    },
    element: {
      marginBottom: theme.spacing(1),
    },
    input: {
      display: 'flex',
      flexFlow: 'column nowrap',
      flexGrow: 1,
    },
    result: {
      userSelect: 'none',
    },
    code: {
      fontFamily: 'Roboto Mono !important',
      color: props.textColor,
    },
  }));

  const classes = useStyles();
  const { result } = props;
  return (
    <div className={clsx(classes.element, classes.input)}>
      <TextField
        className={classes.result}
        InputProps={{
          classes: {
            input: classes.code,
          },
        }}
        multiline
        rows={10}
        variant="outlined"
        value={result}
      />
    </div>
  );
}
