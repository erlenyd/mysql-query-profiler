import React, { useState, useContext } from 'react';
import {
  TextField,
  Button,
  Paper,
  Typography,
  makeStyles,
  LinearProgress,
  FormControlLabel,
  Checkbox,
} from '@material-ui/core';
import { useHistory, useLocation } from 'react-router';
import clsx from 'clsx';
import routes from '../../routes';
import { GlobalSnackbarContext } from '../../context/GlobalSnackbarContext';
import { TabContext } from '../../context/TabContext';

const useStyles = makeStyles((theme) => ({
  wrapper: {
    display: 'flex',
    flexFlow: 'column nowrap',
    flexShrink: 1,
    // alignItems: 'center',
    maxWidth: 300 + theme.spacing(4),
  },
  container: {
    display: 'flex',
    flexFlow: 'column nowrap',
    flexGrow: 0,
    flexShrink: 1,
    width: 300,
    padding: `${theme.spacing(1)}px ${theme.spacing(2)}px`,
  },
  content: {
    display: 'flex',
    flexFlow: 'column nowrap',
  },
  element: {
    marginBottom: theme.spacing(1),
  },
  title: {
    textAlign: 'center',
    marginBottom: theme.spacing(2),
  },
  errorMessage: {
    textAlign: 'center',
    whiteSpace: 'pre-line',
    wordWrap: 'break-word',
  },
  progressBar: {
    borderRadius: '2px',
  },
}));

interface LoginProps {
  onConnect?: () => void;
  tabID?: string;
}

const DefaultProps = {
  onConnect: undefined,
  tabID: undefined,
};

/**
 * The form for logging in to a database.
 * @param onConnect: A function to call when finishing connection. Used if Login is in a dialog
 * @param tabID: The ID of the tab this was opened for. Is necessary instead of the active tab ID since inactive tabs may change connection
 */
export default function Login({
  onConnect = undefined,
  tabID = undefined,
}: LoginProps) {
  const snackBarContext = useContext(GlobalSnackbarContext);
  const { state: tabState, dispatch } = useContext(TabContext);
  const { activeTabID, displayTabs, manager, tabs } = tabState;
  const id = tabID || activeTabID;
  const tab = tabs[id];

  const history = useHistory();
  const location = useLocation();

  const [host, setHost] = useState(tab?.loginDetails?.host || '');
  const [user, setUser] = useState(tab?.loginDetails?.user || '');
  const [password, setPassword] = useState(tab?.loginDetails?.password || '');
  const [port, setPort] = useState(Number(tab?.loginDetails?.port || 33060));
  const [database, setDatabase] = useState(tab?.loginDetails?.database || '');
  const [connecting, setConnecting] = useState(false);
  const [errorMessage, setErrorMessage] = useState('');
  const [savePassword, setSavePassword] = useState(
    tab?.loginDetails?.savePassword || false
  );

  async function handleConnectByLogin() {
    setConnecting(true);
    try {
      await manager.connect(id, tab?.serial, {
        host,
        user,
        password,
        port,
        database,
        savePassword,
      });
      dispatch({
        type: 'SET_LOGIN_DETAILS',
        payload: {
          id,
          loginDetails: {
            ...{ host, user, password, port },
            database,
            savePassword,
          },
        },
      });
      setErrorMessage('');
      snackBarContext?.setPopupMessage('Connected successfully');
      snackBarContext?.setPopupSeverity('success');
      snackBarContext?.setPopupOpen(true);
      const oldLabel = displayTabs[id].tabLabel;
      if (
        oldLabel === 'New connection' ||
        oldLabel.includes(':') ||
        oldLabel === ''
      ) {
        dispatch({
          type: 'SET_DISPLAYTAB_LABEL',
          payload: { id, label: `${host}:${database}` },
        });
      }
      if (location.pathname === routes.login) {
        history.push(routes.dashboard);
      } else if (onConnect) {
        onConnect();
      }
    } catch (error) {
      console.error(error);
      if (error?.info?.msg?.includes('Unknown database')) {
        const newErrorMessage = `${error.info.msg.replace(
          'Unknown',
          'The'
        )} does not exist on the chosen server`;
        setErrorMessage(newErrorMessage);
      } else if (String(error).includes('database was not passed')) {
        setErrorMessage('There was no database to connect to');
      } else if (
        String(error).includes('EAI_AGAIN') ||
        String(error).includes('ENOTFOUND')
      ) {
        setErrorMessage(`The host '${host}' could not be found`);
      } else if (String(error).includes('Access denied')) {
        setErrorMessage(`Wrong username or password, user: ${user}@${host}`);
      } else if (String(error).includes('The server has gone away')) {
        setErrorMessage(
          `The server connection is not using the X Protocol.
          Make sure you are connecting to the correct port and using a MySQL 5.7.12 (or higher) server instance.
          Default port for X Protocol servers is 33060`
        );
      } else if (String(error).includes('Insufficient DB-user privileges')) {
        setErrorMessage(String(error));
      } else {
        setErrorMessage(`Failed to connect: ${error}`);
      }

      setConnecting(false);
    }
  }

  function handleKeyUp(e: { key: string }) {
    if (e.key === 'Enter') {
      handleConnectByLogin();
    }
  }

  const classes = useStyles();

  return (
    <div className={classes.wrapper}>
      <Paper className={classes.container} elevation={1}>
        <Typography className={classes.title} variant="h5">
          Connect to a database
        </Typography>
        <div className={clsx(classes.content, classes.element)}>
          <TextField
            autoFocus
            id="host-input"
            className={classes.element}
            variant="filled"
            label="Host"
            value={host}
            onChange={(event) => setHost(event.target.value)}
            onKeyUp={handleKeyUp}
          />
          <TextField
            id="user-input"
            className={classes.element}
            variant="filled"
            label="User"
            value={user}
            onChange={(event) => setUser(event.target.value)}
            onKeyUp={handleKeyUp}
          />
          <TextField
            className={classes.element}
            variant="filled"
            label="Password"
            type="password"
            value={password}
            onChange={(event) => setPassword(event.target.value)}
            onKeyUp={handleKeyUp}
          />
          <TextField
            className={classes.element}
            variant="filled"
            label="Port"
            value={port}
            onChange={(event) => setPort(Number(event.target.value))}
            onKeyUp={handleKeyUp}
          />
          <TextField
            className={classes.element}
            variant="filled"
            label="Database"
            value={database}
            onChange={(event) => setDatabase(event.target.value)}
            onKeyUp={handleKeyUp}
          />
        </div>
        <FormControlLabel
          label="Save password"
          control={
            <Checkbox
              color="primary"
              checked={savePassword}
              onChange={(event) => setSavePassword(event.target.checked)}
            />
          }
        />
        {!!errorMessage && (
          <Typography
            color="error"
            className={clsx(classes.element, classes.errorMessage)}
          >
            {errorMessage}
          </Typography>
        )}
        <Button
          className={classes.element}
          type="button"
          variant="contained"
          color="primary"
          onClick={() => handleConnectByLogin()}
        >
          Connect
        </Button>
        <LinearProgress
          hidden={!connecting}
          className={clsx(classes.element, classes.progressBar)}
          color="primary"
        />
      </Paper>
    </div>
  );
}

Login.defaultProps = DefaultProps;
