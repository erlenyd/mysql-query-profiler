import { Icon, IconButton, makeStyles } from '@material-ui/core';
import { keys, max, min, valuesIn } from 'lodash';
import React, { useContext, useEffect, useRef, useState } from 'react';
import clsx from 'clsx';
import { TabContext } from '../../context/TabContext';
import Tab from './Tab';
import noScrollStyles from './no-scrollbar.module.css';

const useStyles = (tabWidth: number, tabsWidth: number) =>
  makeStyles((theme) => ({
    wrapper: {
      display: 'flex',
      flexFlow: 'row nowrap',
      alignItems: 'center',
      justifyItems: 'center',
      maxHeight: 40,
      backgroundColor: theme.palette.primary.dark,
    },
    tabsWrapper: {
      display: 'flex',
      flexFlow: 'column nowrap',
      alignItems: 'flex-start',
      justifyItems: 'center',
      maxHeight: 40,
      maxWidth: tabsWidth + 38,
      overflow: 'scroll',
    },
    tabs: {
      display: 'flex',
      flexFlow: 'row nowrap',
      alignItems: 'center',
      justifyItems: 'flex-start',
    },
    endButtons: {
      display: 'flex',
      flexFlow: 'row nowrap',
      flexGrow: 1,
      justifyContent: 'space-between',
      marginLeft: theme.spacing(1),
    },
    activeIndicatorWrapper: {
      display: 'inline-block',
      // flexFlow: 'row nowrap',
      width: '100%',
    },
    activeIndicator: {
      flexGrow: 1,
      flexShrink: 1,
      flexBasis: tabWidth,
      maxWidth: tabWidth,
      minWidth: tabWidth,
      height: 2,
      minHeight: 2,
      marginTop: 1,
      backgroundColor: theme.palette.primary.main,
      transitionProperty: 'margin-left',
      transitionDuration: '0.2s',
    },
  }));

/**
 * The tab bar, responsible for containing and managing the displayed tabs. Is meant to also contain the buttons for manipulating the app later (close app, minimize/maximize etc.)
 */
export default function TabBar() {
  const { state: tabState, dispatch } = useContext(TabContext);
  const { activeTabID, displayTabs, tabs } = tabState;
  const tab = tabs[activeTabID];

  const [activeTabIndex, setActiveTabIndex] = useState(0);
  const [width, setWidth] = useState(0);
  // const [maximized, setMaximized] = useState(true);

  const wrapperRef = useRef<HTMLDivElement>(null);
  const tabsWrapperRef = useRef<HTMLDivElement>(null);

  const availableWidth = width - 38 - 360;
  const numberOfTabs = keys(displayTabs).length;
  const fullTabWidth = 196;
  const minTabWidth = 110;
  const isFull = availableWidth < numberOfTabs * fullTabWidth;

  const tabWidth = !isFull
    ? fullTabWidth
    : max([
        min([availableWidth / keys(displayTabs).length, fullTabWidth]),
        minTabWidth,
      ]) || 0;

  useEffect(() => {
    const tabIndex = activeTabID
      ? valuesIn(displayTabs).findIndex((value) => value.tabID === activeTabID)
      : 0;
    setActiveTabIndex(tabIndex);
    if (tabsWrapperRef.current && isFull) {
      // Scroll to the active tab
      // Item 0 is the div containing the tabs
      // Item tabIndex is the active tab within item 0
      tabsWrapperRef.current.children
        .item(0)
        ?.children.item(tabIndex)
        ?.scrollIntoView({ behavior: 'auto' });
    }
  }, [activeTabID, displayTabs]);

  useEffect(() => {
    const nextWidth = wrapperRef?.current
      ? wrapperRef?.current?.offsetWidth
      : 0;
    setWidth(nextWidth);
  }, [wrapperRef.current]);

  // function toggleMaximized() {
  //   const window = remote?.getCurrentWindow();
  //   if (window?.isMaximized()) {
  //     window?.unmaximize();
  //     setMaximized(false);
  //   } else {
  //     window?.maximize();
  //     setMaximized(true);
  //   }
  // }

  const classes = useStyles(tabWidth, availableWidth)();

  return (
    <div
      className={clsx(classes.wrapper, noScrollStyles.appbar)}
      ref={wrapperRef}
    >
      <div
        className={clsx(classes.tabsWrapper, noScrollStyles.tabswrapper)}
        ref={tabsWrapperRef}
      >
        <div className={classes.tabs}>
          {valuesIn(displayTabs).map((displayTab) => (
            <Tab
              key={displayTab.tabID}
              label={displayTab.tabLabel}
              uuid={displayTab.tabID}
              isActive={activeTabID === displayTab.tabID}
              width={tabWidth}
              canClose={valuesIn(displayTabs).length > 1}
              onSwitch={(id) =>
                dispatch({ type: 'SET_ACTIVE_TAB', payload: id })
              }
              onClose={(id) => dispatch({ type: 'DELETE_TAB', payload: id })}
            />
          ))}
        </div>
        <div className={classes.activeIndicatorWrapper}>
          <div
            className={classes.activeIndicator}
            style={{ marginLeft: tabWidth * activeTabIndex }}
          />
        </div>
      </div>
      <div className={classes.endButtons}>
        <IconButton size="small" onClick={() => dispatch({ type: 'ADD_TAB' })}>
          <Icon>add</Icon>
        </IconButton>
        {/* <div style={{ alignSelf: 'flex-end' }}>
          <IconButton size="small" onClick={() => {}}>
            <Icon>minimize</Icon>
          </IconButton>
          <IconButton size="small" onClick={() => toggleMaximized()}>
            {maximized ? <Icon>fullscreen_exit</Icon> : <Icon>fullscreen</Icon>}
          </IconButton>
          <IconButton size="small" onClick={() => {}}>
            <Icon>clear</Icon>
          </IconButton>
        </div> */}
      </div>
    </div>
  );
}
