import {
  Dialog,
  Icon,
  IconButton,
  makeStyles,
  Menu,
  MenuItem,
} from '@material-ui/core';
import React, { useContext, useState } from 'react';
import { TabContext } from '../../context/TabContext';
import EditTextDialog from '../EditTextDialog';
import Login from '../login/Login';

const useStyles = (active: boolean) =>
  makeStyles((theme) => ({
    iconButton: {
      color: active ? 'rgba(255,255,255,0.9)' : 'rgba(255,255,255,0.6)',
    },
  }));

interface TabMenuProps {
  id: string;
  label: string;
  active: boolean;
}

/**
 * A menu that is displayed when clicking the options button on a tab
 * @param props The id, label and active status of the tab this menu opens for
 */
export default function TabMenu(props: TabMenuProps) {
  const { id, label, active } = props;

  const { state: tabState, dispatch } = useContext(TabContext);
  const { tabs, manager } = tabState;
  const tab = tabs[id];

  const [editLabelOpen, setEditLabelOpen] = useState(false);
  const [changeConnectionOpen, setChangeConnectionOpen] = useState(false);
  const [menuAnchorEl, setMenuAnchorEl] = useState<null | HTMLButtonElement>(
    null
  );

  const classes = useStyles(active)();

  function changeLabel(value: string) {
    dispatch({ type: 'SET_DISPLAYTAB_LABEL', payload: { id, label: value } });
    setEditLabelOpen(false);
  }

  return (
    <>
      <IconButton
        size="small"
        onClick={(event: React.MouseEvent<HTMLButtonElement>) => {
          setMenuAnchorEl(event.target as HTMLButtonElement);
        }}
      >
        <Icon className={classes.iconButton}>more_vert</Icon>
      </IconButton>
      <Menu
        id="icon-menu"
        anchorEl={menuAnchorEl}
        keepMounted
        open={Boolean(menuAnchorEl)}
        onClose={() => setMenuAnchorEl(null)}
      >
        <MenuItem
          onClick={() => {
            setEditLabelOpen(true);
            setMenuAnchorEl(null);
          }}
        >
          Edit tab label
        </MenuItem>
        <MenuItem
          disabled={tab.recordingRunning || !manager.connections[id]}
          onClick={() => {
            setChangeConnectionOpen(true);
            setMenuAnchorEl(null);
          }}
        >
          Edit connection
        </MenuItem>
      </Menu>
      <EditTextDialog
        open={editLabelOpen}
        onClose={() => setEditLabelOpen(false)}
        title="Edit tab name"
        valuePlaceholder="Tab name"
        value={label}
        buttonTitle="Save"
        onSubmit={changeLabel}
      />
      <Dialog
        open={changeConnectionOpen}
        onClose={() => setChangeConnectionOpen(false)}
      >
        <Login onConnect={() => setChangeConnectionOpen(false)} tabID={id} />
      </Dialog>
    </>
  );
}
