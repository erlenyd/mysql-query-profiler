import {
  Button,
  Divider,
  Icon,
  IconButton,
  makeStyles,
  Tooltip,
} from '@material-ui/core';
import React, { useContext } from 'react';
import { TabContext } from '../../context/TabContext';
import TabMenu from './TabMenu';

// Need to accept width before making styles, therefore a double arrow function
const useStyles = (width: number, active: boolean, canClose: boolean) =>
  makeStyles(() => ({
    wrapper: {
      display: 'flex',
      flexFlow: 'row nowrap',
      alignItems: 'center',
      justifyItems: 'center',
      flexGrow: 1,
      flexShrink: 0,
      flexBasis: width,
      maxWidth: width,
    },
    labelButton: {
      flexGrow: 1,
      maxWidth: width - 30 - 30 - 5, // option button, close button, divider margin
      minWidth: width - 30 - 30 - 5,
    },
    labelButtonText: {
      maxWidth: width - 30 - 30 - 5 - 16,
      color: active ? 'rgba(255,255,255,0.9)' : 'rgba(255,255,255,0.6)',
      display: 'inline-block',
      textOverflow: 'ellipsis',
      whiteSpace: 'nowrap',
      overflow: 'hidden',
    },
    closeButton: {
      color:
        active && canClose ? 'rgba(255,255,255,0.9)' : 'rgba(255,255,255,0.6)',
    },
    endDivider: {
      marginLeft: 4,
    },
  }));

interface TabProps {
  label: string;
  uuid: string;
  isActive: boolean;
  width: number;
  canClose: boolean;
  onSwitch: (uuid: string) => void;
  onClose: (uuid: string) => void;
}

/**
 * The displayed tab, containing its label and some controls.
 * @param props The information needed to display and manipulate the tab
 */
export default function Tab(props: TabProps) {
  const { label, uuid, isActive, width, canClose, onSwitch, onClose } = props;

  const { state: tabState } = useContext(TabContext);
  const { tabs } = tabState;
  const tab = tabs[uuid];

  const closeable = canClose && !tab.recordingRunning;

  // Passing in max width of a tab
  const classes = useStyles(width, isActive, closeable)();

  return (
    <div className={classes.wrapper}>
      <Tooltip title={label} enterDelay={500} enterNextDelay={500}>
        <Button
          className={classes.labelButton}
          classes={{ label: classes.labelButtonText }}
          onClick={() => onSwitch(uuid)}
        >
          {label}
        </Button>
      </Tooltip>
      <TabMenu active={isActive} id={uuid} label={label} />
      <IconButton
        size="small"
        onClick={() => onClose(uuid)}
        disabled={!closeable}
      >
        <Icon className={classes.closeButton}>clear</Icon>
      </IconButton>
      <Divider className={classes.endDivider} orientation="vertical" flexItem />
    </div>
  );
}
