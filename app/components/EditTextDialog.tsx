import { Button, Dialog, DialogTitle, TextField } from '@material-ui/core';
import React, { useState } from 'react';

interface EditTextDialogProps {
  open: boolean;
  title: string;
  buttonTitle: string;
  value: string;
  valuePlaceholder: string;
  onClose: () => void;
  onSubmit: (value: string) => void;
}

/**
 * A general purpose dialog for editing simple text
 * @param props The information needed to display and manipulate the text
 */
export default function EditTextDialog(props: EditTextDialogProps) {
  const {
    open,
    title,
    buttonTitle,
    value,
    valuePlaceholder,
    onClose,
    onSubmit,
  } = props;

  const [inputValue, setInputValue] = useState(value);

  function checkShortcut(key: string) {
    if (key === 'Enter') {
      onSubmit(inputValue);
    }
  }

  return (
    <Dialog open={open} onClose={() => onClose()}>
      <DialogTitle>{title}</DialogTitle>
      <TextField
        autoFocus
        variant="filled"
        label={valuePlaceholder}
        value={inputValue}
        onChange={(event) => setInputValue(event.target.value)}
        onKeyDown={(event) => checkShortcut(event.key)}
      />
      <Button
        variant="contained"
        color="primary"
        onClick={() => onSubmit(inputValue)}
      >
        {buttonTitle}
      </Button>
    </Dialog>
  );
}
