/* eslint-disable @typescript-eslint/no-explicit-any */
import { ResultValue } from '@mysql/xdevapi';
import { ChartColors } from '../backend/data-processor/types/ChartColors';
import { ChartData } from '../backend/data-processor/types/ChartData';
import { ExplainAnalyzeNode } from '../backend/data-processor/types/ExplainAnalyzeNode';
import { StageTimes } from '../backend/data-processor/types/StageTimes';
import { QueryOutput } from '../backend/types/QueryOutput';

export interface Recording {
  chartData?: ChartData;
  stageTimes?: StageTimes;
  chartColors?: ChartColors;
  top3Memory?: string[];
  optimizerTrace?: ResultValue[][];
  queryOutput?: QueryOutput;
  explainAnalyze: boolean;
  explainAnalyzeRoot?: ExplainAnalyzeNode;
  explainAnalyzeParents?: { [id: string]: string };
  error?: any;
  label: string; // On both in order to avoid having to create an 'activeRecordingListItem'
  uuid: string;
  isSaved: boolean;
  query: string;
  elapsed: number;
}
