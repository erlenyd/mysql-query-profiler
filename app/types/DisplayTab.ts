export interface DisplayTab {
  tabID: string; // UUID
  tabLabel: string;
}
