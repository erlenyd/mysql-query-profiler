/* eslint-disable @typescript-eslint/no-explicit-any */
import { RawRecording } from '../backend/recorder/SqlManager';

export interface RecordingUpdate {
  result?: RawRecording;
  elapsed?: number;
  label: string;
  color: string;
  error?: any;
  explainAnalyze: boolean;
  query: string;
  tabID?: string;
}
