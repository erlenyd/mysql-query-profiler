export interface RecordingListItem {
  uuid: string;
  query: string;
  elapsed: number;
  label: string;
  color: string;
  viewing: boolean;
  isSaved: boolean;
}
