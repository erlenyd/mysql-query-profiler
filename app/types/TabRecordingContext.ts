import { Recording } from './Recording';
import { RecordingListItem } from './RecordingListItem';

export interface TabRecordingContext {
  tabID: string;
  activeRecordingID?: string;
  recordings: { [key: string]: Recording };
  recordingListItems: { [key: string]: RecordingListItem };
}
