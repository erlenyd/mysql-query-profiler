import { LoginDetails } from '../backend/types/LoginDetails';
import { Recording } from './Recording';
import { RecordingListItem } from './RecordingListItem';

export interface Tab {
  tabID: string; // UUID
  serial: number; // How many tabs had been made when this one was made
  activeRecordingID?: string;
  recordings: { [key: string]: Recording };
  recordingListItems: { [key: string]: RecordingListItem };
  loginDetails?: LoginDetails;

  // QueryRecorder state
  recordingRunning: boolean;
  queryRunning: boolean;
  inputQuery: string;
  timestep: number;
  explainAnalyze: boolean;
  inputLabel: string;
  inputColor: string;
  showAdvancedOptions: boolean;
  recordingState: string; // What the manager is doing while recording
}
